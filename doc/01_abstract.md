
\newpage

# Abstract

In recent research projects OpenStreetMap (OSM) was extended to contain temporal information about events, such as public festivals, concerts or sport events. Since the original OSM project only allowed spatial information the whole OSM infrastructure was cloned onto a local server at TU Munich.

In this project an Android application called 'OpenEventMap' was developed to view, edit and add such events on our OSM server. It comes with an intuitive user interface that allows to do this without directly working on the OSM tags (key-value pairs).

The Android platform can be used to obtain user related information, such as the user's current location, and to search events only at locations nearby.

Details about all the functionalities of the developed app are given in the rest of this document.
