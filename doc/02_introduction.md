# Introduction

As a widely used tool to collaboratively gather spatial information and to explore this data on a map, OpenStreetMap serves as a perfect base to also gather temporal information and store this into just the same database. Also there exist a lot of open source editors to manipulate the OSM database, such as [JOSM][url_josm]. And even a wide variety of multi-purpose Android apps have been developed. For a complete overview of these apps visit the [OSM wiki][url_wiki_android]. [Vespucci][url_vespucci] was chosen as the starting point for OpenEventMap since it provided the core functionalities such as navigating a map and uploading changesets to the OSM server. All the customizations to Vespucci are explained in the next section.

## Customizing Vespucci

Vespucci was designed to be an editor that works with the original OSM server. However we require OpenEventMap to work with our own OSM server. This is why the default server API was changed from `http://api.openstreetmap.org/` to `http://osm.openeventmap.tum.de/`. In addition, to get a consistent view on the database the source for the map images had to be changed. Instead of using the Mapnik images from `http://{a|b|c}.tile.openstreetmap.org`, we now use the images provided by our OSM server clone at `http://{a|b|c}.tile.openeventmap.tum.de/osm_tiles/` as default.

A couple of menu buttons were removed from the original application, as we wanted to restrict the use of OpenEventMap to editing events only. Functionalities such as editing tags of OSM elements manually is therefore no longer provided as they would confuse the user and clutter the user interface.

[url_josm]: https://josm.openstreetmap.de/
[url_wiki_android]: http://wiki.openstreetmap.org/wiki/Android
[url_vespucci]: http://wiki.openstreetmap.org/wiki/Vespucci
