# OpenEventMap

This chapter explains in detail how the OpenEventMap Android app is structured, what the newly developed parts are and how everything works together behind the scenes.

## The OSM Server

To understand where the displayed events come from you need to know what kind of interfaces the OSM server provides.

First there is the standard OSM API reachable at: `http://osm.openeventmap.tum.de/api/0.6/`.
This API is used by OpenEventMap for downloading OSM elements (namely nodes, ways and relations).
These elements are delivered in form of XML files.
Therefore a couple of XML parsers were written that extract the relevant event information and create Java Event objects that can be used in the app.

Second there is an SQL database reachable at: `jdbc:postgresql://osm.openeventmap.tum.de:5432/myosmdb`.
This database contains all event information that was inserted in the OSM database in condensed form that allows for easy and fast searching using SQL.
However, since the results are obtained using [JDBC][url_jdbc] again a transformation into Java Event objects usable in the app needs to be performed.

## OSM elements

In OpenStreetMap there exist exactly three types of basic elements.

 - **Nodes** are located at given coordinates consisting of latitude and longitude and represent single objects such as trees or parts of bigger objects such as the corner of a building.
 - **Ways** consist of an ordered list of nodes. They can represent buildings or roads for example.
 - **Relations** group multiple OSM elements together for defining a relationship between them. For example many short borders could together make up the border of a country.

Each of these elements can be identified by its type, one of 'node', 'way' or 'relation', and by its id.
The id itself is not enough because elements of different types may have the same id.

Events are attached to these basic OSM elements using tags.
A tag is a key-value pair describing a property of the given element.
The relevant tags for events are the following:

- `event` : Set to 'yes' if the element contains more event information. Otherwise set to 'no' or left out entirely.
- `event:ID:name` : Contains the name of the event.
- `event:ID:category` : Contains the main category of the event.
- `event:ID:subcategory` : Contains a subcategory of the given main category.
- `event:ID:startdate` : When is the event supposed to start.
- `event:ID:enddate` : When is the event supposed to end.
- `event:ID:related_items` : What other OSM elements are related to this event.
- `event:ID:url` : A link to a homepage containing more information about the event.
- `event:ID:num_participants` : A rough estimate of how many people take part in the event.
- `event:ID:howoften` : Tells us whether the event takes place on a regularly time interval.
- `event:ID:comment` : Some additional information not contained in the other event tags.
- `event:ID:organization` : The organization responsible for the event.

Note that many events can be associated with one and the same OSM element.
This is why every event has its own event number (ID) that is used in the event tags.
So each event can be identified by the type and the id of the OSM element to which it is attached and by the number of the event within this OSM element.
This information as well as a latitude and a longitude value (at least for nodes) are additionally contained in the SQL database mentioned above.
However the values of the comment and organization tags are currently not included in this SQL table.

## The Java Event Object

Android applications are mainly written in Java.
This is why the two different formats of events available from the server mentioned above need to be transformed into Java objects.
The Event class contains fields for all the stored information as well as a couple of methods needed for import, export and visualization.
These objects are used throughout all OpenEventMap activities presented below.

## The OpenEventMap Activities

Android activities are user interfaces allowing the user to complete one specific task at a time.
Besides the `Main` activity that was already present in Vespucci but widely extended in OpenEventMap there are several new activities designed for OpenEventMap.

![ Activity work flow. Solid arrows show what other activities can be started by the current activity. Dashed arrows indicate that the Main activity is started for displaying or editing the related OSM elements, but after that the user will be redirected to the last activity. ](images/Activities.png)

### The Main Activity ###

This User Interface (`Main.java`) is mainly used to navigate (pan and zoom) the world map and to display the events as colored markers on the map.
The colors indicate the category of the event.
Through its action bar (main menu) the following actions can be performed:

- Search for events by event properties (name, category, start date, end date) either world-wide, only in the currently displayed bounding box or in a certain radius around the user's current location (determined by GPS).
- Search for places by inserting a query for [Nominatim][url_nominatim], which allows essentially for searching places by name.
- Center the map around the user's current location (determined by GPS)
- Upload changes to the OSM server. Needed when new events were created or existing events were modified.
- Enter the preferences. Needed for entering user credentials for the OSM server before uploading changes is possible.
- Undo changes. Delete created events or undo the changes of an existing event.
- Enable the creation of a new event by pressing the add event menu item.

Besides the sole UI, the main activity is responsible to perform a lot of actions (mainly utilizing other classes such as the `Server` and the `Storage`) in the background:

- It downloads information from the SQL database when the user requests an event search or navigates on the map.
- It downloads information from the API when the user wants to view or edit event information.
- It keeps track of what the user is currently doing and is starting other activities as needed.
- It checks whether the user has clicked an event on the map. If multiple events at the same spot are assigned to different OSM elements, the user can choose the wanted element through a dialog. If multiple events are assigned to this single element, the user can choose the wanted event through a dialog as well.
- It initiates the creation of an event, if the user has pressed the add event menu item before and clicked on some location on the map afterwards.
- It shows details of a selected event above the map to have the event information and the view of the map at the same time.

![ The Main activity displaying events with colored markers. The blue marker indicates a social event and the green marker indicates a sports event. ](images/MainActivity.png)

![ The Main activity displaying related items of the selected event. ](images/MainActivity-ViewRelatedItems.png)

![ The Main activity displaying related items of the selected event, ready to be edited by the user. ](images/MainActivity-EditRelatedItems.png)

### The Event Viewer Activity ###

The Event Viewer (`EventViewer.java`) is started when the user selects an event on the map or one of the event search results.
In order to show the most up-to-date information and the locally made changes, the event object is rebuild from the API and the local storage.
The UI of the Event Viewer consists of a couple of Android `TextView`s displaying all the relevant event information in a human readable format.
Especially the start and end dates are converted into a format matching the user's locale, and as such respecting the user's language preferences.

Available actions here are:

- Viewing the event on the map. This will bring up the Main activity again, now only displaying this single event in the middle of the map together with all its related items.
- Start editing of the event. This will start the Event Editor activity for modifying the current event.
- Add a new event to the same OSM element. This will start the Event Editor for entering information about the new event.
- Go back to the Main activity.

![ The Event Viewer displaying detailed information about an event selected on the map. ](images/EventViewer.png)

### The Event Editor Activity ###

The Event Editor (`EventEditor.java`) is composed of several Android `EditText`s, `Button`s, `Spinner`s and `Dialog`s that allow the user to input event information in a most natural way.
For example dates are entered through a date picker where the day, month and year can be entered in the same intuitive and therefore consistent way as for example birthdays are entered in the Contacts app.
In addition a small calendar could appear on large displays to choose the date directly from the calendar view.

The Event Editor is started whenever the user decides to edit an existing event in the Event Viewer, or when a new event should be created.
It can be used to perform the following actions:

- Change the values of all the available event tags.
- Save the changes to the local storage to be uploaded later on.
- Start the Main activity to display the map with surrounding OSM elements in order to choose the related items of the event.

![ The Event Editor displaying detailed information about an event selected on the map and ready to be edited by the user. ](images/EventEditor.png)

### The Event Search Activity ###

This activity (`EventSearch.java`) allows the user to input name, category as well as start and end date of events he is interested in.
Submitting this information will start another activity, actually performing the search and displaying the results in a `ListView`.

![ The Event Search activity lets the user insert a couple of filter criteria for the event search. ](images/EventSearch.png)

### The Event Search Results Activity ###

Here the actual search is performed on the SQL database.
The results show the most important information about each events satisfying the given search criteria.
A custom adapter (`EventListAdapter.java`) and item layout (`res/layout/event_search_list_item.xml`) have been created and designed to have the needed flexibility in displaying the event details.

If no matching events could be found the user has the following options:

- Create a new event with the already specified information. This saves editing time and makes inserting events much faster after a failed search.
- Return to the Event Search activity and refine the search criteria.
- Stop searching and go back to the map.

In addition, clicking one of the results starts the Event Viewer for viewing all details about the event.

![ The Event Search Results activity displaying summerized information about all events matching the search criteria. ](images/EventSearch-MultipleResults.png)

![ The Event Search Results activity letting the user decide what to do after no events where found that match the given search criteria. ](images/EventSearch-NoResults.png)

### The Place Search Activity ###

This most simple activity (`PlaceSearch.java`) only offers an `EditText` where the user can input a search query for the Nominatim service.
After commiting this query another activity will be started actually perfoming the search on the server and displaying the results.

![ The Place Search activity lets the user enter the name of a location to search for. ](images/PlaceSearch.png)

### The Place Search Results Activity ###

Finally if some results have been found for the place the user is searching, they are displayed here using directly the result string that Nominatim is returning.
These strings are very long, but also very detailed so that the user can simply decide which search result is actually the one he wanted in the first place. At the moment the results are limited to the area around Munich, as event and map data are only available there right now.

Choosing one of the results starts the Main activity again and centers the map around the given place.
The user could now perform an event search in order to find events only around the given place using exactly the same workflow as described before.

![ The Place Search Results activity letting the user choose one of the matching places for the search criteria. ](images/PlaceSearchResults.png)

[url_jdbc]: http://www.oracle.com/technetwork/java/javase/jdbc/index.html
[url_nominatim]: http://wiki.openstreetmap.org/wiki/Nominatim
