# Build Instructions

To build the app with Eclipse on your computer you need the following things:

- The Android SDK (available at [http://developer.android.com/sdk/index.html#download][android_sdk]) including:

	- Eclipse
	- Android Development Tools (ADT) plugin for Eclipse
	- Android SDK Tools
	- Android Platform-Tools
	- The latest Android platform
	- The latest Android system image for the emulator

- The Android Support Library v4 (Installation instructions available at [http://developer.android.com/tools/support-library/setup.html][support_library])

- The ActionBarSherlock source code (available at [http://actionbarsherlock.com/download.html][action_bar_sherlock])

- The OpenEventMap source code (available at [https://bitbucket.org/polous/android-app][open_event_map])

Since OpenEventMap depends on ActionBarSherlock in order to also support devices running an older version of Android, we need to set up the ActionBarSherlock project first.
Extract the downloaded source code of ActionBarSherlock.
In Eclipse go to *File->New->Project...* and in the folder *Android* select *Android Project from Existing Code*.
As *root directory* select the folder **actionbarsherlock** in the extracted source package and in *New Project Name* enter **ActionBarSherlock** (note the capital letters).
Also make sure to copy the project into your workspace.
For some reason the android-support-v4.jar in the libs folder of your ActionBarSherlock does not contain all necessary classes for the OpenEventMap project.
Therefore make sure to replace it with the version provided by your Android SDK (for more details see: [http://developer.android.com/tools/support-library/setup.html][support_library]).

Once cloned OpenEventMap from the git repository, use Eclipse to import the existing project into your workspace.
Make sure **ActionBarSherlock** is included in the *Projects* section of the *Java Build Path* and that it is referenced as *Library* in the *Android* section of your projects properties.
Finally check that **ActionBarSherlock/libs/android-support-v4.jar** is included in the *Libraries* section of your *Java Build Path*.
Having done all these configuration steps, Eclipse should now build the Android application without any other problems.

[android_sdk]: http://developer.android.com/sdk/index.html#download
[support_library]: http://developer.android.com/tools/support-library/setup.html
[action_bar_sherlock]: http://actionbarsherlock.com/download.html
[open_event_map]: https://bitbucket.org/polous/android-app
