package de.blau.android;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(
	formKey = "",
	mailTo = "MarkMuth@gmx.net",
	customReportContent = {
			ReportField.USER_COMMENT,
			ReportField.ANDROID_VERSION,
			ReportField.APP_VERSION_NAME,
			ReportField.BRAND,
			ReportField.PHONE_MODEL,
			ReportField.CUSTOM_DATA,
			ReportField.STACK_TRACE,
			ReportField.APP_VERSION_CODE,
			ReportField.LOGCAT },
    mode = ReportingInteractionMode.NOTIFICATION,
	resNotifTickerText = R.string.crash_notif_ticker_text,
	resNotifTitle = R.string.crash_notif_title,
	resNotifText = R.string.crash_notif_text,
	resDialogText = R.string.crash_dialog_text)
public class Application extends android.app.Application {
	public static Main mainActivity;
	public static String userAgent;
	
	@Override
	public void onCreate() {
		// The following line triggers the initialization of ACRA
		ACRA.init(this);
		super.onCreate();
		String appName = getString(R.string.app_name);
		String appVersion = getString(R.string.app_version);
		userAgent = appName + "/" + appVersion;
	}
}
