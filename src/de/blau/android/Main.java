package de.blau.android;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.acra.ACRA;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.ZoomControls;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;

import de.blau.android.Logic.CursorPaddirection;
import de.blau.android.Logic.Mode;
import de.blau.android.TagEditor.TagEditorData;
import de.blau.android.actionbar.ModeDropdownAdapter;
import de.blau.android.actionbar.UndoDialogFactory;
import de.blau.android.easyedit.EasyEditManager;
import de.blau.android.exception.OsmException;
import de.blau.android.osb.Bug;
import de.blau.android.osb.CommitTask;
import de.blau.android.osm.BoundingBox;
import de.blau.android.osm.Node;
import de.blau.android.osm.OsmElement;
import de.blau.android.osm.Relation;
import de.blau.android.osm.Server;
import de.blau.android.osm.StorageDelegator;
import de.blau.android.osm.UndoStorage;
import de.blau.android.osm.Way;
import de.blau.android.photos.Photo;
import de.blau.android.prefs.PrefEditor;
import de.blau.android.prefs.Preferences;
import de.blau.android.presets.Preset;
import de.blau.android.resources.Profile;
import de.blau.android.services.TrackerService;
import de.blau.android.services.TrackerService.TrackerBinder;
import de.blau.android.services.TrackerService.TrackerLocationListener;
import de.blau.android.util.GeoMath;
import de.blau.android.util.OAuthHelper;
import de.blau.android.util.SavingHelper;
import de.blau.android.views.overlay.OpenStreetMapViewOverlay;
import de.tum.bgu.lfk.openeventmap.Event;
import de.tum.bgu.lfk.openeventmap.EventEditor;
import de.tum.bgu.lfk.openeventmap.EventIdentifier;
import de.tum.bgu.lfk.openeventmap.EventQuery;
import de.tum.bgu.lfk.openeventmap.EventSearch;
import de.tum.bgu.lfk.openeventmap.EventViewer;
import de.tum.bgu.lfk.openeventmap.MapOverlay;
import de.tum.bgu.lfk.openeventmap.Nominatim;
import de.tum.bgu.lfk.openeventmap.OsmElementIdentifier;
import de.tum.bgu.lfk.openeventmap.PlaceSearch;
import de.tum.bgu.lfk.openeventmap.PlaceSearchableActivity;
import de.tum.bgu.lfk.openeventmap.SearchableActivity;

/**
 * This is the main Activity from where other Activities will be started.
 * 
 * @author mb
 * @author mark
 */
public class Main extends SherlockActivity implements OnNavigationListener, ServiceConnection, TrackerLocationListener {

	/**
	 * Tag used for Android-logging.
	 */
	private static final String DEBUG_TAG = Main.class.getName();
	
	private static final String LOG_TAG = "MARK";

	/**
	 * Requests a {@link BoundingBox} as an activity-result.
	 */
	public static final int REQUEST_BOUNDINGBOX = 0;

	/**
	 * Requests a list of {@link Tag Tags} as an activity-result.
	 */
	public static final int REQUEST_EDIT_TAG = 1;
	// Following request codes are used to identify which activity returned a result
	public static final int REQUEST_EVENT_VIEWER = 2;
	public static final int REQUEST_EVENT_EDITOR = 3;
	public static final int REQUEST_EVENT_SEARCH = 4;
	public static final int REQUEST_SEARCHABLE_ACTIVITY = 5;
	public static final int REQUEST_PLACE_SEARCH = 6;
	public static final int REQUEST_PLACE_SEARCHABLE_ACTIVITY = 7;

	private DialogFactory dialogFactory;
	
	// Following strings help the main activity to remember in which order other activities were launched.
	private static String eventViewerStartedBy;
	private static String eventEditorStartedBy;
	private static String eventSearchStartedBy;
	private static String searchableActivityStartedBy;
	private static EventQuery searchableActivityStartedWithEventQuery;
	private static String placeSearchStartedBy;
	private static String placeSearchableActivityStartedBy;
	private static String placeSearchableActivityStartedWithPlaceQuery;
	
	// Following location is used to search in the current location only
	private Location lastGPSLocation = null;
	boolean useGPSLocationToShow = false;
	boolean useGPSLocationToSearch = false;
	
	// Following variable stores whether the add event button has already been pressed
	boolean addingNewEvent = false;
	
	/** Following variable stores the event that was selected on the map */
	Event selectedEvent;
	
	/**
	 * Used to handle clicks on events. Choose between multiple OsmElements, Events etc.
	 */
	private EventClickHandler eventClickHandler = new EventClickHandler();
	
	private List<OsmElementIdentifier> elementChoice;
	
	/** Objects to handle showing device orientation. */
	private SensorManager sensorManager;
	private Sensor sensor;
	private SensorEventListener sensorListener = new SensorEventListener() {
		float lastOrientation = -9999;
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
		@Override
		public void onSensorChanged(SensorEvent event) {
			float orientation = event.values[0];
			map.setOrientation(orientation);
			// Repaint map only if orientation changed by at least 1 degree since last repaint
			if (Math.abs(orientation - lastOrientation) > 1) {
				lastOrientation = orientation;
				map.invalidate();
			}
		}
	};

	/** The map View. */
	private Map map;
	/** Detector for taps, drags, and scaling. */
	private VersionedGestureDetector mDetector;
	/** Onscreen map zoom controls. */
	private ZoomControls zoomControls;
	/** Area for showing event details above the map */
	private LinearLayout event_details;
	/**
	 * Our user-preferences.
	 */
	private Preferences prefs;

	/**
	 * Adapter providing items for the mode selection dropdown in the ActionBar
	 */
	private ModeDropdownAdapter modeDropdown;

	/**
	 * The manager for the EasyEdit mode
	 */
	private EasyEditManager easyEditManager;

	/**
	 * The logic that manipulates the model. (non-UI)<br/>
	 * This is created in {@link #onCreate(Bundle)} and never changed afterwards.<br/>
	 * If may be null or not reflect the current state if accessed from outside this activity.
	 */
	protected static Logic logic;
	
	/**
	 * The currently selected preset
	 */
	private static Preset currentPreset;

	/**
	 * Flag indicating whether the map will be re-downloaded once the activity resumes
	 */
	private static boolean redownloadOnResume;

	/**
	 * Flag indicating whether data should be loaded from a file when the activity resumes.
	 * Set by {@link #onCreate(Bundle)}.
	 * Overridden by {@link #redownloadOnResume}.
	 */
	private boolean loadOnResume;

	/** Initialized in onCreate - this empty file indicates by its existence that showGPS should be enabled on start */
	private File showGPSFlagFile = null;
	private boolean showGPS;
	private boolean followGPS;
	/**
	 * a local copy of the desired value for {@link TrackerService#setListenerNeedsGPS(boolean)}.
	 * Will be automatically given to the tracker service on connect.
	 */
	private boolean wantLocationUpdates = false;

	/**
	 * The current instance of the tracker service
	 */
	private TrackerService tracker = null;

	private UndoListener undoListener;

	/**
	 * While the activity is fully active (between onResume and onPause), this stores the currently active instance
	 */
	private static Main runningInstance;

	private boolean shouldCreateEventFromEventQuery = false;
	private EventQuery createEventFromEventQuery = null;

	public boolean shouldDrawRelatedItemsOnMap = false;
	public Set<Long> relatedNodes = null;
	public Set<Long> relatedWays = null;
	public Set<Long> relatedRelations = null;
	private Set<Long> nodesToDownload = null;
	private Set<Long> waysToDownload = null;
	
	public boolean isRelatedItemsEditingEnabled = false;
	public Event eventOfRelatedItems = null;
	
	private Button legendButton = null;
	public int getLegendButtonTop() {
		if (legendButton != null) {
			return legendButton.getTop();
		}
		return 0;
	}
	public int getLegendButtonLeft() {
		if (legendButton != null) {
			return legendButton.getLeft();
		}
		return 0;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		Log.i("Main", "onCreate");
		setTheme(R.style.Theme_customMain);
		
		super.onCreate(savedInstanceState);
		Application.mainActivity = this;
		
		showGPSFlagFile = new File(getFilesDir(), "showgps.flag");
		showGPS = showGPSFlagFile.exists();
		
		sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
		if (sensorManager != null) {
			sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
			if (sensor == null) {
				sensorManager = null;
			}
		}
		
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		
		if (map != null) {
			map.onDestroy();
		}
		map = new Map(getApplicationContext());
		map.setId(1);
		dialogFactory = new DialogFactory(this);
		
		//Register some Listener
		MapTouchListener mapTouchListener = new MapTouchListener();
		map.setOnTouchListener(mapTouchListener);
		map.setOnCreateContextMenuListener(mapTouchListener);
		map.setOnKeyListener(new MapKeyListener());
		mDetector = VersionedGestureDetector.newInstance(getApplicationContext(), mapTouchListener);
		
		// Set up the zoom in/out controls
		zoomControls = new ZoomControls(getApplicationContext());
		zoomControls.setOnZoomInClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				logic.zoom(Logic.ZOOM_IN);
				updateZoomControls();
			}
		});
		zoomControls.setOnZoomOutClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				logic.zoom(Logic.ZOOM_OUT);
				updateZoomControls();
			}
		});

		LayoutInflater linf = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		event_details = (LinearLayout) linf.inflate(R.layout.event_details, null);
		event_details.setId(2);
		

		LinearLayout ll = new LinearLayout(getApplicationContext());
		ll.setOrientation(LinearLayout.VERTICAL);
		
		LinearLayout.LayoutParams rlp_event_details = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		ll.addView(event_details, rlp_event_details);
		
		RelativeLayout rl2 = new RelativeLayout(getApplicationContext());
		
		RelativeLayout.LayoutParams rlp_map = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		rl2.addView(map, rlp_map);
	
		RelativeLayout.LayoutParams rlp_zoomControls = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		rlp_zoomControls.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		rlp_zoomControls.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		rl2.addView(zoomControls, rlp_zoomControls);
		
		// Insert a button to toggle the visibility of the legend
		legendButton = new Button(this);
		legendButton.setText("Legend");
		legendButton.setTextColor(Color.argb(127, 255, 255, 255));
		legendButton.setBackgroundColor(Color.argb(127, 127, 127, 127));
		legendButton.setWidth(182); // make it the same width as the legend above
		legendButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Main.this.toggleLegendVisibility();
			}
		});
		RelativeLayout.LayoutParams rlp_legendButton = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		rlp_legendButton.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		rlp_legendButton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		rl2.addView(legendButton, rlp_legendButton);
		
		ll.addView(rl2);
		
		setContentView(ll);
		
		Button moreButton = (Button) findViewById(R.id.event_details_more_button);
		moreButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// hide the event details from the main activity
				event_details.setVisibility(View.GONE);
				
				// show all event details int he event viewer
				Intent eventViewerIntent = new Intent(getApplicationContext(), EventViewer.class);
				eventViewerIntent.putExtra(EventViewer.EVENT_EXTRA, selectedEvent);
				eventViewerStartedBy = Main.class.getName();
				startActivityForResult(eventViewerIntent, Main.REQUEST_EVENT_VIEWER);
			}
		});
		
		Button hideButton = (Button) findViewById(R.id.event_details_hide_button);
		hideButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				event_details.setVisibility(View.GONE);
			}
		});
		
		event_details.setVisibility(View.GONE);
		
		//Load previous logic (inkl. StorageDelegator)
		logic = (Logic) getLastNonConfigurationInstance();
		loadOnResume = false;
		if (logic == null) {
			Log.i("Main", "onCreate - creating new logic");
			logic = new Logic(map, new Profile(getApplicationContext()));
			if (isLastActivityAvailable()) {
				// Start loading after resume to ensure loading dialog can be removed afterwards
				loadOnResume = true;
			} else {
				// check if we have a position
				Location loc = getLastLocation();
				BoundingBox box = null;
				if (loc != null) {
					try {
						box = GeoMath.createBoundingBoxForCoordinates(loc.getLatitude(),
							loc.getLongitude(), 1000); // a km hardwired for now
					} catch (OsmException e) {
						ACRA.getErrorReporter().handleException(e);
					}
				}
				openEmptyMap(box);
//				gotoBoxPicker();
			}
		} else {
			Log.i("Main", "onCreate - using logic from getLastNonConfigurationInstance");
			logic.setMap(map);
		}
		
		easyEditManager = new EasyEditManager(this, logic);
	}
	
	protected void toggleLegendVisibility() {
		de.tum.bgu.lfk.openeventmap.MapOverlay events = map.getEventsOverlay();
		events.toggleLegendVisibility();
		map.invalidate();
	}

	/**
	 * Get the best last position
	 */
	private Location getLastLocation() {
		LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
		List<String> providers = locationManager.getProviders(true);
		Location bestLocation = null;
		for (String provider : providers) {
			try {
				Location location = locationManager.getLastKnownLocation(provider);
				if (bestLocation == null || !bestLocation.hasAccuracy() ||
						(location != null && location.hasAccuracy() &&
								location.getAccuracy() < bestLocation.getAccuracy())) {
					bestLocation = location;
				}
			} catch (IllegalArgumentException e) {
			} catch (SecurityException e) {
			}
		}
		return bestLocation;
	}
	
	/**
	 * Loads the preferences into {@link #map} and {@link #logic}, triggers new {@inheritDoc}
	 */
	@Override
	protected void onStart() {
		Log.d("Main", "onStart");
		super.onStart();
		prefs = new Preferences(this);
		logic.setPrefs(prefs);
		map.setPrefs(prefs);
		
		map.createOverlays();
		map.requestFocus();
		map.setKeepScreenOn(prefs.isKeepScreenOnEnabled());
		
		undoListener = new UndoListener();
		
		showActionBar();
		
		logic.setSelectedBug(null);
		logic.setSelectedNode(null);
		logic.setSelectedWay(null);
		logic.setSelectedRelation(null);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d("Main", "onResume");

		bindService(new Intent(this, TrackerService.class), this, BIND_AUTO_CREATE);
		
		if (redownloadOnResume) {
			redownloadOnResume = false;
			logic.downloadLast();
		} else if (loadOnResume) {
			loadOnResume = false;
			logic.loadFromFile(getApplicationContext());
		}
		logic.loadEditingState();
		if (currentPreset == null) {
			currentPreset = prefs.getPreset();
		}
		
		logic.updateProfile();
		
		runningInstance = this;
		
		updateActionbarEditMode();
		if (!prefs.isOpenStreetBugsEnabled() && logic.getMode() == Mode.MODE_OPENSTREETBUG) {
			logic.setMode(Mode.MODE_MOVE);
		}
		if (modeDropdown != null)
			modeDropdown.setShowOpenStreetBug(prefs.isOpenStreetBugsEnabled());
		
		if (tracker != null) tracker.setListener(this);
		
		setShowGPS(showGPS); // reactive GPS listener if needed
		setFollowGPS(followGPS);
		
		map.setKeepScreenOn(prefs.isKeepScreenOnEnabled());
	}

	@Override
	protected void onPause() {
		Log.d("Main", "onPause");
		runningInstance = null;
		disableLocationUpdates();
		if (tracker != null) tracker.setListener(null);

		// onPause is the last lifecycle callback guaranteed to be called on pre-honeycomb devices
		// on honeycomb and later, onStop is also guaranteed to be called, so we can defer saving.
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) saveData();
		
		super.onPause();
	}
	
	@Override
	protected void onStop() {
		Log.d("Main", "onStop");
		
		// On devices with Android versions before Honeycomb, we already save data in onPause
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) saveData();
		
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.d("Main", "onDestroy");
		map.onDestroy();
		if (tracker != null) tracker.setListener(null);
		try {
			unbindService(this);
		} catch (Exception e) {} // ignore errors, this is just cleanup
		super.onDestroy();
	}

	/**
	 * Save current data (state, downloaded data, changes, ...) to file(s)
	 */
	private void saveData() {
		Log.i("Main", "saving data");
		logic.save();
		if (showGPS) {
			try {
				showGPSFlagFile.createNewFile();
			} catch (IOException e) {
				Log.e("Main", "failed to create showGPS flag file");
			}
		} else {
			showGPSFlagFile.delete();
		}
		// if something was selected save that
		
	}

	/**
	 * Update the state of the onscreen zoom controls to reflect their ability
	 * to zoom in/out.
	 */
	private void updateZoomControls() {
		zoomControls.setIsZoomInEnabled(logic.canZoom(Logic.ZOOM_IN));
		zoomControls.setIsZoomOutEnabled(logic.canZoom(Logic.ZOOM_OUT));
	}
	
	@Override
	public Object onRetainNonConfigurationInstance() {
		Log.d("Main", "onRetainNonConfigurationInstance");
		return logic;
	}

	/**
	 * Sets up the Action Bar.
	 */
	private void showActionBar() {
		Log.d("Main", "showActionBar");
		ActionBar actionbar = getSupportActionBar();
		actionbar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionbar_bg)));
		actionbar.setDisplayShowHomeEnabled(true);
		actionbar.setDisplayShowTitleEnabled(false);

		if (prefs.depreciatedModesEnabled()) {
			actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			modeDropdown = new ModeDropdownAdapter(this, prefs.isOpenStreetBugsEnabled(), prefs.depreciatedModesEnabled());
			actionbar.setListNavigationCallbacks(modeDropdown, this);	
			ToggleButton lock = (ToggleButton) findViewById(R.id.lock);
			if (lock != null) lock.setVisibility(View.GONE);
		} else {
			actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
			actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM|ActionBar.DISPLAY_SHOW_HOME);
			
			View lockLayout = View.inflate(getApplicationContext(), R.layout.lock, null);
			actionbar.setCustomView(lockLayout);
			ToggleButton lock = setLock(logic.getMode());
			findViewById(R.id.lock).setVisibility(View.INVISIBLE);
			lock.setOnClickListener(new View.OnClickListener() {
			    public void onClick(View b) {
			        Log.d("Main", "Lock pressed");
			        if(((ToggleButton)b).isChecked()) {
			        	logic.setMode(Logic.Mode.MODE_EASYEDIT);
			        } else {
			        	logic.setMode(Logic.Mode.MODE_MOVE);
			        }
			        onEditModeChanged();
			    }
			});
		}	
	
		actionbar.show();
		setSupportProgressBarIndeterminateVisibility(false);
	}
	
	/**
	 * Set lock button to locked or unlocked depending on the edit mode
	 * @param mode
	 * @return
	 */
	private ToggleButton setLock(Logic.Mode mode) {
		ToggleButton lock = (ToggleButton) findViewById(R.id.lock);
		lock.setChecked(mode == Logic.Mode.MODE_EASYEDIT);
		logic.setMode(mode == Logic.Mode.MODE_EASYEDIT ? Logic.Mode.MODE_EASYEDIT : Logic.Mode.MODE_MOVE); // zap any other mode
		return lock; // for convenience
	}

	
	public void updateActionbarEditMode() {
		Log.d("Main", "updateActionbarEditMode");
		if (modeDropdown != null && (prefs!=null && prefs.depreciatedModesEnabled())) 
			getSupportActionBar().setSelectedNavigationItem(modeDropdown.getIndexForMode(logic.getMode()));
		else { 
			setLock(logic.getMode());
		}
	}
	
	public static void onEditModeChanged() {
		Log.d("Main", "onEditModeChanged");
		if (runningInstance != null) runningInstance.updateActionbarEditMode();
	}

	public void onElementChoiceDownloaded(List<OsmElementIdentifier> downloadedElements) {
		OsmElementIdentifier[] eventElements = new OsmElementIdentifier[downloadedElements.size()];
		int i = 0;
		for (OsmElementIdentifier o : downloadedElements) {
			OsmElement e = logic.delegator.getOsmElement(o.osmType, o.osmId);
			if (e == null) {
				Log.e(LOG_TAG, "Element was not downloaded into storage!");
			}
			String name = e.getTagWithKey("name");
			if (name == null) {
				name = "";
			}
			eventElements[i++] = new OsmElementIdentifier(o.osmType, o.osmId, name);
		}
		
		dialogFactory.setMultipleEventElements(eventElements);
		showDialog(DialogFactory.CHOOSE_FROM_MULTIPLE_EVENT_ELEMENTS);
	}

	public void onEventElementSelected(String string) {
		Log.d(LOG_TAG, "onEventElementSelected: Received string is " + string);

		// MARK Following call assures that the event element dialog is recreated every time, intitialized with the right choices!
		removeDialog(DialogFactory.CHOOSE_FROM_MULTIPLE_EVENT_ELEMENTS);
		
		// Step 1: Check if the node/way/relation associated with this event is downloaded
		String[] stringParts = string.split(":");
		String elementType = stringParts[0];
		long elementId = Long.valueOf(stringParts[1]);
		
		OsmElement element = logic.delegator.getOsmElement(elementType, elementId);
		if (element == null) {
			Log.d(LOG_TAG, "onEventElementSelected: Element is not available in the storage!");
			// Step 1?false: Download the missing OSM element from the api
			if (elementType.equalsIgnoreCase(Node.NAME)) {
				logic.downloadAndAddNode(elementId);
			}
			if (elementType.equalsIgnoreCase(Way.NAME)) {
				// MARK TODO Implement downloading of ways
				Log.w(LOG_TAG, "onEventElementSelected: Downloading of ways not yet supported!");
				return;
			}
			if (elementType.equalsIgnoreCase(Relation.NAME)) {
				// MARK TODO Implement downloading of relations
				Log.w(LOG_TAG, "onEventElementSelected: Downloading of relations not yet supported!");
				return;
			}
		} else {
			Log.d(LOG_TAG, "onEventElementSelected: Element is available in the storage!");
			onEventElementDownloaded(element);
		}
	}
	
	public void onEventElementDownloaded(OsmElement element) {
		// Step 2: Select the associated element and check if multiple events are attached
		if (element instanceof Node) {
			logic.setSelectedNode((Node) element);
			HashMap<Long, Event> attachedEvents = ((Node) element).getAttachedEvents();
			if (attachedEvents.size() == 0) {
				Log.w(LOG_TAG, "onEventElementDownloaded: No events were created from the node!");
				return;
			}
			if (attachedEvents.size() == 1) {
				// Step 3: Show the event information. Use another callback method!
				Log.d(LOG_TAG, "onEventElementDownloaded: Exactly on event is associated to this node!");				
				Event e = attachedEvents.values().iterator().next();
				onEventSelected(e.getEventType() + ":" + e.getTypeId() + ":" + e.getNumber());
			} else {
				// Step 2?true: Let user select the right event
				Log.d(LOG_TAG, "onEventElementDownloaded: Multiple events are associated to this node!");
				
				EventIdentifier[] eventIdentifiers = new EventIdentifier[attachedEvents.size()];

				int cEventNumber = 0;
				for (Long eventId : attachedEvents.keySet()) {
					Event cEvent = attachedEvents.get(eventId);
					String name = cEvent.getName();
					if (name == null) {
						name = "";
					}
					eventIdentifiers[cEventNumber++] = new EventIdentifier(cEvent.getEventType(), cEvent.getTypeId(), cEvent.getNumber(), name);						
				}
					dialogFactory.setMultipleEvents(eventIdentifiers);
					showDialog(DialogFactory.CHOOSE_FROM_MULTIPLE_EVENTS);
			}
		} else if (element instanceof Way) {
			logic.setSelectedWay((Way) element);
			// MARK TODO
		} else if (element instanceof Relation) {
			logic.setSelectedRelation((Relation) element);
			// MARK TODO
		}
	}

	public void onEventSelected(String eventString) {
		Log.d(LOG_TAG, "onEventSelected");

		// MARK Following call assures that the event dialog is recreated every time, intitialized with the right choices!
		removeDialog(DialogFactory.CHOOSE_FROM_MULTIPLE_EVENTS);
		
		String[] eventParts = eventString.split(":");
		if (eventParts.length != 3) {
			Log.w(LOG_TAG, "onEventSelected: eventString has not enough parts! Number of parts: " + eventParts.length);
		}
		String osmType = eventParts[0];
		long osmId = Long.valueOf(eventParts[1]);
		long eventId = Long.valueOf(eventParts[2]);
		if (osmType.equalsIgnoreCase(Node.NAME)) {
			Log.d(LOG_TAG, "onEventSelected, Node");	
			Node eventNode = (Node) logic.delegator.getOsmElement(Node.NAME, osmId);
			if (eventNode != null) {
				Log.d(LOG_TAG, "onEventSelected, Node != null");	
				logic.setSelectedNode(eventNode);
				Event event = eventNode.getAttachedEvent(eventId);

				// Show the event information in the event details area
				event_details.setVisibility(View.VISIBLE);
				selectedEvent = event;
				loadEventIntoDetailsArea(selectedEvent);

				return;
			} else {
				Log.w(LOG_TAG, "onEventSelected: Node not available!");
			}
		}
		if (osmType.equalsIgnoreCase(Way.NAME)) {
			// MARK TODO
		}
		if (osmType.equalsIgnoreCase(Relation.NAME)) {
			// MARK TODO
		}		
	}
	
	private void loadEventIntoDetailsArea(Event event) {
		TextView tvName = (TextView) findViewById(R.id.event_details_name_value);
		TextView tvCate = (TextView) findViewById(R.id.event_details_category_value);
		TextView tvDate = (TextView) findViewById(R.id.event_details_date_value);
		TextView tvURL  = (TextView) findViewById(R.id.event_details_url);
		TextView tvNumP = (TextView) findViewById(R.id.event_details_num_participants_value);
		TextView tvHowO = (TextView) findViewById(R.id.event_details_howoften_value);
		
		tvName.setText("Name: " + event.getName());
		tvCate.setText("Category: " + event.getCategory());
		String startDate = event.getStartdate() != null ? event.getStartdateAsString() : getString(R.string.event_view_value_unspecified);
		String endDate = event.getEnddate() != null ? event.getEnddateAsString() : getString(R.string.event_view_value_unspecified);
		tvDate.setText("From " + startDate + " to " + endDate);
		tvURL .setText("URL: " + (event.getUrl() != null ? event.getUrl() : getString(R.string.event_view_value_unspecified)));
		tvNumP.setText("Number of Participants: " + (event.getNumParticipants() != null ? event.getNumParticipants() : getString(R.string.event_view_value_unspecified)));
		tvHowO.setText("How often: " + (event.getHowoften() != null ? event.getHowoften() : getString(R.string.event_view_value_unspecified)));
	}

	public void onGPSLocationRetrieved() {
		if (lastGPSLocation == null) {
			Log.w(LOG_TAG, "onGPSLocationRetrieved got null location!");
			return;
		}
		if (useGPSLocationToShow) {
			useGPSLocationToShow = false;
			int lon = (int) (lastGPSLocation.getLongitude() * 1E7);
			int lat = (int) (lastGPSLocation.getLatitude() * 1E7);
			logic.moveViewBox(lon, lat);
		}
		if (useGPSLocationToSearch) {
			useGPSLocationToSearch = false;
			// Compute bounding box to contain the area in around 1km distance
			try {
				BoundingBox searchArea = GeoMath.createBoundingBoxForCoordinates(
						lastGPSLocation.getLatitude(), lastGPSLocation.getLongitude(), 1000);

				Intent searchIntent = new Intent(getApplicationContext(), EventSearch.class);
				eventSearchStartedBy = Main.class.getName();
				searchIntent.putExtra(EventSearch.SEARCH_AREA_EXTRA, searchArea);
				startActivityForResult(searchIntent, REQUEST_EVENT_SEARCH);
			} catch (OsmException e) {
				Log.e(LOG_TAG, "Error computing the search area bounding box!");
			}
		}
	}
	
	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		Log.d("Main", "onNavigationItemSelected");
		Mode mode = modeDropdown.getModeForItem(itemPosition);
		logic.setMode(mode);
		if (mode == Mode.MODE_TAG_EDIT) {
			// if something is already/still selected, edit its tags
			// prefer ways over nodes, and deselect what we're *not* editing
			OsmElement e = logic.getSelectedWay();
			if (e == null) e = logic.getSelectedNode();
			else logic.setSelectedNode(null);
			if (e != null) performTagEdit(e, null);
		}
		return true;
	}
	
	
	/**
	 * Creates the menu from the XML file "main_menu.xml".<br> {@inheritDoc}
	 */
 	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		Log.d("Main", "onCreateOptionsMenu");
		
		final MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.main_menu, menu); 
		menu.findItem(R.id.menu_return).setVisible(false);

		MenuItem undo = menu.findItem(R.id.menu_undo);
		undo.setVisible(logic.getUndo().canUndo() || logic.getUndo().canRedo());
		View undoView = undo.getActionView();
		undoView.setOnClickListener(undoListener);
		undoView.setOnLongClickListener(undoListener);
		
		return true;
	}

	@Override
 	public boolean onPrepareOptionsMenu(final Menu menu) {
 		if (isRelatedItemsEditingEnabled || shouldDrawRelatedItemsOnMap) {
 			menu.findItem(R.id.menu_main_add_event).setVisible(false);
 			menu.findItem(R.id.menu_main_refresh).setVisible(false);
			menu.findItem(R.id.menu_main_search).setVisible(false);
			menu.findItem(R.id.menu_main_search_places).setVisible(false);
			menu.findItem(R.id.menu_undo).setVisible(false);
			menu.findItem(R.id.menu_main_go_to_current_location).setVisible(false);
			menu.findItem(R.id.menu_transfer_upload).setVisible(false);
			menu.findItem(R.id.menu_confing).setVisible(false);
			menu.findItem(R.id.menu_help).setVisible(false);
			menu.findItem(R.id.menu_return).setVisible(true);
		} else {
 			menu.findItem(R.id.menu_main_add_event).setVisible(true);
 			menu.findItem(R.id.menu_main_refresh).setVisible(true);
			menu.findItem(R.id.menu_main_search).setVisible(true);
			menu.findItem(R.id.menu_main_search_places).setVisible(true);
			menu.findItem(R.id.menu_undo).setVisible(true);
			menu.findItem(R.id.menu_main_go_to_current_location).setVisible(true);
			menu.findItem(R.id.menu_transfer_upload).setVisible(true);
			menu.findItem(R.id.menu_confing).setVisible(true);
			menu.findItem(R.id.menu_help).setVisible(true);
			menu.findItem(R.id.menu_return).setVisible(false); 			
		}
		return true; 		
 	}
 	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
 	public void adjustOptionsMenu() { 		
 		invalidateOptionsMenu();
 	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		Log.d("Main", "onOptionsItemSelected");
		
		Intent searchIntent = null;
		
		switch (item.getItemId()) {
		case R.id.menu_confing:
			startActivity(new Intent(getApplicationContext(), PrefEditor.class));
			return true;
			
		case R.id.menu_help:
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.openeventmap.tum.de/app/help.html"));
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			return true;

		case R.id.menu_main_add_event:
			if (!addingNewEvent) {
				addingNewEvent = true;
				Toast.makeText(this, "Click on the map to add a new event.", Toast.LENGTH_SHORT).show();
			} else {
				addingNewEvent = false;
				Toast.makeText(this, "Canceled adding of event.", Toast.LENGTH_SHORT).show();
			}
			return true;

		case R.id.menu_main_refresh:
			map.getEventsOverlay().getEvents();			
			return true;
			
		case R.id.menu_main_search_event_in_current_location:
			useGPSLocationToSearch = true;
			requestSingleGPSUpdate();			
			return true;

		case R.id.menu_main_search_event_in_current_map_section:
			searchIntent = new Intent(getApplicationContext(), EventSearch.class);
			eventSearchStartedBy = Main.class.getName();
			searchIntent.putExtra(EventSearch.SEARCH_AREA_EXTRA, map.getViewBox());
			startActivityForResult(searchIntent, REQUEST_EVENT_SEARCH);
			return true;

		case R.id.menu_main_search_event_everywhere:
			searchIntent = new Intent(getApplicationContext(), EventSearch.class);
			eventSearchStartedBy = Main.class.getName();
			startActivityForResult(searchIntent, REQUEST_EVENT_SEARCH);
			return true;
			
		case R.id.menu_main_search_places:
			searchIntent = new Intent(getApplicationContext(), PlaceSearch.class);
			placeSearchStartedBy = Main.class.getName();
			startActivityForResult(searchIntent, REQUEST_PLACE_SEARCH);
			return true;
			
		case R.id.menu_main_go_to_current_location:
			// MARK Retrieve current location and center map on it
			useGPSLocationToShow = true;
			requestSingleGPSUpdate();
			return true;

		case R.id.menu_transfer_upload:
			confirmUpload();
			return true;

		case R.id.menu_undo:
			// should not happen
			undoListener.onClick(null);
			return true;

		case R.id.menu_return:
			if (isRelatedItemsEditingEnabled) {
				isRelatedItemsEditingEnabled = false;
				adjustOptionsMenu();
				saveRelatedItemsAndReturnToEventEditor();				
			}
			if (shouldDrawRelatedItemsOnMap) {
				shouldDrawRelatedItemsOnMap = false;
				adjustOptionsMenu();
				map.getEventsOverlay().showEventsInViewBox();
				Intent eventViewerIntent = new Intent(getApplicationContext(), EventViewer.class);
				eventViewerIntent.putExtra(EventViewer.EVENT_EXTRA, eventOfRelatedItems);
				startActivityForResult(eventViewerIntent, Main.REQUEST_EVENT_VIEWER);
			}
			return true;
		}
		
		return false;
	}

	private void saveRelatedItemsAndReturnToEventEditor() {
		Event event = eventOfRelatedItems;
		
		Log.d(LOG_TAG, "Editing of related items finished!");
		
		OsmElement osmElement = logic.delegator.getOsmElement(event.getEventType(), event.getTypeId());
		if (osmElement != null) {
			// Already downloaded and ready to display:
			if (osmElement instanceof Node) {
				Node node = (Node) osmElement;
				// Remove the node itself from the list of related items
				long osmId = node.getOsmId();
				relatedNodes.remove(osmId);
				// Change the related items in the event
				event.setRelatedItems(relatedNodes, relatedWays, relatedRelations);
				// Change the tags in the OSM element
				SortedMap<String, String> nodeTags = node.getTags();
				if (nodeTags != null) {
					Log.d(LOG_TAG, "Tags is not null!");
					SortedMap<String, String> tags = new TreeMap<String, String>(node.getTags());
					// Remove old tags of this event
					String[] eventTags = event.getTagKeys();
					for (String eventTag : eventTags) {
						tags.remove(eventTag);
					}
					// Add new tags of this event
					tags.putAll(event.getTags());

					if (logic.setTags(node.getName(), node.getOsmId(), tags)) {
						Log.d(LOG_TAG, "Altered tags!");
					} else {
						Log.d(LOG_TAG, "Tags were not altered!");
					}
				}
				// Return to the EventEditor
				Intent eventEditorIntent = new Intent(getApplicationContext(), EventEditor.class);
				eventEditorIntent.putExtra(EventEditor.EVENT_EXTRA, event);
				startActivityForResult(eventEditorIntent, Main.REQUEST_EVENT_EDITOR);
			}
		} else {
			Log.w(LOG_TAG, "Node not there. Should have been downloaded earlier!");
		}
		
	}

	private void requestSingleGPSUpdate() {
		
		final LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
		
		// Check if GPS is enabled on the device
		try {
			if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				Toast.makeText(this, "Please turn on GPS!", Toast.LENGTH_SHORT).show();
				return;
			}
		} catch (Exception e) {
			Log.e(LOG_TAG, "Error when checking for GPS: " + e.getMessage());
			return;
		}
		
		Toast.makeText(this, "Waiting for GPS update...", Toast.LENGTH_SHORT).show();
		
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {				
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
				// Do nothing					
			}				
			@Override
			public void onProviderEnabled(String provider) {
				// Do nothing					
			}				
			@Override
			public void onProviderDisabled(String provider) {
				// Do nothing					
			}				
			@Override
			public void onLocationChanged(Location location) {
				lm.removeUpdates(this);
				lastGPSLocation = location;
				onGPSLocationRetrieved();
			}
		});
	}

	private void setShowGPS(boolean show) {
		if (show && !ensureGPSProviderEnabled()) {
			show = false;
		}
		showGPS = show;
		Log.d("Main", "showGPS: "+ show);
		if (show) {
			enableLocationUpdates();
		} else {
			setFollowGPS(false);
			map.setLocation(null);
			disableLocationUpdates();
		}
		map.invalidate();
		triggerMenuInvalidation();
	}
	
	/**
	 * Checks if GPS is enabled in the settings.
	 * If not, returns false and shows location settings.
	 * @return true if GPS is enabled, false if not
	 */
	private boolean ensureGPSProviderEnabled() {
		LocationManager locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
		try {
			if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				return true;
			} else {
				startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
				return false;
			}
		} catch (Exception e) {
			Log.e("Main", "Error when checking for GPS, assuming GPS not available", e);
			Toast.makeText(this, R.string.gps_failure, Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	private void setFollowGPS(boolean follow) {
		// Log.d("Main","setFollowGPS");
		if (followGPS != follow) {
			followGPS = follow;
			if (follow) {
				setShowGPS(true);
				if (map.getLocation() != null) onLocationChanged(map.getLocation());
			}
			map.setFollowGPS(follow);
			triggerMenuInvalidation();
		}
	}
	
	private void toggleShowGPS() {
		boolean newState = !showGPS;
		setShowGPS(newState);
	}
	
	
	private void toggleFollowGPS() {
		boolean newState = !followGPS;
		setFollowGPS(newState);
		map.setFollowGPS(newState);
	}
	
	private void enableLocationUpdates() {
		if (wantLocationUpdates == true) return;
		if (sensorManager != null) {
			sensorManager.registerListener(sensorListener, sensor, SensorManager.SENSOR_DELAY_UI);
		}
		wantLocationUpdates = true;
		if (tracker != null) tracker.setListenerNeedsGPS(true);
	}
	
	private void disableLocationUpdates() {
		if (wantLocationUpdates == false) return;
		if (sensorManager != null) sensorManager.unregisterListener(sensorListener);
		wantLocationUpdates  = false;
		if (tracker != null) tracker.setListenerNeedsGPS(false);
	}

	/**
	 * Handles the menu click on "download current view".<br>
	 * When no {@link #delegator} is set, the user will be redirected to AreaPicker.<br>
	 * When the user made some changes, {@link #DIALOG_TRANSFER_DOWNLOAD_CURRENT_WITH_CHANGES} will be shown.<br>
	 * Otherwise the current viewBox will be re-downloaded from the server.
	 * @param add 
	 */
	private void onMenuDownloadCurrent(boolean add) {
		Log.d("Main", "onMenuDownloadCurrent");
		if (logic.hasChanges() && !add) {
			showDialog(DialogFactory.DOWNLOAD_CURRENT_WITH_CHANGES);
		} else {
			performCurrentViewHttpLoad(add);
		}
	}

	/**
	 * Uses {@link DialogFactory} to create Dialogs<br> {@inheritDoc}
	 */
	@Override
	protected Dialog onCreateDialog(final int id) {
		Log.d("Main", "onCreateDialog");
		Dialog dialog = dialogFactory.create(id);
		if (dialog != null) {
			return dialog;
		}
		return super.onCreateDialog(id);
	}
	
	/**
	 * Prepare the fields of dialogs before they are shown. Only some need this special
	 * handling.
	 * @param id Dialog ID number.
	 * @param dialog Dialog object.
	 */
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		Log.d("Main", "onPrepareDialog");
		super.onPrepareDialog(id, dialog);
		if (dialog instanceof AlertDialog) {
			AlertDialog ad = (AlertDialog)dialog;
			switch (id) {
			case DialogFactory.CONFIRM_UPLOAD:
				TextView changes = (TextView)ad.findViewById(R.id.upload_changes);
				changes.setText(getString(R.string.confirm_upload_text, getPendingChanges()));
				break;
			case DialogFactory.OPENSTREETBUG_EDIT:
				Bug bug = logic.getSelectedBug();
				ad.setTitle(getString((bug.getId() == 0) ? R.string.openstreetbug_new_title : R.string.openstreetbug_edit_title));
				TextView comments = (TextView)ad.findViewById(R.id.openstreetbug_comments);
				comments.setText(Html.fromHtml(bug.getComment())); // ugly
				EditText comment = (EditText)ad.findViewById(R.id.openstreetbug_comment);
				comment.setText("");
				comment.setFocusable( true);
				comment.setFocusableInTouchMode(true);
				comment.setEnabled(true);
				CheckBox close = (CheckBox)ad.findViewById(R.id.openstreetbug_close);
				close.setChecked(bug.isClosed());
				if (bug.isClosed()) {
					close.setText(R.string.openstreetbug_edit_closed);
				} else {
					close.setText(R.string.openstreetbug_edit_close);
				}
				close.setEnabled(/* !bug.isClosed() && */ bug.getId() != 0);
				Button commit = ad.getButton(AlertDialog.BUTTON_POSITIVE);
				commit.setEnabled(/* !bug.isClosed() */ true);
				break;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		Log.d("Main", "onActivityResult");
		super.onActivityResult(requestCode, resultCode, data);
		if (data != null) {
			switch(requestCode) {
			
			case REQUEST_BOUNDINGBOX : 
				handleBoxPickerResult(resultCode, data);				
				break;
				
			case REQUEST_EDIT_TAG :
				if (resultCode == RESULT_OK) {
					handleTagEditorResult(data);
				}
				break;
				
			case REQUEST_EVENT_VIEWER :
				if (resultCode == RESULT_OK) {
					// MARK See whether the EventViewer was closed or if the event should be edited.
					int nextAction = data.getIntExtra(EventViewer.ACTION_EXTRA, -1);

					if (nextAction == EventViewer.ACTION_EDIT) {
						Event event = (Event) data.getSerializableExtra(EventViewer.EVENT_EXTRA);
						Log.d(LOG_TAG, "EventViewer requested to edit the event!");
						if (event == null) Log.e("MARK", "Main activity received null event!");
						else {
							OsmElement osmElement = logic.delegator.getOsmElement(event.getEventType(), event.getTypeId());
							if (osmElement != null) {
								// Already downloaded and ready to display:
								if (osmElement instanceof Node) {
									Log.d(LOG_TAG, "Node already exists. Ready to edit:");
									logic.setSelectedNode((Node) osmElement);

									Intent eventEditorIntent = new Intent(getApplicationContext(), EventEditor.class);
									eventEditorIntent.putExtra(EventEditor.EVENT_EXTRA, new Event(event));
									eventEditorStartedBy = EventViewer.class.getName();
									startActivityForResult(eventEditorIntent, Main.REQUEST_EVENT_EDITOR);
								}
							} else {
								Log.w(LOG_TAG, "Node not there. Should have been downloaded earlier!");
							}
						}
					} else if (nextAction == EventViewer.ACTION_BACK) {
						// Return to the activity that started the EventViewer!
						if (eventViewerStartedBy == Main.class.getName()) {
							Log.d(LOG_TAG, "Returning to Main from EventViewer!");
							// Do nothing
						} else if (eventViewerStartedBy == SearchableActivity.class.getName()) {
							Log.d(LOG_TAG, "Returning to SearchableActivity from EventViewer!");

							Intent searchableActivityIntent = new Intent(getApplicationContext(), SearchableActivity.class);
							searchableActivityIntent.setAction(Intent.ACTION_SEARCH);
							searchableActivityIntent.putExtra(SearchableActivity.EVENT_QUERY_EXTRA, searchableActivityStartedWithEventQuery);
							startActivityForResult(searchableActivityIntent, REQUEST_SEARCHABLE_ACTIVITY);
						} else {
							Log.w(LOG_TAG, "eventViewerStartedBy was not set correctly: " + eventViewerStartedBy);							
						}
						Log.d(LOG_TAG, "EventViewer exited without a request!");						
					} else if (nextAction == EventViewer.ACTION_ADD) {
						Event event = (Event) data.getSerializableExtra(EventViewer.EVENT_EXTRA);
						Log.d(LOG_TAG, "EventViewer requested to add a new event!");
						if (event == null) Log.e("MARK", "Main activity received null event!");
						else {
							OsmElement osmElement = logic.delegator.getOsmElement(event.getEventType(), event.getTypeId());
							if (osmElement != null) {
								// Already downloaded and ready to display:
								if (osmElement instanceof Node) {
									// MARK Find the next unused event number and start event editor with an almost empty event
									Node node = (Node) osmElement;
									
									Event addEvent = node.getNewEvent();
											
									logic.setSelectedNode(node);

									Intent eventEditorIntent = new Intent(getApplicationContext(), EventEditor.class);
									eventEditorIntent.putExtra(EventEditor.EVENT_EXTRA, addEvent);
									eventEditorStartedBy = EventViewer.class.getName();
									startActivityForResult(eventEditorIntent, Main.REQUEST_EVENT_EDITOR);
								}
							} else {
								Log.w(LOG_TAG, "Node not there. Should have been downloaded earlier!");
							}
						}
					} else if (nextAction == EventViewer.ACTION_SHOWONMAP) {
						Event event = (Event) data.getSerializableExtra(EventViewer.EVENT_EXTRA);
						eventOfRelatedItems = event;
						relatedNodes = new HashSet<Long>();
						relatedWays = new HashSet<Long>();
						relatedRelations = new HashSet<Long>();
						Node eventNode = null;
						Way eventWay = null;
						Relation eventRelation = null;
						if (event.getEventType().equalsIgnoreCase(Node.NAME)) {
							eventNode = (Node) logic.delegator.getOsmElement(Node.NAME, event.getTypeId());
							// Also add the original event node to the set of related nodes
							relatedNodes.add(event.getTypeId());
						} else if (event.getEventType().equalsIgnoreCase(Way.NAME)) {
							Log.w(LOG_TAG, "Related items can not be shown for events assigned to ways at the moment!");
//							eventWay = (Way) logic.delegator.getOsmElement(Way.NAME, event.getTypeId());
//							// Also add the original event way to the set of related ways
//							relatedWays.add(event.getTypeId());
						} else if (event.getEventType().equalsIgnoreCase(Relation.NAME)) {
							Log.w(LOG_TAG, "Related items can not be shown for events assigned to relations at the moment!");
//							eventRelation = (Relation) logic.delegator.getOsmElement(Relation.NAME, event.getTypeId());
//							// Also add the original event way to the set of related relations
//							relatedRelations.add(event.getTypeId());
						}
						if (eventNode == null) {
							Log.e(LOG_TAG, "Event node not available in storage for action show on map!");
						} else {
							// Download related items
							Event.extractRelatedItems(event, relatedNodes, relatedWays, relatedRelations);
							downloadRelatedItems();
						}
						Log.d(LOG_TAG, "EventViewer requested to show the event on the map!");
						// Tell event MapOverlay to only display this single event for now!
						Collection<Event> eventList = new LinkedList<Event>();
						eventList.add(event);
						map.getEventsOverlay().showEventList(eventList);
						// Tell map to display the selected event node and its related items
						shouldDrawRelatedItemsOnMap = true;
						adjustOptionsMenu();
						logic.moveViewBox(event.getLon(), event.getLat());
					} else {
						Log.w(LOG_TAG, "EventViewer returned invalid action: " + nextAction);
					}					
				} else {
					Log.w(LOG_TAG, "EventViewer exited with bad result code: " + resultCode);
				}
				break;
				
			case REQUEST_EVENT_EDITOR :
				if (resultCode == RESULT_OK) {
					int nextAction = data.getIntExtra(EventEditor.ACTION_EXTRA, -1);
					Event event = (Event) data.getSerializableExtra(EventEditor.EVENT_EXTRA);
					if (nextAction == EventEditor.ACTION_SAVE) {
						// MARK New event properties need to be reflected in the node tags
						Node node = logic.getSelectedNode();
						if (node == null) {
							Log.d(LOG_TAG, "Selected node is null!");
							if (event.getEventType().equalsIgnoreCase(Node.NAME)) {
								Log.d(LOG_TAG, "Looking up node...");
								node = (Node) logic.delegator.getOsmElement(Node.NAME, event.getTypeId());
							} else {
								Log.w(LOG_TAG, "Not a valid node!");
								Log.w(LOG_TAG, "getEventType():" + event.getEventType());
							}
						}
						if (node != null) {
							Log.d(LOG_TAG, "Node is not null anymore!");
							SortedMap<String, String> nodeTags = node.getTags();
							if (nodeTags != null) {
								Log.d(LOG_TAG, "Tags is not null!");
								SortedMap<String, String> tags = new TreeMap<String, String>(node.getTags());
								// Remove old tags of this event
								String[] eventTags = event.getTagKeys();
								for (String eventTag : eventTags) {
									tags.remove(eventTag);
								}
								// Add new tags of this event
								tags.putAll(event.getTags());

								if (logic.setTags(node.getName(), node.getOsmId(), tags)) {
									Log.d(LOG_TAG, "Altered tags!");							
								} else {
									Log.d(LOG_TAG, "Tags were not altered!");
								}
							}
						}
					} else if (nextAction == EventEditor.ACTION_EDIT_RELATED_ITEMS) {
						// MARK Save the changes so far made in the EventEditor
						Node node = logic.getSelectedNode();
						if (node == null) {
							Log.d(LOG_TAG, "Selected node is null!");
							if (event.getEventType().equalsIgnoreCase(Node.NAME)) {
								if (event.getTypeId() >= 0L) {
									Log.d(LOG_TAG, "Looking up node...");
									Log.d(LOG_TAG, "Node id >= 0");
									node = (Node) logic.delegator.getOsmElement(Node.NAME, event.getTypeId());
								} else {
									Log.d(LOG_TAG, "Looking up node...");
									Log.d(LOG_TAG, "Node id < 0");
									node = (Node) logic.delegator.getOsmElement(Node.NAME, event.getTypeId());
								}
							}
						}
						if (node != null) {
							Log.d(LOG_TAG, "Node is not null anymore!");
							SortedMap<String, String> nodeTags = node.getTags();
							if (nodeTags != null) {
								Log.d(LOG_TAG, "Tags is not null!");
								SortedMap<String, String> tags = new TreeMap<String, String>(node.getTags());
								// Remove old tags of this event
								String[] eventTags = event.getTagKeys();
								for (String eventTag : eventTags) {
									tags.remove(eventTag);
								}
								// Add new tags of this event
								tags.putAll(event.getTags());

								if (logic.setTags(node.getName(), node.getOsmId(), tags)) {
									Log.d(LOG_TAG, "Altered tags!");							
								} else {
									Log.d(LOG_TAG, "Tags were not altered!");
								}
							}
						}
						// Now enable the editing of the related items
						isRelatedItemsEditingEnabled = true;
						eventOfRelatedItems = event;
						adjustOptionsMenu();
						// Tell event MapOverlay to only display this single event for now!
						Collection<Event> eventList = new LinkedList<Event>();
						eventList.add(event);
						map.getEventsOverlay().showEventList(eventList);
						// Make sure all currently related items are available in the storage
						relatedNodes = new HashSet<Long>();
						relatedWays = new HashSet<Long>();
						relatedRelations = new HashSet<Long>();
						Node eventNode = null;
						Way eventWay = null;
						Relation eventRelation = null;
						if (event.getEventType().equalsIgnoreCase(Node.NAME)) {
							eventNode = (Node) logic.delegator.getOsmElement(Node.NAME, event.getTypeId());
							// Also add the original event node to the set of related nodes
								relatedNodes.add(event.getTypeId());
						} else if (event.getEventType().equalsIgnoreCase(Way.NAME)) {
							Log.w(LOG_TAG, "Related items can not be shown for events assigned to ways at the moment!");
//							eventWay = (Way) logic.delegator.getOsmElement(Way.NAME, event.getTypeId());
//							// Also add the original event way to the set of related ways
//							relatedWays.add(event.getTypeId());
						} else if (event.getEventType().equalsIgnoreCase(Relation.NAME)) {
							Log.w(LOG_TAG, "Related items can not be shown for events assigned to relations at the moment!");
//							eventRelation = (Relation) logic.delegator.getOsmElement(Relation.NAME, event.getTypeId());
//							// Also add the original event way to the set of related relations
//							relatedRelations.add(event.getTypeId());
						}
						if (eventNode == null) {
							Log.e(LOG_TAG, "Event node not available in storage for action show on map!");
						} else {
							// Download related items
							Event.extractRelatedItems(event, relatedNodes, relatedWays, relatedRelations);
							downloadRelatedItems();
						}
					} else if (nextAction == EventEditor.ACTION_BACK) {
						// Do nothing
					} else {
						Log.w(LOG_TAG, "EventEditor returned invalid action: " + nextAction);
					}
					if (nextAction == EventEditor.ACTION_SAVE || nextAction == EventEditor.ACTION_BACK) {
						// Return to the activity that started the EventEditor no matter if the event was modified or not!
						if (eventEditorStartedBy.equals(EventViewer.class.getName())) {
							Log.d(LOG_TAG, "Returning to EventViewer from EventEditor!");
							Intent eventViewerIntent = new Intent(getApplicationContext(), EventViewer.class);
							eventViewerIntent.putExtra(EventViewer.EVENT_EXTRA, new Event(event));
							startActivityForResult(eventViewerIntent, Main.REQUEST_EVENT_VIEWER);
						} else if (eventEditorStartedBy.equals(Main.class.getName())) {
							// We have already returned to the main activity!
							Log.d(LOG_TAG, "Already returned to Main from EventEditor!");
						} else {
							Log.w(LOG_TAG, "eventEditorStartedBy was not set correctly: " + eventEditorStartedBy);
						}
					}
				} else {
					Log.w(LOG_TAG, "EventEditor exited with bad result code: " + resultCode);
				}
				break;

			case REQUEST_EVENT_SEARCH :
				if (resultCode == RESULT_OK) {
					int nextAction = data.getIntExtra(EventSearch.ACTION_EXTRA, -1);

					if (nextAction == EventSearch.ACTION_SEARCH) {
						EventQuery eventQuery = (EventQuery) data.getSerializableExtra(EventSearch.EVENT_QUERY_EXTRA);
						
						Intent searchableActivityIntent = new Intent(getApplicationContext(), SearchableActivity.class);
						searchableActivityIntent.setAction(Intent.ACTION_SEARCH);
						searchableActivityIntent.putExtra(SearchableActivity.EVENT_QUERY_EXTRA, eventQuery);
						searchableActivityStartedBy = EventSearch.class.getName();
						searchableActivityStartedWithEventQuery = eventQuery;
						startActivityForResult(searchableActivityIntent, REQUEST_SEARCHABLE_ACTIVITY);
					} else if (nextAction == EventSearch.ACTION_BACK) {
						// Return to the activity that started the EventSearch!
						if (eventSearchStartedBy == Main.class.getName()) {
							Log.d(LOG_TAG, "Returning to Main from EventSearch!");
							// Do nothing
						} else {
							Log.w(LOG_TAG, "eventSearchStartedBy was not set correctly: " + eventSearchStartedBy);							
						}					
					} else {
						Log.w(LOG_TAG, "EventSearch returned invalid action: " + nextAction);
					}					
				} else {
					Log.w(LOG_TAG, "EventSearch exited with bad result code: " + resultCode);
				}
				break;

			case REQUEST_SEARCHABLE_ACTIVITY :
				if (resultCode == RESULT_OK) {
					int nextAction = data.getIntExtra(SearchableActivity.ACTION_EXTRA, -1);

					if (nextAction == SearchableActivity.ACTION_VIEW) {
						Event event = (Event) data.getSerializableExtra(SearchableActivity.EVENT_EXTRA);
						
						// Ensure the element containing the event is available in the storage
						String elementType = event.getEventType();
						if (elementType.equalsIgnoreCase(Node.NAME)) {
							long elementId = event.getTypeId();						
							OsmElement element = logic.delegator.getOsmElement(Node.NAME, elementId);
							if (element == null) {
								logic.downloadElementForEventAndStartViewer(event);
							} else {
								Intent eventViewerIntent = new Intent(getApplicationContext(), EventViewer.class);
								eventViewerIntent.putExtra(EventViewer.EVENT_EXTRA, ((Node) element).getAttachedEvent(event.getNumber()));
								eventViewerStartedBy = SearchableActivity.class.getName();
								startActivityForResult(eventViewerIntent, REQUEST_EVENT_VIEWER);
							}
						} else {
							Log.w(LOG_TAG, "Action currently only supported for nodes!");
						}
					} else if (nextAction == SearchableActivity.ACTION_BACK) {
						// Return to the activity that started the SearchableActivity!
						if (searchableActivityStartedBy == EventSearch.class.getName()) {
							Log.d(LOG_TAG, "Returning to EventSearch from SearchableActivity!");
							Intent searchIntent = new Intent(getApplicationContext(), EventSearch.class);
							searchIntent.putExtra(EventSearch.EVENT_QUERY_EXTRA, searchableActivityStartedWithEventQuery);
							startActivityForResult(searchIntent, REQUEST_EVENT_SEARCH);
						} else {
							Log.w(LOG_TAG, "searchableActivityStartedBy was not set correctly: " + searchableActivityStartedBy);							
						}					
					} else if (nextAction == SearchableActivity.ACTION_CREATE_EVENT) {
						shouldCreateEventFromEventQuery = true;
						createEventFromEventQuery = searchableActivityStartedWithEventQuery;
						Toast.makeText(this, "Long click on the map to create the new event!", Toast.LENGTH_SHORT).show();
					} else {
						Log.w(LOG_TAG, "SearchableActivity returned invalid action: " + nextAction);
					}					
				} else {
					Log.w(LOG_TAG, "SearchableActivity exited with bad result code: " + resultCode);
				}
				break;
				
			case REQUEST_PLACE_SEARCH :
				if (resultCode == RESULT_OK) {
					int nextAction = data.getIntExtra(PlaceSearch.ACTION_EXTRA, -1);

					if (nextAction == PlaceSearch.ACTION_SEARCH) {
						String placeQuery = data.getStringExtra(PlaceSearch.PLACE_QUERY_EXTRA);
						
						Intent searchableActivityIntent = new Intent(getApplicationContext(), PlaceSearchableActivity.class);
						searchableActivityIntent.setAction(Intent.ACTION_SEARCH);
						searchableActivityIntent.putExtra(PlaceSearchableActivity.PLACE_QUERY_EXTRA, placeQuery);
						placeSearchableActivityStartedBy = PlaceSearch.class.getName();
						placeSearchableActivityStartedWithPlaceQuery = placeQuery;
						startActivityForResult(searchableActivityIntent, REQUEST_PLACE_SEARCHABLE_ACTIVITY);
					} else if (nextAction == PlaceSearch.ACTION_BACK) {
						// Return to the activity that started the PlaceSearch!
						if (placeSearchStartedBy == Main.class.getName()) {
							Log.d(LOG_TAG, "Returning to Main from PlaceSearch!");
							// Do nothing
						} else {
							Log.w(LOG_TAG, "placeSearchStartedBy was not set correctly: " + eventSearchStartedBy);							
						}					
					} else {
						Log.w(LOG_TAG, "PlaceSearch returned invalid action: " + nextAction);
					}					
				} else {
					Log.w(LOG_TAG, "PlaceSearch exited with bad result code: " + resultCode);
				}
				break;

			case REQUEST_PLACE_SEARCHABLE_ACTIVITY :
				if (resultCode == RESULT_OK) {
					int nextAction = data.getIntExtra(PlaceSearchableActivity.ACTION_EXTRA, -1);

					if (nextAction == PlaceSearchableActivity.ACTION_VIEW) {
						Nominatim.SearchResult searchResult = (Nominatim.SearchResult) data.getSerializableExtra(PlaceSearchableActivity.SEARCH_RESULT_EXTRA);
						Log.d(LOG_TAG, "PlaceSearchableActivity requested to show the place on the map!");
						logic.moveViewBox(searchResult.lon, searchResult.lat);
					} else if (nextAction == PlaceSearchableActivity.ACTION_BACK) {
						// Return to the activity that started the PlaceSearchableActivity!
						if (placeSearchableActivityStartedBy == PlaceSearch.class.getName()) {
							Log.d(LOG_TAG, "Returning to PlaceSearch from PlaceSearchableActivity!");
							Intent searchIntent = new Intent(getApplicationContext(), PlaceSearch.class);
							searchIntent.putExtra(PlaceSearch.PLACE_QUERY_EXTRA, placeSearchableActivityStartedWithPlaceQuery);
							startActivityForResult(searchIntent, REQUEST_PLACE_SEARCH);
						} else {
							Log.w(LOG_TAG, "placeSearchableActivityStartedBy was not set correctly: " + placeSearchableActivityStartedBy);							
						}					
					} else {
						Log.w(LOG_TAG, "PlaceSearchableActivity returned invalid action: " + nextAction);
					}					
				} else {
					Log.w(LOG_TAG, "PlaceSearchableActivity exited with bad result code: " + resultCode);
				}
				break;
			}			
		}
	}

	@SuppressWarnings("deprecation")
	private void downloadRelatedItems() {
		Log.d(LOG_TAG, "downloadRelatedItems: #relatedNodes = " + relatedNodes.size());
		Log.d(LOG_TAG, "downloadRelatedItems: #relatedWays = " + relatedWays.size());
		Log.d(LOG_TAG, "downloadRelatedItems: #relatedRelations = " + relatedRelations.size());
		// Show the loading dialog.
		showDialog(DialogFactory.PROGRESS_LOADING);
		// Find nodes that need to be downloaded.
		nodesToDownload = new HashSet<Long>();
		for (Long nodeId : relatedNodes) {
			if (logic.delegator.getOsmElement(Node.NAME, nodeId) == null) {
				nodesToDownload.add(nodeId);
			}
		}
		Log.d(LOG_TAG, "downloadRelatedItems: #nodesToDownload = " + nodesToDownload.size());
		// Find ways that need to be downloaded.
		waysToDownload = new HashSet<Long>();
		for (Long wayId : relatedWays) {
			if (logic.delegator.getCurrentStorage().getWay(wayId) == null) {
				waysToDownload.add(wayId);
			}
		}
		Log.d(LOG_TAG, "downloadRelatedItems: #waysToDownload = " + waysToDownload.size());
		if (waysToDownload.size() > 0) {
			// Download ways and see which nodes are referenced
			Set<Long> copyOfWaysToDownload = new HashSet<Long>(waysToDownload);
			for (Long wayId : copyOfWaysToDownload) {
				logic.downloadWayForReferencesOnlyAsync(wayId);
			}
		} else {
			// Download the missing nodes
			if (nodesToDownload.size() > 0) {
				Set<Long> copyOfNodesToDownload = new HashSet<Long>(nodesToDownload);
				for (Long nodeId : copyOfNodesToDownload) {
					logic.downloadNodeAsync(nodeId);
				}
			} else {
				finishDownloadingRelatedItems();
			}
		}
	}

	public void downloadedWayForReferenceOnlyAsync(long id, Way way) {
		// Find referenced nodes that need to be downloaded.
		for (Long nodeId : way.nodeIds) {
			if (logic.delegator.getCurrentStorage().getNode(nodeId) == null) {
				nodesToDownload.add(nodeId);
			}
		}
		Log.d(LOG_TAG, "downloadedWayForReferenceOnlyAsync: #nodesToDownload = " + nodesToDownload.size());
		// Remove downloaded way from the list of ways that need to be downloaded
		waysToDownload.remove(id);
		Log.d(LOG_TAG, "downloadedWayForReferenceOnlyAsync: #waysToDownload = " + waysToDownload.size());
		// Once all missing ways were downloaded start to download the missing nodes.
		if (waysToDownload.isEmpty()) {
			Log.d(LOG_TAG, "downloadedWayForReferenceOnlyAsync: All related ways are downloaded!");
			if (nodesToDownload.size() > 0) {
				Set<Long> copyOfNodesToDownload = new HashSet<Long>(nodesToDownload);
				for (Long nodeId : copyOfNodesToDownload) {
					logic.downloadNodeAsync(nodeId);
				}
			} else {
				for (Long wayId : relatedWays) {
					if (logic.delegator.getCurrentStorage().getWay(wayId) == null) {
						waysToDownload.add(wayId);
					}
				}
				Log.d(LOG_TAG, "downloadedWayForReferenceOnlyAsync: #waysToDownload = " + waysToDownload.size());
				Set<Long> copyOfWaysToDownload = new HashSet<Long>(waysToDownload);
				for (Long wayId : copyOfWaysToDownload) {
					logic.downloadWayAsync(wayId);
				}
			}
		}
	}
	
	public void downloadedNodeAsync(long id) {
		// Remove downloaded node from the list of nodes that need to be downloaded
		nodesToDownload.remove(id);
		Log.d(LOG_TAG, "downloadedNodeAsync: #nodesToDownload = " + nodesToDownload.size());
		// Once all missing nodes were downloaded download the ways if any.
		if (nodesToDownload.isEmpty()) {
			Log.d(LOG_TAG, "downloadedNodeAsync: All related nodes are downloaded!");
			for (Long wayId : relatedWays) {
				if (logic.delegator.getCurrentStorage().getWay(wayId) == null) {
					waysToDownload.add(wayId);
				}
			}
			Log.d(LOG_TAG, "downloadedNodeAsync: #waysToDownload = " + waysToDownload.size());
			if (waysToDownload.size() > 0) {
				Set<Long> copyOfWaysToDownload = new HashSet<Long>(waysToDownload);
				for (Long wayId : copyOfWaysToDownload) {
					logic.downloadWayAsync(wayId);
				}
			} else {
				finishDownloadingRelatedItems();
			}
		}
	}
	
	public void downloadedWayAsync(long id) {
		// Remove downloaded way from the list of ways that need to be downloaded
		waysToDownload.remove(id);
		Log.d(LOG_TAG, "downloadedWayAsync: #waysToDownload = " + waysToDownload.size());
		// Once all missing ways were downloaded dismiss the loading dialog.
		if (waysToDownload.isEmpty()) {
			Log.d(LOG_TAG, "downloadedWayAsync: All related ways are downloaded!");
			finishDownloadingRelatedItems();
		}
	}
	
	@SuppressWarnings("deprecation")
	private void finishDownloadingRelatedItems() {
		Log.d(LOG_TAG, "Finished downloading of related items!");
		dismissDialog(DialogFactory.PROGRESS_LOADING);
		// When related items should be edited also all elements in the events vicinity need to be downloaded
		if (isRelatedItemsEditingEnabled) {
			int lat = eventOfRelatedItems.getLat();
			int lon = eventOfRelatedItems.getLon();
			BoundingBox vicinity;
			try {
				vicinity = new BoundingBox(lat, lon, 10000);
				logic.downloadBox(vicinity, true);
				logic.moveViewBox(lon, lat);
			} catch (OsmException e) {
			}
		}
		map.invalidate();
	}

	public void startEventViewerForEvent(Event event) {		
		String osmType = event.getEventType();
		long osmId = event.getTypeId();
		long eventId = event.getNumber();
		if (osmType.equalsIgnoreCase(Node.NAME)) {	
			Node eventNode = (Node) logic.delegator.getOsmElement(Node.NAME, osmId);
			if (eventNode != null) {	
				logic.setSelectedNode(eventNode);
				// Extract the event from the node to have all information available
				Event event2 = eventNode.getAttachedEvent(eventId);
				Intent eventViewerIntent = new Intent(getApplicationContext(), EventViewer.class);
				eventViewerIntent.putExtra(EventViewer.EVENT_EXTRA, new Event(event2));
				eventViewerStartedBy = SearchableActivity.class.getName();
				startActivityForResult(eventViewerIntent, Main.REQUEST_EVENT_VIEWER);
				return;
			} else {
				Log.w(LOG_TAG, "startEventViewerForEvent: Node not available!");
			}
		} else if (osmType.equalsIgnoreCase(Way.NAME)) {
			// MARK TODO
			Log.w(LOG_TAG, "startEventViewerForEvent: Not yet implemented for ways!");
		} else if (osmType.equalsIgnoreCase(Relation.NAME)) {
			// MARK TODO
			Log.w(LOG_TAG, "startEventViewerForEvent: Not yet implemented for relations!");
		} else {
			Log.w(LOG_TAG, "startEventViewerForEvent: Invalid element type: " + osmType);
		}
	}
	/**
	 * @param resultCode
	 * @param data
	 */
	private void handleBoxPickerResult(final int resultCode, final Intent data) {
		Bundle b = data.getExtras();
		int left = b.getInt(BoxPicker.RESULT_LEFT);
		int bottom = b.getInt(BoxPicker.RESULT_BOTTOM);
		int right = b.getInt(BoxPicker.RESULT_RIGHT);
		int top = b.getInt(BoxPicker.RESULT_TOP);

		try {
			BoundingBox box = new BoundingBox(left, bottom, right, top);
			if (resultCode == RESULT_OK) {
				performHttpLoad(box);
			} else if (resultCode == RESULT_CANCELED) { // pointless, box will not be valid in this case
				openEmptyMap(box);
			}
		} catch (OsmException e) {
			//Values should be done checked in LocationPciker.
			Log.e(DEBUG_TAG, "OsmException", e);
		}
	}

	/**
	 * @param data
	 */
	private void handleTagEditorResult(final Intent data) {
		Bundle b = data.getExtras();
		if (b != null && b.containsKey(TagEditor.TAGEDIT_DATA)) {
			// Read data from extras
			TagEditorData editorData = (TagEditorData) b.getSerializable(TagEditor.TAGEDIT_DATA);
			if (editorData.tags != null) {
				logic.setTags(editorData.type, editorData.osmId, editorData.tags);
			}
			if (editorData.parents != null) {
				logic.updateParentRelations(editorData.type, editorData.osmId, editorData.parents);
			}
			if (editorData.members != null && editorData.type.equals(Relation.NAME)) {
				logic.updateRelation(editorData.osmId, editorData.members);
			}
			map.invalidate();
		}
	}
	
	@Override
	public void onLowMemory() {
		Log.d("Main", "onLowMemory");
		super.onLowMemory();
		map.onLowMemory();
	}

	/**
	 * TODO: put this in Logic!!! Checks if a serialized {@link StorageDelegator} file is available.
	 * 
	 * @return true, when the file is available, otherwise false.
	 */
	private boolean isLastActivityAvailable() {
		FileInputStream in = null;
		try {
			in = openFileInput(StorageDelegator.FILENAME);
			return true;
		} catch (final FileNotFoundException e) {
			return false;
		} finally {
			SavingHelper.close(in);
		}
	}

	public void performCurrentViewHttpLoad(boolean add) {
		logic.downloadCurrent(add);
	}

	private void performHttpLoad(final BoundingBox box) {
		logic.downloadBox(box, false);
	}

	private void openEmptyMap(final BoundingBox box) {
		logic.newEmptyMap(box);
	}

	/**
	 * 
	 */
	public void performUpload(final String comment, final String source) {
		dismissDialog(DialogFactory.CONFIRM_UPLOAD);
		final Server server = prefs.getServer();

		if (server != null && server.isLoginSet()) {
			if (logic.hasChanges()) {
				logic.upload(comment, source);
				logic.checkForMail();
			} else {
				Toast.makeText(getApplicationContext(), R.string.toast_no_changes, Toast.LENGTH_LONG).show();
			}
		} else {
			showDialog(DialogFactory.NO_LOGIN_DATA);
		}
	}
	
	/**
	 * Commit changes to the currently selected OpenStreetBug.
	 * @param comment Comment to add to the bug.
	 * @param close Flag to indicate if the bug is to be closed.
	 */
	public void performOpenStreetBugCommit(final String comment, final boolean close) {
		Log.d("Vespucci", "Main.performOpenStreetBugCommit");
		dismissDialog(DialogFactory.OPENSTREETBUG_EDIT);
		new CommitTask(logic.getSelectedBug(), comment, close) {
			
			/** Flag to track if the bug is new. */
			private boolean newBug;
			
			@Override
			protected void onPreExecute() {
				newBug = (bug.getId() == 0);
				setSupportProgressBarIndeterminateVisibility(true);
			}
			
			@Override
			protected Boolean doInBackground(Server... args) {
				// execute() is called below with no arguments (args will be empty)
				// getDisplayName() is deferred to here in case a lengthy OSM query
				// is required to determine the nickname
				
				Server server = prefs.getServer();
				
				return super.doInBackground(server);
			}
			
			@Override
			protected void onPostExecute(Boolean result) {
				if (result && newBug) {
					for (OpenStreetMapViewOverlay o : map.getOverlays()) {
						if (o instanceof de.blau.android.osb.MapOverlay) {
							((de.blau.android.osb.MapOverlay)o).addBug(bug);
						}
					}
				}
				setSupportProgressBarIndeterminateVisibility(false);
				Toast.makeText(getApplicationContext(), result ? R.string.openstreetbug_commit_ok : R.string.openstreetbug_commit_fail, Toast.LENGTH_SHORT).show();
				map.invalidate();
			}
			
		}.execute();
	}

	/**
	 * 
	 */
	public void confirmUpload() {
		final Server server = prefs.getServer();

		if (server != null && server.isLoginSet()) {
			if (server.needOAuthHandshake()) {
				oAuthHandshake(server);
			} else {
				if (logic.hasChanges()) {
					showDialog(DialogFactory.CONFIRM_UPLOAD);
				} else {
					Toast.makeText(getApplicationContext(), R.string.toast_no_changes, Toast.LENGTH_LONG).show();
				}
			}
		} else {
			showDialog(DialogFactory.NO_LOGIN_DATA);
		}
	}
	
	

	private void oAuthHandshake(Server server) {
		Server[] s = {server};
		AsyncTask<Server, Void, Boolean> loader = new AsyncTask<Server, Void, Boolean>() {
				
			@Override
			protected void onPreExecute() {
				Log.d("Main", "oAuthHandshake onPreExecute");
			}
			
			@Override
			protected Boolean doInBackground(Server... s) {
				String url = s[0].getBaseURL();
				OAuthHelper oa = new OAuthHelper(url);
				Log.d("Main", "oauth auth url " + url);
				try {
					String authUrl = oa.getRequestToken();
					Log.d("Main", "authURl " + authUrl);
					Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(authUrl));
					startActivity(myIntent);
					return Boolean.valueOf(true);
				} catch (Exception e) {
					Log.d("Main", "OAuth handshake failed");
					e.printStackTrace();
				}
				return Boolean.valueOf(false);
			}
			
			@Override
			protected void onPostExecute(Boolean result) {
				Log.d("Main", "oAuthHandshake onPostExecute");
				if (!result.booleanValue()) Toast.makeText(getApplicationContext(), R.string.toast_oauth_handshake_failed, Toast.LENGTH_LONG).show();
			}
		};
		loader.execute(s);	
	}

	/**
	 * Starts the LocationPicker activity for requesting a location.
	 */
	public void gotoBoxPicker() {
		Intent intent = new Intent(getApplicationContext(), BoxPicker.class);
		if (logic.hasChanges()) {
			DialogFactory.createDataLossActivityDialog(this, intent, REQUEST_BOUNDINGBOX).show();
		} else {
			startActivityForResult(intent, REQUEST_BOUNDINGBOX);
		}
	}

	private enum AppendMode {
		APPEND_START, APPEND_APPEND
	}

	/**
	 * @param selectedElement
	 * @param focusOn if not null focus on the value field of this key
	 */
	public void performTagEdit(final OsmElement selectedElement, String focusOn) {
		if (selectedElement instanceof Node) {
			logic.setSelectedNode((Node) selectedElement);
		} else if (selectedElement instanceof Way) {
			logic.setSelectedWay((Way) selectedElement);
		}
	
		if (selectedElement != null) {
			if (logic.delegator.getOsmElement(selectedElement.getName(), selectedElement.getOsmId()) != null) {
				Intent startTagEditor = new Intent(getApplicationContext(), TagEditor.class);
				startTagEditor.putExtra(TagEditor.TAGEDIT_DATA, new TagEditorData(selectedElement, focusOn));
				startActivityForResult(startTagEditor, Main.REQUEST_EDIT_TAG);
			}
		}
	}
	
	/**
	 * Starts a new activity to display the properties of an event.
	 * 
	 * @param event The {@link Event} to be viewed
	 */
	public void startEventViewer(final Event event) {
		Intent eventViewerIntent = new Intent(getApplicationContext(), EventViewer.class);
		eventViewerIntent.putExtra(EventViewer.EVENT_EXTRA, new Event(event));
		startActivityForResult(eventViewerIntent, Main.REQUEST_EVENT_VIEWER);
	}

	public void startEventEditor(Event event) {
		// assume the logic has already selected the node to which the event will be added
		Node selectedElement = logic.getSelectedNode();
		if (logic.delegator.getOsmElement(selectedElement.getName(), selectedElement.getOsmId()) != null) {
			Intent eventEditorIntent = new Intent(getApplicationContext(), EventEditor.class);

			if (event == null) {
				event = new Event();
				event.setLat(selectedElement.getLat());
				event.setLon(selectedElement.getLon());
				event.setNumber(0L); // say this is the first event for the current node
			}
			event.setEventType(Node.NAME);
			event.setTypeId(selectedElement.getOsmId());
			
			eventEditorIntent.putExtra(EventEditor.EVENT_EXTRA, new Event(event));
			eventEditorStartedBy = Main.class.getName();
			startActivityForResult(eventEditorIntent, Main.REQUEST_EVENT_EDITOR);
		}
	}
	
	/**
	 * potentially do some special stuff for invoking undo and exiting
	 */
	@Override
	public void onBackPressed() {
		// super.onBackPressed();
		Log.d("Main","onBackPressed()");
		String name = logic.getUndo().undo();
		if ((name != null) && (prefs.useBackForUndo())) {
			Toast.makeText(Main.this, getResources().getString(R.string.undo) + ": " + name, Toast.LENGTH_SHORT).show();
		} else {
		    new AlertDialog.Builder(this)
	        .setTitle(R.string.exit_title)
	        .setMessage(R.string.exit_text)
	        .setNegativeButton(R.string.no, null)
	        .setPositiveButton(R.string.yes, 
	        	new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface arg0, int arg1) {
		                Main.super.onBackPressed();
		            }
	        }).create().show();
		}
	}
	
	/**
	 * catch back button in action modes where onBackPressed is not invoked
	 * this is probably not guaranteed to work and will not in android 3.something
	 */
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
	    if(easyEditManager.isProcessingAction()) {
	        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
	        	if (easyEditManager.handleBackPressed())
	        		return true;
	        }
	    }
	    return super.dispatchKeyEvent(event);
	}
	
	public class UndoListener implements OnClickListener, OnLongClickListener {

		@Override
		public void onClick(View arg0) {
			String name = logic.getUndo().undo();
			if (name != null) {
				Toast.makeText(Main.this, getResources().getString(R.string.undo) + ": " + name, Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(Main.this, getResources().getString(R.string.undo_nothing), Toast.LENGTH_SHORT).show();
			}
			map.invalidate();
		}

		@Override
		public boolean onLongClick(View v) {
			UndoStorage undo = logic.getUndo();
			if (undo.canUndo() || undo.canRedo()) {
				UndoDialogFactory.showUndoDialog(Main.this, undo);
			} else {
				Toast.makeText(Main.this, getResources().getString(R.string.undo_nothing), Toast.LENGTH_SHORT).show();				
			}
			return true;
		}
	}

	/**
	 * A TouchListener for all gestures made on the touchscreen.
	 * 
	 * @author mb
	 */
	private class MapTouchListener implements OnTouchListener, VersionedGestureDetector.OnGestureListener, OnCreateContextMenuListener, OnMenuItemClickListener {

		private AppendMode appendMode;

		private List<OsmElement> clickedNodesAndWays;
		private List<Bug> clickedBugs;
		private List<Photo> clickedPhotos;
		private List<Event> clickedEvents;
		
		@Override
		public boolean onTouch(final View v, final MotionEvent m) {
			if (m.getAction() == MotionEvent.ACTION_DOWN) {
				clickedBugs = null;
				clickedPhotos = null;
				clickedNodesAndWays = null;
				logic.handleTouchEventDown(m.getX(), m.getY());
			}
			mDetector.onTouchEvent(v, m);
			return v.onTouchEvent(m);
		}
		
		@Override
		public void onDown(View v, float x, float y) {
			// DO NOTHING
		}
		
		@Override
		public void onClick(View v, float x, float y) {
			if (addingNewEvent || shouldCreateEventFromEventQuery) {
				if (addingNewEvent) {
					addingNewEvent = false;
				}
				// MARK Add a new event to the clicked location
					logic.addEventNode(x,y);
					if (shouldCreateEventFromEventQuery) {
						shouldCreateEventFromEventQuery = false;
						
						Node selectedElement = logic.getSelectedNode();
						Event event = new Event();
						event.setLat(selectedElement.getLat());
						event.setLon(selectedElement.getLon());
						event.setNumber(0L);
						if (createEventFromEventQuery != null) {
							String name = createEventFromEventQuery.name;
							if (name != null) {
								event.setName(name);
							}
							String category = createEventFromEventQuery.category;
							if (category != null && !category.equals(getResources().getStringArray(R.array.event_tag_category_values_with_all)[0])) {
								event.setCategory(category);
							}
							Date startdate = createEventFromEventQuery.startdate;
							if (startdate != null) {
								event.setStartdate(startdate);
							}
							Date enddate = createEventFromEventQuery.enddate;
							if (enddate != null) {
								event.setEnddate(enddate);
							}
						} else {
							Log.w(LOG_TAG, "onLongClick(): Event query is null! This should never happen!");
						}

						event.setEventType(Node.NAME);
						event.setTypeId(selectedElement.getOsmId());
						
						Intent eventEditorIntent = new Intent(getApplicationContext(), EventEditor.class);
						eventEditorIntent.putExtra(EventEditor.EVENT_EXTRA, event);
						eventEditorStartedBy = Main.class.getName();
						startActivityForResult(eventEditorIntent, Main.REQUEST_EVENT_EDITOR);
					} else {
						startEventEditor(null);				
					}
					
					return;
			}
			
			if (isRelatedItemsEditingEnabled) {
				
				clickedNodesAndWays = logic.getClickedNodesAndWays(x, y);
				switch (clickedNodesAndWays.size()) {
				case 0:
					break;
				case 1:
					// exactly one element touched
					changeRelatedStatusOfElement(clickedNodesAndWays.get(0));
					break;
				default:
					// multiple possible elements touched - show menu
					clickedBugs = null;
					clickedPhotos = null;
					Log.d(LOG_TAG, "Multiple elements were clicked!");
					if (menuRequired()) {
						v.showContextMenu();
					} else {
						// menuRequired tells us it's ok to just take the first one
						changeRelatedStatusOfElement(clickedNodesAndWays.get(0));
					}
					break;
				}				
				return;
			}
			de.blau.android.osb.MapOverlay osbo = map.getOpenStreetBugsOverlay();
			clickedBugs = (osbo != null) ? osbo.getClickedBugs(x, y, map.getViewBox()) : null;
			
			de.blau.android.photos.MapOverlay photos = map.getPhotosOverlay();
			clickedPhotos = (photos != null) ? photos.getClickedPhotos(x, y, map.getViewBox()) : null;

			de.tum.bgu.lfk.openeventmap.MapOverlay events = map.getEventsOverlay();
			clickedEvents = (events != null) ? events.getClickedEvents(x, y, map.getViewBox()) : null;
			
			Mode mode = logic.getMode();
			boolean isInEditZoomRange = logic.isInEditZoomRange();
			
			if (showGPS && !followGPS && map.getLocation() != null) {
				// check if this was a click on the GPS mark use the same calculations we use all over the place ... really belongs in a separate method 
				final float tolerance = Profile.getCurrent().nodeToleranceValue;				
				float differenceX = Math.abs(GeoMath.lonE7ToX(map.getWidth(), map.getViewBox(), (int)(map.getLocation().getLongitude() * 1E7)) - x);
				float differenceY = Math.abs(GeoMath.latE7ToY(map.getHeight(), map.getViewBox(), (int)(map.getLocation().getLatitude() * 1E7)) - y);
				if ((differenceX <= tolerance) && (differenceY <= tolerance)) {
					if (Math.hypot(differenceX, differenceY) <= tolerance) {
						setFollowGPS(true);
						map.invalidate();
						return;
					}
				}
			}
			
			if (isInEditZoomRange) {
				switch (mode) {
				case MODE_MOVE:
					if (clickedEvents != null && clickedEvents.size() > 0) {
						// Step 0: Check whether events from multiple OSM elements where clicked
						HashSet<Long> nSet = new HashSet<Long>();
						HashSet<Long> wSet = new HashSet<Long>();
						HashSet<Long> rSet = new HashSet<Long>();
						
						// Note: These events were downloaded from the SQL database and have different
						// values for the eventType then the OSM elements have!
						for (Event event : clickedEvents) {
							if (event.getEventType().equalsIgnoreCase("node")) {
								nSet.add(Long.valueOf(event.getTypeId()));
								continue;
							}
							if (event.getEventType().equalsIgnoreCase("way")) {
								wSet.add(Long.valueOf(event.getTypeId()));
								continue;
							}
							if (event.getEventType().equalsIgnoreCase("rel")) {
								rSet.add(Long.valueOf(event.getTypeId()));
								continue;
							}							
						}
						
						int nClickedElements = nSet.size() + wSet.size() + rSet.size();
						if (nClickedElements == 1) {
							Log.d(LOG_TAG, "Exactly one element was clicked!");
							if (nSet.size() == 1) {
								onEventElementSelected(Node.NAME + ":" + String.valueOf(nSet.iterator().next()));
								return;
							}
							if (wSet.size() == 1) {
								onEventElementSelected(Way.NAME + ":" + String.valueOf(wSet.iterator().next()));
								return;
							}
							if (rSet.size() == 1) {
								onEventElementSelected(Relation.NAME + ":" + String.valueOf(rSet.iterator().next()));
								return;
							}
						}
						if (nClickedElements > 1) {
							Log.d(LOG_TAG, "More than one element was clicked!");
							// Step 0?true: Let user select the right OSM element
							elementChoice = new LinkedList<OsmElementIdentifier>();
							for (Long elementId : nSet) {
								elementChoice.add(new OsmElementIdentifier(Node.NAME, elementId, ""));
							}
							for (Long elementId : wSet) {
								elementChoice.add(new OsmElementIdentifier(Way.NAME, elementId, ""));
							}
							for (Long elementId : rSet) {
								elementChoice.add(new OsmElementIdentifier(Relation.NAME, elementId, ""));
							}
							logic.downloadElementChoices(elementChoice);
						}
					}
					break;
				case MODE_OPENSTREETBUG:
					switch ((clickedBugs == null) ? 0 : clickedBugs.size()) {
					case 0:
						performBugEdit(logic.makeNewBug(x, y));
						break;
					case 1:
						performBugEdit(clickedBugs.get(0));
						break;
					default:
						v.showContextMenu();
						break;
					}
					break;
				case MODE_ADD:
					logic.performAdd(x, y);
					break;
				case MODE_TAG_EDIT:
					selectElementForTagEdit(v, x, y);
					break;
				case MODE_ERASE:
					selectElementForErase(v, x, y);
					break;
				case MODE_SPLIT:
					selectElementForSplit(v, x, y);
					break;
				case MODE_APPEND:
					performAppend(v, x, y);
					break;
				case MODE_EASYEDIT:
					performEasyEdit(v, x, y);
					break;
				}
				map.invalidate();
			} else {
				Toast.makeText(getApplicationContext(), "Zoom closer to select an event!", Toast.LENGTH_SHORT).show();
			}
		}
		
		private void changeRelatedStatusOfElement(OsmElement element) {
			if (element instanceof Node) {
				Long id = element.getOsmId();
				if (relatedNodes.contains(id)) {
					relatedNodes.remove(id);
				} else {
					relatedNodes.add(id);
				}
			}
			if (element instanceof Way) {
				Long id = element.getOsmId();
				if (relatedWays.contains(id)) {
					relatedWays.remove(id);
				} else {
					relatedWays.add(id);
				}
			}
			if (element instanceof Relation) {
				Long id = element.getOsmId();
				if (relatedRelations.contains(id)) {
					relatedRelations.remove(id);
				} else {
					relatedRelations.add(id);
				}
			}
			map.invalidate();
		}

		private void viewPhoto(Photo photo) {
			try {
				Intent myIntent = new Intent(Intent.ACTION_VIEW); 
				myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
				myIntent.setDataAndType(photo.getRef(), "image/jpeg"); // black magic only works this way
				startActivity(myIntent);
				map.getPhotosOverlay().setSelected(photo);
				//TODO may need a map.invalidate() here
			} catch (Exception ex) {
				Log.d("Main", "viewPhoto exception starting intent: " + ex);	
			}
		}

		@Override
		public void onUp(View v, float x, float y) {
			if (logic.getMode() == Mode.MODE_EASYEDIT) {
				easyEditManager.invalidate();
			}
		}
		
		@Override
		public boolean onLongClick(final View v, final float x, final float y) {
//			if (isRelatedItemsEditingEnabled) {
//				return true;
//			}
//			// MARK Handle long click for event creation
//			if (logic.getMode() != Mode.MODE_EASYEDIT) {
//				logic.addEventNode(x,y);
//				if (shouldCreateEventFromEventQuery) {
//					shouldCreateEventFromEventQuery = false;
//					
//					Node selectedElement = logic.getSelectedNode();
//					Event event = new Event();
//					event.setLat(selectedElement.getLat());
//					event.setLon(selectedElement.getLon());
//					event.setNumber(0L);
//					if (createEventFromEventQuery != null) {
//						String name = createEventFromEventQuery.name;
//						if (name != null) {
//							event.setName(name);
//						}
//						String category = createEventFromEventQuery.category;
//						if (category != null && !category.equals(getResources().getStringArray(R.array.event_tag_category_values_with_all)[0])) {
//							event.setCategory(category);
//						}
//						Date startdate = createEventFromEventQuery.startdate;
//						if (startdate != null) {
//							event.setStartdate(startdate);
//						}
//						Date enddate = createEventFromEventQuery.enddate;
//						if (enddate != null) {
//							event.setEnddate(enddate);
//						}
//					} else {
//						Log.w(LOG_TAG, "onLongClick(): Event query is null! This should never happen!");
//					}
//					
//					Intent eventEditorIntent = new Intent(getApplicationContext(), EventEditor.class);
//					eventEditorIntent.putExtra(EventEditor.EVENT_EXTRA, event);
//					eventEditorStartedBy = Main.class.getName();
//					startActivityForResult(eventEditorIntent, Main.REQUEST_EVENT_EDITOR);
//				} else {
//					startEventEditor(null);				
//				}
//					
//				return true;
//			}
//			
//			if (logic.isInEditZoomRange()) {
//				return easyEditManager.handleLongClick(v, x, y);
//			} else {
//				Toast.makeText(getApplicationContext(), R.string.toast_not_in_edit_range, Toast.LENGTH_LONG).show();
//			}

//			Toast.makeText(Main.this, "Long clicks are now ignored.", Toast.LENGTH_LONG).show();
			
			return true; // long click handled
		}

		@Override
		public void onDrag(View v, float x, float y, float dx, float dy) {
			
			logic.handleTouchEventMove(x, y, -dx, dy);
			setFollowGPS(false);
		}
		
		@Override
		public void onScale(View v, float scaleFactor, float prevSpan, float curSpan) {
			logic.zoom((curSpan - prevSpan) / prevSpan);
			updateZoomControls();
		}
		
		private void selectElementForTagEdit(final View v, final float x, final float y) {
			clickedNodesAndWays = logic.getClickedNodesAndWays(x, y);
			switch (((clickedBugs == null) ? 0 : clickedBugs.size()) + clickedNodesAndWays.size()) {
			case 0:
				// no elements were touched, ignore
				break;
			case 1:
				// exactly one element touched
				if (clickedBugs != null && clickedBugs.size() == 1) {
					performBugEdit(clickedBugs.get(0));
				} else {
					performTagEdit(clickedNodesAndWays.get(0), null);
				}
				break;
			default:
				// multiple possible elements touched - show menu
				v.showContextMenu();
				break;
			}
		}

		private void selectElementForErase(final View v, final float x, final float y) {
			clickedNodesAndWays = logic.getClickedNodes(x, y);
			switch (clickedNodesAndWays.size()) {
			case 0:
				// no elements were touched, ignore
				break;
			case 1:
				Log.i("Delete mode", "delete node");
				if (clickedNodesAndWays.get(0).hasParentRelations()) {
					Log.i("Delete mode", "node has relations");
					new AlertDialog.Builder(runningInstance)
						.setTitle(R.string.delete)
						.setMessage(R.string.deletenode_relation_description)
						.setPositiveButton(R.string.deletenode,
							new DialogInterface.OnClickListener() {	
								@Override
								public void onClick(DialogInterface dialog, int which) {
									logic.performEraseNode((Node)clickedNodesAndWays.get(0));
								}
							})
						.show();
				} else {
					logic.performEraseNode((Node)clickedNodesAndWays.get(0));
				}
				break;
			default:
				v.showContextMenu();
				break;
			}
		}

		private void selectElementForSplit(final View v, final float x, final float y) {
			clickedNodesAndWays = logic.getClickedNodes(x, y);

			//TODO remove nodes with no ways from list
//			for (Iterator iterator = clickedNodesAndWays.iterator(); iterator.hasNext();) {
//				Node node = (Node) iterator.next();
//				if (node.getWaysCount() < 1) {
//					iterator.remove();
//				}
//			}

			switch (clickedNodesAndWays.size()) {
			case 0:
				// no elements were touched, ignore
				break;
			case 1:
				logic.performSplit((Node)clickedNodesAndWays.get(0));
				break;
			default:
				v.showContextMenu();
				break;
			}
		}

		/**
		 * Appends a new Node to a selected Way. If any Way was yet selected, the user have to select one end-node
		 * first. When the user clicks on an empty area, a new node will generated. When he clicks on a existing way,
		 * the new node will be generated on that way. when he selects a different node, this one will be used. when he
		 * selects the previous selected node, it will be de-selected.
		 * 
		 * @param x the click-position on the display.
		 * @param y the click-position on the display.
		 */
		public void performAppend(final View v, final float x, final float y) {
			Node lSelectedNode = logic.getSelectedNode();
			Way lSelectedWay = logic.getSelectedWay();

			if (lSelectedWay == null) {
				clickedNodesAndWays = logic.getClickedEndNodes(x, y);
				switch (clickedNodesAndWays.size()) {
				case 0:
					// no elements touched, ignore
					break;
				case 1:
					logic.performAppendStart(clickedNodesAndWays.get(0));
					break;
				default:
					appendMode = AppendMode.APPEND_START;
					v.showContextMenu();
					break;
				}
			} else if (lSelectedWay.isEndNode(lSelectedNode)) {
				// TODO Resolve multiple possible selections
				logic.performAppendAppend(x, y);
			}
		}
		
		/**
		 * Perform easy edit touch processing.
		 * @param v
		 * @param x the click-position on the display.
		 * @param y the click-position on the display.
		 */
		public void performEasyEdit(final View v, final float x, final float y) {
			if (!easyEditManager.actionModeHandledClick(x, y)) {
				clickedNodesAndWays = logic.getClickedNodesAndWays(x, y);
				switch (((clickedBugs == null) ? 0 : clickedBugs.size()) + clickedNodesAndWays.size() + ((clickedPhotos == null)? 0 : clickedPhotos.size())) {
				case 0:
					// no elements were touched
					easyEditManager.nothingTouched();
					break;
				case 1:
					// exactly one element touched
					if (clickedBugs != null && clickedBugs.size() == 1) {
						performBugEdit(clickedBugs.get(0));
					}
					else if (clickedPhotos != null && clickedPhotos.size() == 1) {
						viewPhoto(clickedPhotos.get(0));
					} else {
						easyEditManager.editElement(clickedNodesAndWays.get(0));
					}
					break;
				default:
					// multiple possible elements touched - show menu
					if (menuRequired()) {
						v.showContextMenu();
					} else {
						// menuRequired tells us it's ok to just take the first one
						easyEditManager.editElement(clickedNodesAndWays.get(0));
					}
					break;
				}
			}
		}

		/**
		 * Edit an OpenStreetBug.
		 * @param bug The bug to edit.
		 */
		private void performBugEdit(final Bug bug) {
			Log.d("Vespucci", "editing bug:"+bug);
			final Server server = prefs.getServer();
			if (server != null && server.isLoginSet()) {
				if (server.needOAuthHandshake()) {
					oAuthHandshake(server);
				} else {
					logic.setSelectedBug(bug);
					showDialog(DialogFactory.OPENSTREETBUG_EDIT);
				}
			} else {
				showDialog(DialogFactory.NO_LOGIN_DATA);
			}
		}
		
		@Override
		public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
			onCreateDefaultContextMenu(menu, v, menuInfo);
		}
			
		public void onCreateDefaultContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
			int id = 0;
			if (clickedPhotos != null) {
				for (Photo p : clickedPhotos) {
					menu.add(Menu.NONE, id++, Menu.NONE, p.getRef().getLastPathSegment()).setOnMenuItemClickListener(this);
				}
			}
			if (clickedBugs != null) {
				for (Bug b : clickedBugs) {
					menu.add(Menu.NONE, id++, Menu.NONE, b.getDescription()).setOnMenuItemClickListener(this);
				}
			}
			if (clickedNodesAndWays != null) {
				for (OsmElement e : clickedNodesAndWays) {
					menu.add(Menu.NONE, id++, Menu.NONE, e.getDescription()).setOnMenuItemClickListener(this);
				}
			}
		}

		/**
		 * Checks if a menu should be shown based on clickedNodesAndWays and clickedBugs.
		 * ClickedNodesAndWays needs to contain nodes first, then ways, ordered by distance from the click.
		 * Assumes multiple elements have been clicked, i.e. a choice is necessary unless heuristics work.
		 */
		private boolean menuRequired() {
			// If the context menu setting requires the menu, show it instead of guessing.
			if (prefs.getForceContextMenu()) return true;
			
			// If bugs are clicked, user should always choose
			if (clickedBugs != null && clickedBugs.size() > 0) return true;
			
			// If photos are clicked, user should always choose
			if (clickedPhotos != null && clickedPhotos.size() > 0) return true;
			
			if (clickedNodesAndWays.size() < 2) {
				Log.e("Main", "WTF? menuRequired called for single item?");
				return true;
			}
			
			// No bugs were clicked. Do we have nodes?
			if (clickedNodesAndWays.get(0) instanceof Node) {
				// Usually, we just take the first node.
				// However, check for *very* closely overlapping nodes first.
				Node candidate = (Node) clickedNodesAndWays.get(0);
				float nodeX = logic.getNodeScreenX(candidate);
				float nodeY = logic.getNodeScreenY(candidate);
				for (int i = 1; i < clickedNodesAndWays.size(); i++) {
					if (!(clickedNodesAndWays.get(i) instanceof Node)) break;
					Node possibleNeighbor = (Node)clickedNodesAndWays.get(i);
					float node2X = logic.getNodeScreenX(possibleNeighbor);
					float node2Y = logic.getNodeScreenY(possibleNeighbor);
					// Fast "square" checking is good enough
					if (Math.abs(nodeX-node2X) < Profile.NODE_OVERLAP_TOLERANCE_VALUE ||
						Math.abs(nodeY-node2Y) < Profile.NODE_OVERLAP_TOLERANCE_VALUE ) {
							// The first node has an EXTREMELY close neighbour. Show context menu
							return true;
					}
				}
				return false; // no colliding neighbours found
			}
			
			// No nodes means we have at least two ways. Since the tolerance for ways is tiny, show the menu.
			return true;
		}
		
		@Override
		public boolean onMenuItemClick(final android.view.MenuItem item) {
			int itemId = item.getItemId();
			int bugsItemId = itemId - ((clickedPhotos == null) ? 0 : clickedPhotos.size());
			if ((clickedPhotos != null) && (itemId < clickedPhotos.size())) {
				viewPhoto(clickedPhotos.get(itemId));
			} else if (clickedBugs != null && bugsItemId >= 0 && bugsItemId < clickedBugs.size()) {
				performBugEdit(clickedBugs.get(bugsItemId));
			} else {
				// this is dependent on which order items where added to the context menu
				itemId -= (((clickedBugs == null) ? 0 : clickedBugs.size() ) + ((clickedPhotos == null) ? 0 : clickedPhotos.size()));
				
				if ((itemId >= 0) && (clickedNodesAndWays != null) && (itemId < clickedNodesAndWays.size())) {
					final OsmElement element = clickedNodesAndWays.get(itemId);
					if (isRelatedItemsEditingEnabled) {
						changeRelatedStatusOfElement(element);
						return true;
					}
					switch (logic.getMode()) {
					case MODE_TAG_EDIT:
						performTagEdit(element, null);
						break;
					case MODE_ERASE:
						if (element.hasParentRelations()) {
							Log.i("Delete mode", "node has relations");
							new AlertDialog.Builder(runningInstance)
								.setTitle(R.string.delete)
								.setMessage(R.string.deletenode_relation_description)
								.setPositiveButton(R.string.deletenode,
									new DialogInterface.OnClickListener() {	
										@Override
										public void onClick(DialogInterface dialog, int which) {
											logic.performEraseNode((Node)element);
										}
									})
								.show();
						} else {
							logic.performEraseNode((Node)element);
						}
						break;
					case MODE_SPLIT:
						logic.performSplit((Node) element);
						break;
					case MODE_APPEND:
						switch (appendMode) {
						case APPEND_START:
							logic.performAppendStart(element);
							break;
						case APPEND_APPEND:
							// TODO
							break;
						}
						break;
					case MODE_EASYEDIT:
						easyEditManager.editElement(element);
						break;
					}
				}
			}
			return true;
		}
	}

	/**
	 * A KeyListener for all key events.
	 * 
	 * @author mb
	 */
	public class MapKeyListener implements OnKeyListener {

		@Override
		public boolean onKey(final View v, final int keyCode, final KeyEvent event) {
			switch (event.getAction()) {
			case KeyEvent.ACTION_UP:
				if (!v.onKeyUp(keyCode, event)) {
					switch (keyCode) {
					case KeyEvent.KEYCODE_VOLUME_UP:
					case KeyEvent.KEYCODE_VOLUME_DOWN:
						// this stops the piercing beep related to volume adjustments
						return true;
					}
				}
				break;
			case KeyEvent.ACTION_DOWN:
				if (!v.onKeyDown(keyCode, event)) {
					switch (keyCode) {
					case KeyEvent.KEYCODE_DPAD_CENTER:
						setFollowGPS(true);
						return true;
						
					case KeyEvent.KEYCODE_DPAD_UP:
						translate(Logic.CursorPaddirection.DIRECTION_UP);
						return true;
						
					case KeyEvent.KEYCODE_DPAD_DOWN:
						translate(Logic.CursorPaddirection.DIRECTION_DOWN);
						return true;
						
					case KeyEvent.KEYCODE_DPAD_LEFT:
						translate(Logic.CursorPaddirection.DIRECTION_LEFT);
						return true;
						
					case KeyEvent.KEYCODE_DPAD_RIGHT:
						translate(Logic.CursorPaddirection.DIRECTION_RIGHT);
						return true;
						
					case KeyEvent.KEYCODE_VOLUME_UP:
					case KeyEvent.KEYCODE_SEARCH:
						logic.zoom(Logic.ZOOM_IN);
						updateZoomControls();
						return true;
						
					case KeyEvent.KEYCODE_VOLUME_DOWN:
					case KeyEvent.KEYCODE_SHIFT_LEFT:
					case KeyEvent.KEYCODE_SHIFT_RIGHT:
						logic.zoom(Logic.ZOOM_OUT);
						updateZoomControls();
						return true;
					}
				}
				break;
			}
			return false;
		}
		
		private void translate(final CursorPaddirection direction) {
			setFollowGPS(false);
			logic.translate(direction);
		}
	}

	/**
	 * @return a list of all pending changes to upload (contains newlines)
	 */
	public String getPendingChanges() {
		List<String> changes = logic.getPendingChanges(this);
		StringBuilder retval = new StringBuilder();
		for (String change : changes) {
			retval.append(change).append('\n');
		}
		return retval.toString();
	}
	
	/**
	 * Invalidates (redraws) the map
	 */
	public void invalidateMap() {
		map.invalidate();
	}
	
	public void triggerMapContextMenu() {
		map.showContextMenu();
	}
	
	public static boolean hasChanges() {
		if (logic == null) return false;
		return logic.hasChanges();
	}
	
	/**
	 * Sets the activity to re-download the last downloaded area on startup
	 * (use e.g. when the API URL is changed)
	 */
	public static void prepareRedownload() {
		redownloadOnResume = true;
	}

	/**
	 * @return the currentPreset
	 */
	public static Preset getCurrentPreset() {
		return currentPreset;
	}

	/**
	 * Resets the current preset, causing it to be re-parsed
	 */
	public static void resetPreset() {
		currentPreset = null;
	}
	
	public String getBaseURL() {
		return prefs.getServer().getBaseURL();
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		Log.i("Main", "Tracker service connected");
		tracker = (((TrackerBinder)service).getService());
		map.setTracker(tracker);
		tracker.setListener(this);
		tracker.setListenerNeedsGPS(wantLocationUpdates);
		triggerMenuInvalidation();
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		// should never happen, but just to be sure
		Log.i("Main", "Tracker service disconnected");
		tracker = null;
		map.setTracker(null);
		triggerMenuInvalidation();
	}

	@Override
	public void onLocationChanged(Location location) {
		if (followGPS) {
			BoundingBox viewBox = map.getViewBox();
			// ensure the view is zoomed in to at least the most zoomed-out
			while (!viewBox.canZoomOut() && viewBox.canZoomIn()) {
				viewBox.zoomIn();
			}
			viewBox.moveTo((int) (location.getLongitude() * 1E7d), (int) (location.getLatitude() * 1E7d));
		}
		if (showGPS) {
			map.setLocation(location);
		}
		map.invalidate();
	}
	
	
	@Override
	/**
	 * DO NOT CALL DIRECTLY in custom code.
	 * Use {@link #triggerMenuInvalidation()} to make it easier to debug and implement workarounds for android bugs.
	 * Must be called from the main thread.
	 */
	public void invalidateOptionsMenu() { 
		// Log.d(DEBUG_TAG, "invalidateOptionsMenu called");
		super.invalidateOptionsMenu();
	}
	
	/**
	 * Simply calls {@link #invalidateOptionsMenu()}.
	 * Used to make it easier to implement workarounds.
	 * MUST BE CALLED FROM THE MAIN/UI THREAD!
	 */
	public void triggerMenuInvalidation() {
		Log.d(DEBUG_TAG, "triggerMenuInvalidation called");
		super.invalidateOptionsMenu(); // TODO delay or make conditional to work around android bug?
	}
	
	/**
	 * Invalidates the options menu of the main activity if such an activity exists.
	 * MUST BE CALLED FROM THE MAIN/UI THREAD!
	 */
	public static void triggerMenuInvalidationStatic() {
		if (Application.mainActivity == null) return;
		// DO NOT IGONORE "wrong thread" EXCEPTIONS FROM THIS.
		// It *will* mess up your menu in many creative ways.
		Application.mainActivity.triggerMenuInvalidation();
	}

	public class EventClickHandler {
		
		private List<Event> clickedEvents;
		
		public void onEventsClicked(List<Event> clickedEvents) {
			this.clickedEvents = clickedEvents;
		}
		
	}
}
