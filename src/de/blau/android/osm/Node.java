package de.blau.android.osm;

import java.io.IOException;
import java.util.HashMap;
import java.util.SortedMap;

import org.xmlpull.v1.XmlSerializer;

import de.tum.bgu.lfk.openeventmap.Event;

/**
 * Node represents a Node in the OSM-data-structure. It stores the lat/lon-pair and provides some package-internal
 * manipulating-methods.
 * 
 * @author mb
 */
public class Node extends OsmElement implements GeoPoint {
	/**
	 * 
	 */
	private static final long serialVersionUID = 152395243648348266L;

	/**
	 * WGS84 decimal Latitude-Coordinate times 1E7.
	 */
	protected int lat;

	/**
	 * WGS84 decimal Longitude-Coordinate times 1E7.
	 */
	protected int lon;

	/**
	 * It's name in the OSM-XML-scheme.
	 */
	public static final String NAME = "node";
	
	/**
	 * Constructor. Call it solely in {@link OsmElementFactory}!
	 * 
	 * @param osmId the OSM-ID. When not yet transmitted to the API it is negative.
	 * @param osmVersion the version of the element
	 * @param status see {@link OsmElement#state}
	 * @param lat WGS84 decimal Latitude-Coordinate times 1E7.
	 * @param lon WGS84 decimal Longitude-Coordinate times 1E7.
	 */
	Node(final long osmId, final long osmVersion, final byte status, final int lat, final int lon) {
		super(osmId, osmVersion, status);
		this.lat = lat;
		this.lon = lon;
	}

	public int getLat() {
		return lat;
	}

	public int getLon() {
		return lon;
	}

	void setLat(final int lat) {
		this.lat = lat;
	}

	void setLon(final int lon) {
		this.lon = lon;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return NAME;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return super.toString() + "\tlat: " + lat + "; lon: " + lon;
	}

	@Override
	public void toXml(final XmlSerializer s, final Long changeSetId)
			throws IllegalArgumentException, IllegalStateException, IOException {
		s.startTag("", "node");
		s.attribute("", "id", Long.toString(osmId));
		if (changeSetId != null) s.attribute("", "changeset", Long.toString(changeSetId));
		s.attribute("", "version", Long.toString(osmVersion));
		s.attribute("", "lat", Double.toString((double) (lat / 1E7)));
		s.attribute("", "lon", Double.toString((double) (lon / 1E7)));
		tagsToXml(s);
		s.endTag("", "node");
	}

	@Override
	public ElementType getType() {
		return ElementType.NODE;
	}

	/**
	 * Scans the element's tags for event information and creates corresponding Event objects.
	 * @return A map of Events created from the tag information. The EventId is used as key value.
	 * Null if there is no tag with key "event" and value "yes"
	 */
	public HashMap<Long, Event> getAttachedEvents() {
		String isEvent = getTagWithKey("event");
		if (isEvent == null) {
			return null;
		}
		if (isEvent.equalsIgnoreCase("yes")) {
			SortedMap<String, String> tags = getTags();
			HashMap<Long, Event> events = new HashMap<Long, Event>();
			
			Event event = null;
			long eventNumber = -1L;
			for (String key : tags.keySet()) {
				if (key.startsWith("event:")) {
					String[] keyParts = key.split(":");
					if (keyParts.length != 3) {
						throw new RuntimeException("Invalid event tag in node: " + String.valueOf(getOsmId()));
					}
					long number = Long.valueOf(keyParts[1]);
					if (number != eventNumber) {
						// Store created event
						if (event != null) {
							events.put(eventNumber, event);
						}
						// Create event object for next event
						event = new Event();
						eventNumber = number;
						event.setEventType(Node.NAME);
						event.setTypeId(this.osmId);
						event.setNumber(eventNumber);
						event.setLat(lat);
						event.setLon(lon);
					}
					event.setTag(keyParts[2], tags.get(key));
				}
			}
			// Store the last created event if any!
			if (event != null) {
				events.put(eventNumber, event);
			}
			return events;
		}
		return null;
	}
	

	/**
	 * Scans the element's tags for event information and creates the corresponding Event object.
	 * @param eventId Number of the event to be received
	 * @return An Event created from the tag information.
	 * Null if there is no tag with key "event" and value "yes" or if there is no Event with that id.
	 */
	public Event getAttachedEvent(long eventId) {
		String isEvent = getTagWithKey("event");
		if (isEvent == null) {
			return null;
		}
		if (isEvent.equalsIgnoreCase("yes")) {
			SortedMap<String, String> tags = getTags();
			
			Event event = null;
			for (String key : tags.keySet()) {
				if (key.startsWith("event:")) {
					String[] keyParts = key.split(":");
					if (keyParts.length != 3) {
						throw new RuntimeException("Invalid event tag in node: " + String.valueOf(getOsmId()));
					}
					long number = Long.valueOf(keyParts[1]);
					if (number == eventId) {
						if (event == null) {
						// Create a new event object 
							event = new Event();
							event.setEventType(Node.NAME);
							event.setTypeId(this.osmId);
							event.setNumber(eventId);
							event.setLat(lat);
							event.setLon(lon);
						}
						event.setTag(keyParts[2], tags.get(key));							
					}
				}
			}
			return event;
		}
		return null;
	}

	/**
	 * Scans the element's tags for event information and creates a new empty Event with an unused event number.
	 * @return A new empty Event with an unused event number.
	 * Null if there is no tag with key "event" and value "yes".
	 */
	public Event getNewEvent() {
		String isEvent = getTagWithKey("event");
		if (isEvent == null) {
			return null;
		}
		if (isEvent.equalsIgnoreCase("yes")) {
			SortedMap<String, String> tags = getTags();
			
			Event event = new Event();
			event.setEventType(Node.NAME);
			event.setTypeId(this.osmId);
			event.setLat(lat);
			event.setLon(lon);
			
			long eventId = -1L;
			
			for (String key : tags.keySet()) {
				if (key.startsWith("event:")) {
					String[] keyParts = key.split(":");
					if (keyParts.length != 3) {
						throw new RuntimeException("Invalid event tag in node: " + String.valueOf(getOsmId()));
					}
					long number = Long.valueOf(keyParts[1]);
					if (number > eventId) {
						eventId = number;							
					}
				}
			}
			
			eventId++;
			event.setNumber(eventId);
			
			return event;
		}
		return null;
	}
	
}
