package de.blau.android.osm;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;
import de.blau.android.exception.OsmParseException;

/**
 * Parses an XML (as InputStream), provided by the OSM api from /api/0.6/node/NODE_ID, and generates an OSM node element in its storage.
 * 
 * @author mark
 */
public class OsmNodeParser extends DefaultHandler {

	private static final String CLASS_NAME = OsmNodeParser.class.getSimpleName();

	/** The storage, where the data will be stored */
	private final Storage storage;

	/** Node that will be created after parsing the XML */
	private Node node;

	public OsmNodeParser() {
		super();
		storage = new Storage();
	}

	public Storage getStorage() {
		return storage;
	}

	/**
	 * Triggers the beginning of parsing.
	 * 
	 * @param in InputStream of the XML document received from the OSM API
	 */
	public void start(final InputStream in) throws SAXException, IOException, ParserConfigurationException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		saxParser.parse(in, this);
	}

	@Override
	public void startElement(final String uri, final String name, final String qName, final Attributes atts) {
		try {
			if (OsmParser.isNode(name)) {
				parseNode(name, atts);
			} else if (OsmParser.isTag(name)) {
				parseTag(atts);
			}
		} catch (OsmParseException e) {
			Log.e(CLASS_NAME, "OsmParseException", e);
		}
	}

	@Override
	public void endElement(final String uri, final String name, final String qName) {
		if (OsmParser.isNode(name)) {
			storage.insertNodeUnsafe(node);
			node = null;
		} else {
			Log.d(CLASS_NAME, "endElement for name = " + name);
		}
	}

	private void parseNode(final String name, final Attributes atts) throws OsmParseException {
		try {
			long osmId = Long.parseLong(atts.getValue("id"));
			long osmVersion = Long.parseLong(atts.getValue("version"));
			byte status = 0;
			int lat = (int) (Double.valueOf(atts.getValue("lat")) * 1E7);
			int lon = (int) (Double.valueOf(atts.getValue("lon")) * 1E7);
			Log.d(CLASS_NAME, "Creating node " + osmId);
			node = OsmElementFactory.createNode(osmId, osmVersion, status, lat, lon);
		} catch (NumberFormatException e) {
			throw new OsmParseException("Error parsing node element.");
		}
	}
	
	private void parseTag(final Attributes atts) {
		if (node == null) {
			Log.e(CLASS_NAME, "Parsing Error: no node was created!");
		} else {
			String k = atts.getValue("k");
			String v = atts.getValue("v");
			node.addOrUpdateTag(k, v);
		}
	}
}