package de.blau.android.osm;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;
import de.blau.android.exception.OsmParseException;

/**
 * Parses an XML (as InputStream), provided by the OSM api from /api/0.6/way/WAY_ID, and generates an OSM way element in its storage.
 * 
 * @author mark
 */
public class OsmWayParser extends DefaultHandler {

	private static final String CLASS_NAME = OsmWayParser.class.getSimpleName();

	/** The storage, where the data will be stored */
	private final Storage storage;
	/** The storage that contains the nodes referenced by the way */
	private final Storage mainStorage;

	/** Way that will be created after parsing the XML */
	private Way way;

	public OsmWayParser() {
		super();
		storage = new Storage();
		mainStorage = null;
	}
	
	/**
	 * When no main storage is given, ways will only contain node IDs.
	 * When a storage is given the way nodes are referenced therein.
	 * 
	 * @param mainStorage Create node references to this storage.
	 */
	public OsmWayParser(Storage mainStorage) {
		super();
		storage = new Storage();
		this.mainStorage = mainStorage;
	}

	public Storage getStorage() {
		return storage;
	}

	/**
	 * Triggers the beginning of parsing.
	 * 
	 * @param in InputStream of the XML document received from the OSM API
	 */
	public void start(final InputStream in) throws SAXException, IOException, ParserConfigurationException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		saxParser.parse(in, this);
	}

	@Override
	public void startElement(final String uri, final String name, final String qName, final Attributes atts) {
		try {
			if (OsmParser.isWay(name)) {
				parseWay(atts);
			} else if (OsmParser.isWayNode(name)) {
				parseWayNode(atts);
			} else if (OsmParser.isTag(name)) {
				parseTag(atts);
			}
		} catch (OsmParseException e) {
			Log.e(CLASS_NAME, "OsmParseException", e);
		}
	}

	@Override
	public void endElement(final String uri, final String name, final String qName) {
		if (OsmParser.isWay(name)) {
			storage.insertWayUnsafe(way);
			way = null;
		} else {
			Log.d(CLASS_NAME, "endElement for name = " + name);
		}
	}

	private void parseWay(final Attributes atts) throws OsmParseException {
		try {
			long osmId = Long.parseLong(atts.getValue("id"));
			long osmVersion = Long.parseLong(atts.getValue("version"));
			byte status = 0;
			Log.d(CLASS_NAME, "Creating way " + osmId);
			way = OsmElementFactory.createWay(osmId, osmVersion, status);
		} catch (NumberFormatException e) {
			throw new OsmParseException("Error parsing way element.");
		}
	}
	
	private void parseWayNode(final Attributes atts) {
		long ref = Long.parseLong(atts.getValue("ref"));
		if (mainStorage == null) {
			way.nodeIds.add(ref);
		} else {
			Node n = mainStorage.getNode(ref);
			if (n != null) {
				way.addNode(n);
			} else {
				Log.e(CLASS_NAME, "Way node not available in main storage!");
			}
		}
	}
	
	private void parseTag(final Attributes atts) {
		if (way == null) {
			Log.e(CLASS_NAME, "Parsing Error: no way was created!");
		} else {
			// MARK Tags are not needed when no main storage is given
			if (mainStorage != null) {
				String k = atts.getValue("k");
				String v = atts.getValue("v");
				way.addOrUpdateTag(k, v);
			}
		}
	}
}