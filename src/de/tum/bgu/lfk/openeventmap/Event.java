package de.tum.bgu.lfk.openeventmap;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import de.blau.android.osm.Node;
import de.blau.android.osm.Relation;
import de.blau.android.osm.Way;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.v4.util.ArrayMap;
import android.util.Log;

/**
 * Event is capable of storing essential information about events and offers a
 * set of functions to export/import information to/from the OSM tag /
 * value format and to convert to/from dates from/to strings.
 * 
 * @author mark
 *
 */
public class Event implements Serializable {

	private static final long serialVersionUID = -8607471430720160759L;
	
	private static final String LOG_TAG = "MARK";
	
	public static final String[] sqlColumns = {
		"id",
		"event_type",
		"type_id",
		"number",
		"name",
		"category",
		"subcategory",
		"startdate",
		"enddate",
		"related_items",
		"url",
		"num_participants",
		"howoften",
		"latitude",
		"longitude"		
	};
	
	public static String sqlColumnsString;
	
	private static Map<String, Integer> categoryColors;
	private static Map<String, Rect> categoryRects;
	
	static {
		categoryColors = new ArrayMap<String, Integer>(12);
		categoryColors.put("accident",     Color.parseColor("#d63e2a"));
		categoryColors.put("concert",      Color.parseColor("#a23336"));
		categoryColors.put("conference",   Color.parseColor("#8a2e2e"));
		categoryColors.put("construction", Color.parseColor("#0067a3"));
		categoryColors.put("educational",  Color.parseColor("#5bf65c"));
		categoryColors.put("exhibition",   Color.parseColor("#f69730"));
		categoryColors.put("natural",      Color.parseColor("#364075"));
		categoryColors.put("political",    Color.parseColor("#5b396b"));
		categoryColors.put("social",       Color.parseColor("#38aadd"));
		categoryColors.put("sport",        Color.parseColor("#769600"));
		categoryColors.put("traffic",      Color.parseColor("#d252b9"));
		categoryColors.put("other",        Color.parseColor("#436978"));
		
		int markerWidth = 18;
		categoryRects = new ArrayMap<String, Rect>(12);
		categoryRects.put("accident",     new Rect( 0*markerWidth, 0,  1*markerWidth-1, 22));
		categoryRects.put("concert",      new Rect( 5*markerWidth, 0,  6*markerWidth-1, 22));
		categoryRects.put("conference",   new Rect(10*markerWidth, 0, 11*markerWidth-1, 22));
		categoryRects.put("construction", new Rect( 6*markerWidth, 0,  7*markerWidth-1, 22));
		categoryRects.put("educational",  new Rect( 2*markerWidth, 0,  3*markerWidth-1, 22));
		categoryRects.put("exhibition",   new Rect( 1*markerWidth, 0,  2*markerWidth-1, 22));
		categoryRects.put("natural",      new Rect(11*markerWidth, 0, 12*markerWidth-1, 22));
		categoryRects.put("political",    new Rect( 8*markerWidth, 0,  9*markerWidth-1, 22));
		categoryRects.put("social",       new Rect( 3*markerWidth, 0,  4*markerWidth-1, 22));
		categoryRects.put("sport",        new Rect( 7*markerWidth, 0,  8*markerWidth-1, 22));
		categoryRects.put("traffic",      new Rect( 4*markerWidth, 0,  5*markerWidth-1, 22));
		categoryRects.put("other",        new Rect( 9*markerWidth, 0, 10*markerWidth-1, 22));
		
		sqlColumnsString = "";
		for (String c : sqlColumns) {
			if (sqlColumnsString.length() > 0)
				sqlColumnsString += ",";
			sqlColumnsString += c;
		}
	}
	
	public static int getColorForCategegory(String category) {
		Integer color = categoryColors.get(category);
		if (color == null) {
			return categoryColors.get("other");
		}
		return color;
	}

	public static Rect getRectForCategory(String category) {
		Rect rect = categoryRects.get(category);
		if (rect == null) {
			return categoryRects.get("other");
		}
		return rect;
	}

	/** Id of the element in the SQL table search_event. 
	 *  Read from the SQL table search_event from column id. */
	public int id;
	/** Type of the node this event is associated with. "node", "way" or "rel".
	 *  Read from the SQL table search_event from column event_type. */
	private String eventType;
	/** OSM id of the underlying OSM element.
	 *  Read from the SQL table search_event from column type_id. */
	private long typeId;
	/** Number of the event in the underlying OSM element.
	 *  Read from the SQL table search_event from column number. */
	private long number;
	/** Name of the event.
	 *  Read from the SQL table search_event from column name.
	 *  Read from the OSM tag with key event:number:name. */
	private String name;
	/** Category of the event.
	 *  Read from the SQL table search_event from column category.
	 *  Read from the OSM tag with key event:number:category. */
	private String category;
	/** Sub-Category of the event.
	 *  Read from the SQL table search_event from column subcategory.
	 *  Read from the OSM tag with key event:number:subcategory. */
	private String subcategory;
	/** Start date of the event.
	 *  Read from the SQL table search_event from column startdate.
	 *  Read from the OSM tag with key event:number:startdate. */
	private Date startdate;
	/** End date of the event.
	 *  Read from the SQL table search_event from column enddate.
	 *  Read from the OSM tag with key event:number:enddate. */
	private Date enddate;
	/** Related OSM elements of the event. Given as string containing the elements types and ids.
	 *  Read from the SQL table search_event from column related_items.
	 *  Read from the OSM tag with key event:number:related_items. */
	private String relatedItems;
	/** Url of the event.
	 *  Read from the SQL table search_event from column url.
	 *  Read from the OSM tag with key event:number:url. */
	private String url;
	/** Number of participants of the event.
	 *  Read from the SQL table search_event from column num_participants.
	 *  Read from the OSM tag with key event:number:num_participants. */
	private String numParticipants;
	/** How often this event occurs.
	 *  Read from the SQL table search_event from column howoften.
	 *  Read from the OSM tag with key event:number:howoften. */
	private String howoften;
	/** Comment on the event.
	 *  Currently not available in the SQL table search_event!
	 *  Read from the OSM tag with key event:number:comment. */
	private String comment;
	/** Organization of the event.
	 *  Currently not available in the SQL table search_event!
	 *  Read from the OSM tag with key event:number:organization. */
	private String organization;
	/** Latitude of the event. Given in 10^7 times the latitude.
	 *  Read from the SQL table search_event from column latitude.
	 *  Read from the OSM node tag with key lat. */
	public int latitude;
	/** Longitude of the event. Given in 10^7 times the longitude.
	 *  Read from the SQL table search_event from column longitude.
	 *  Read from the OSM node tag with key lon. */
	public int longitude;
	

	public Event() {
		this.id = -1;
		this.eventType = null;
		this.typeId = -1L;
		this.number = -1L;
		this.name = null;
		this.category = null;
		this.subcategory = null;
		this.startdate = null;
		this.enddate = null;
		this.relatedItems = null;
		this.url = null;
		this.numParticipants = null;
		this.howoften = null;
		this.latitude = Integer.MIN_VALUE;
		this.longitude = Integer.MIN_VALUE;
	}

	public Event(Event other) {
		this.id = other.id;
		if (other.eventType == null) this.eventType = null;
		else this.eventType = new String(other.eventType);
		this.typeId = other.typeId;
		this.number = other.number;
		if (other.name == null) this.name = null;
		else this.name = new String(other.name);
		if (other.category == null) this.category = null;
		else this.category = new String(other.category);
		if (other.subcategory == null) this.subcategory = null;
		else this.subcategory = new String(other.subcategory);
		if (other.startdate == null) this.startdate = null;
		else this.startdate = new Date(other.startdate.getTime());
		if (other.enddate == null) this.enddate = null;
		else this.enddate = new Date(other.enddate.getTime());
		if (other.relatedItems == null) this.relatedItems = null;
		else this.relatedItems = new String(other.relatedItems);
		if (other.url == null) this.url = null;
		else this.url = new String(other.url);
		if (other.numParticipants == null) this.numParticipants = null;
		else this.numParticipants = new String(other.numParticipants);
		if (other.howoften == null) this.howoften = null;
		else this.howoften = new String(other.howoften);
		if (other.comment == null) this.comment = null;
		else this.comment = new String(other.comment);
		if (other.organization == null) this.organization = null;
		else this.organization = new String(other.organization);
		this.latitude = other.latitude;
		this.longitude = other.longitude;
	}

	public String[] getTagKeys() {
		String[] result = new String[11];
		String baseTag = "event:" + String.valueOf(number) + ":";
		result[0] = baseTag + "name";
		result[1] = baseTag + "category";
		result[2] = baseTag + "subcategory";
		result[3] = baseTag + "startdate";
		result[4] = baseTag + "enddate";
		result[5] = baseTag + "related_items";
		result[6] = baseTag + "url";
		result[7] = baseTag + "num_participants";
		result[8] = baseTag + "howoften";
		result[9] = baseTag + "comment";
		result[10] = baseTag + "organization";
		return result;
	};
	
	public Map<String, String> getTags() {
		// Every event should have a "number" >= 0
		if (number >= 0) {
			Map<String, String> tags = new ArrayMap<String, String>(9);
			tags.put("event", "yes");
			String eventBaseTag = "event:" + number + ":";

			if (name != null && name.length() > 0)
				tags.put(eventBaseTag + "name", name);			
			if (category != null && category.length() > 0)
				tags.put(eventBaseTag + "category", category);			
			if (subcategory != null && subcategory.length() > 0)
				tags.put(eventBaseTag + "subcategory", subcategory);
			if (startdate != null)
				tags.put(eventBaseTag + "startdate", Event.dateToString(startdate));
			if (enddate != null)
				tags.put(eventBaseTag + "enddate", Event.dateToString(enddate));
			if (relatedItems != null && relatedItems.length() > 0)
				tags.put(eventBaseTag + "related_items", relatedItems);
			if (url != null && url.length() > 0)
				tags.put(eventBaseTag + "url", url);	
			if (numParticipants != null && numParticipants.length() > 0)
				tags.put(eventBaseTag + "num_participants", numParticipants);	
			if (howoften != null && howoften.length() > 0)
				tags.put(eventBaseTag + "howoften", howoften);
			if (comment != null && comment.length() > 0)
				tags.put(eventBaseTag + "comment", comment);	
			if (organization != null && organization.length() > 0)
				tags.put(eventBaseTag + "organization", organization);
			return tags;
		}

		return null;
	}

	public static String dateToString(Date d) {
		if (d == null) {
			Log.d(LOG_TAG, "Invalid date: null");
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		return c.get(Calendar.DAY_OF_MONTH) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.YEAR);
	}
	
	public static Date stringToDate(String date) {
		String[] dateParts;
		try {
			// String is DD/MM/YYYY
			dateParts = date.split("/");
			if (dateParts.length == 3) {
				Calendar c = Calendar.getInstance();
				c.set(Integer.valueOf(dateParts[2]), Integer.valueOf(dateParts[1]) - 1, Integer.valueOf(dateParts[0]));
				return c.getTime();			
			}
			// String is DD.MM.YYYY-HH:MM
			dateParts = date.split("\\.");
			if (dateParts.length == 3) {
				String[] lastParts = dateParts[2].split("-");
				if (lastParts.length == 2) {
					Calendar c = Calendar.getInstance();
					c.set(Integer.valueOf(lastParts[0]), Integer.valueOf(dateParts[1]) - 1, Integer.valueOf(dateParts[0]));
					return c.getTime();
				}
			}
			// String is DD.MM.YYYY
			dateParts = date.split("\\.");
			if (dateParts.length == 3) {
				Calendar c = Calendar.getInstance();
				c.set(Integer.valueOf(dateParts[2]), Integer.valueOf(dateParts[1]) - 1, Integer.valueOf(dateParts[0]));
				return c.getTime();			
			}
			Log.w(LOG_TAG, "Invalid date string: " + date);			
		} catch (Exception e) {
			Log.w(LOG_TAG, "Invalid date string: " + date);
		}
		return null;
	}
	
	public String getStartdateAsString() {
		return dateToString(startdate);
	}
	
	public String getEnddateAsString() {
		return dateToString(enddate);
	}
	
	public static String dateToSQLString(Date d) {
		if (d == null) {
			Log.d(LOG_TAG, "Invalid date: null");
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		return c.get(Calendar.YEAR) + "-" + (c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.DAY_OF_MONTH);		
	}
	
	public static Date SQLStringToDate(String date) {
		if (date == null) {
			Log.d(LOG_TAG, "Invalid SQL date string: null");
			return null;
		}
		String[] dateParts = date.split("-");
		if (dateParts.length != 3) {
			Log.w(LOG_TAG, "Invalid SQL date string: " + date);
			return null;
		}
		try {
			Calendar c = Calendar.getInstance();
			c.set(Integer.valueOf(dateParts[0]), Integer.valueOf(dateParts[1]) - 1, Integer.valueOf(dateParts[2]));
			return c.getTime();
		} catch (Exception e) {
			Log.w(LOG_TAG, "Invalid SQL date string: " + date);
		}
		return null;		
	}
	
	public String getStartdateAsSQLString() {
		return dateToSQLString(startdate);
	}
	
	public String getEnddateAsSQLString() {
		return dateToSQLString(enddate);
	}

	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getEventType() {
		return eventType;
	}
	
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public long getTypeId() {
		return typeId;
	}
	
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}
	
	public long getNumber() {
		return number;
	}
	
	public void setNumber(long l) {
		this.number = l;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;		
	}

	public String getCategory() {
		return this.category;
	}
	
	public void setCategory(String category) {
		this.category = category;		
	}

	public String getSubcategory() {
		return this.subcategory;
	}
	
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;		
	}
	
	public Date getStartdate() {
		return startdate;
	}
	
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	
	public Date getEnddate() {
		return enddate;
	}
	
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	
	public String getRelatedItems() {
		return relatedItems;
	}

	public void setRelatedItems(String relatedItems) {
		this.relatedItems = relatedItems;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNumParticipants() {
		return numParticipants;
	}

	public void setNumParticipants(String numParticipants) {
		this.numParticipants = numParticipants;
	}

	public String getHowoften() {
		return howoften;
	}

	public void setHowoften(String howoften) {
		this.howoften = howoften;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public int getLat() {
		return this.latitude;
	}
	
	public void setLat(int latitude) {
		this.latitude = latitude;
	}

	public int getLon() {
		return this.longitude;
	}
	
	public void setLon(int longitude) {
		this.longitude = longitude;
	}

	public void setTag(String tag, String value) {
		if (tag.equals("name"))
			setName(value);
		else if (tag.equals("category"))
			setCategory(value);
		else if (tag.equals("subcategory"))
			setSubcategory(value);
		else if (tag.equals("startdate"))
			setStartdate(stringToDate(value));
		else if (tag.equals("enddate"))
			setEnddate(stringToDate(value));
		else if (tag.equals("related_items"))
			setRelatedItems(value);
		else if (tag.equals("url"))
			setUrl(value);
		else if (tag.equals("num_participants"))
			setNumParticipants(value);
		else if (tag.equals("howoften"))
			setHowoften(value);
		else if (tag.equals("comment"))
			setComment(value);
		else if (tag.equals("organization"))
			setOrganization(value);
		else Log.w(LOG_TAG, "Unknown event tag used: " + tag + ", value : " + value);
	}

	/**Compares two {@link Event}s by their start date (null-safe).
	 * Events with later start date or start date not specified are considered
	 * "greater". */
	public static class ComparatorStartdate implements Comparator<Event> {

		@Override
		public int compare(Event lhs, Event rhs) {
			Date lDate = lhs.getStartdate();
			Date rDate = rhs.getStartdate();
			
			if (lDate == null && rDate == null) {
				return 0; // both null are considered equivalent
			}
			if (lDate == null) {
				return 1; // consider null bigger than anything else
			}
			if (rDate == null) {
				return -1; // consider null bigger than anything else
			}
			return lDate.compareTo(rDate); // use natural order of Dates
		}
	}
	
	/**
	 * Adds the IDs of the related OSM elements of a given event to the given ID sets.
	 * 
	 * @param event The event having related items.
	 * @param relatedNodeIds The set of related nodes.
	 * @param relatedWayIds The set of related ways.
	 * @param relatedRelationIds The set of related relations.
	 */
	public static void extractRelatedItems(Event event, Set<Long> relatedNodeIds, Set<Long> relatedWayIds, Set<Long> relatedRelationIds) {
		String[] relatedItemsParts = event.splitRelatedItems();
		if (relatedItemsParts != null) {
			for (String relatedItemsPart : relatedItemsParts) {
				String[] typeAndId = getElementTypeAndIdOfRelatedItemString(relatedItemsPart);
				try {
					if (typeAndId[0].equalsIgnoreCase(Node.NAME)) {
						long nodeId = Long.parseLong(typeAndId[1]);
						relatedNodeIds.add(nodeId);
					} else if (typeAndId[0].equalsIgnoreCase(Way.NAME)) {
						long wayId = Long.parseLong(typeAndId[1]);
						relatedWayIds.add(wayId);
					} else if (typeAndId[0].equalsIgnoreCase(Relation.NAME)) {
						Log.w(LOG_TAG, "Relations are currently not supported as related items.");
					}
				} catch (Exception e) {
					Log.e(LOG_TAG, "Problem parsing related item string: " + relatedItemsPart);
				}
			}
		} else {
			Log.d(LOG_TAG, "Event has no related items!");
		}
	}
	
	public int numberOfRelatedNodes() {
		int result = 0;
		String[] relatedItemStrings = splitRelatedItems();
		if (relatedItemStrings != null) {
			for (int i = 0; i < relatedItemStrings.length; i++) {
				String elementType = Event.getElementTypeOfRelatedItemString(relatedItemStrings[i]);
				if (elementType != null && elementType.equalsIgnoreCase(Node.NAME)) {
					result++;
				}
			}
		}
		return result;
	}
	
	public int numberOfRelatedWays() {
		int result = 0;
		String[] relatedItemStrings = splitRelatedItems();
		if (relatedItemStrings != null) {
			for (int i = 0; i < relatedItemStrings.length; i++) {
				String elementType = Event.getElementTypeOfRelatedItemString(relatedItemStrings[i]);
				if (elementType != null && elementType.equalsIgnoreCase(Way.NAME)) {
					result++;
				}
			}
		}
		return result;
	}
	
	public int numberOfRelatedRelations() {
		int result = 0;
		String[] relatedItemStrings = splitRelatedItems();
		if (relatedItemStrings != null) {
			for (int i = 0; i < relatedItemStrings.length; i++) {
				String elementType = Event.getElementTypeOfRelatedItemString(relatedItemStrings[i]);
				if (elementType != null && elementType.equalsIgnoreCase(Relation.NAME)) {
					result++;
				}
			}
		}
		return result;
	}
	
	public static String getElementTypeOfRelatedItemString(String relatedItemString) {
		if (relatedItemString != null) {
			String[] typeAndId = relatedItemString.split(":");
			if (typeAndId != null && typeAndId.length == 2) {
				return typeAndId[0];
			}
		}
		return null;
	}
	
	public static String getElementIdOfRelatedItemString(String relatedItemString) {
		if (relatedItemString != null) {
			String[] typeAndId = relatedItemString.split(":");
			if (typeAndId != null && typeAndId.length == 2) {
				return typeAndId[1];
			}
		}
		return null;
	}

	public static String[] getElementTypeAndIdOfRelatedItemString(String relatedItemString) {
		if (relatedItemString != null) {
			String[] typeAndId = relatedItemString.split(":");
			return typeAndId;
		}
		return null;
	}

	public String[] splitRelatedItems() {
		if (relatedItems != null) {
			String ri = relatedItems.replaceAll("\\s", "");
			if (ri.length() > 0) {
				return ri.split(",");
			}
		}
		return null;
	}

	public void setRelatedItems(Set<Long> relatedNodes, Set<Long> relatedWays, Set<Long> relatedRelations) {
		String items = "";
		for (Long l : relatedNodes) {
			items += "Node" + ":" + String.valueOf(l) + ", ";
		}
		for (Long l : relatedWays) {
			items += "Way" + ":" + String.valueOf(l) + ", ";
		}
		for (Long l : relatedRelations) {
			items += "Relation" + ":" + String.valueOf(l) + ", ";
		}
		// Remove trailing comma and space, if any
		if (items.length() > 0) {
			items = items.substring(0, items.length()-2);
		}
		Log.d(LOG_TAG, "Set related items to: " + items);
		relatedItems = items;
	}
}
