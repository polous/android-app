package de.tum.bgu.lfk.openeventmap;

import java.text.DateFormat;
import java.util.Date;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.util.SimpleArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import de.blau.android.R;

public class EventEditor extends SherlockFragmentActivity implements DatePickerFragment.DateSetListener {

	public static final String EVENT_EXTRA = "de.tum.bgu.lfk.openeventmap.EventEditor.extra_event";
	
	public static final String ACTION_EXTRA = "de.tum.bgu.lfk.openeventmap.EventEditor.extra_action";
	public static final int ACTION_SAVE = 0;
	public static final int ACTION_BACK = 1;
	public static final int ACTION_EDIT_RELATED_ITEMS = 2;
	
	private Event event;
	
	private ArrayAdapter<CharSequence> categoryAdapter;
	private ArrayAdapter<CharSequence> subcategoryAdapter;
	private ArrayAdapter<CharSequence> numParticipantsAdapter;
	private ArrayAdapter<CharSequence> howoftenAdapter;	
	
	private Intent resultIntent;
	
	private Date startdate;
	private Date enddate;
	
	private enum EventVerification {
		VERIFICATION_OK,
		VERIFICATION_CHOOSE_NAME,
		VERIFICATION_CHOOSE_CATEGORY,
		VERIFICATION_CHOOSE_SUBCATEGORY,
		VERIFICATION_CHOOSE_NUMPARTICIPANTS,
		VERIFICATION_CHOOSE_HOWOFTEN
	}
	
	private static final SimpleArrayMap<String, Integer> subcategoriesMap = new SimpleArrayMap<String, Integer>(11);
	
	static {
		// Populate subcategoriesMap. Used to map selected category name onto possible subcategory values.
		subcategoriesMap.put("-- Please Choose --", R.array.event_tag_subcategory_please_choose_values);
		subcategoriesMap.put("accident", R.array.event_tag_subcategory_accident_values);
		subcategoriesMap.put("concert", R.array.event_tag_subcategory_concert_values);
		subcategoriesMap.put("conference", R.array.event_tag_subcategory_conference_values);
		subcategoriesMap.put("construction", R.array.event_tag_subcategory_construction_values);
		subcategoriesMap.put("educational", R.array.event_tag_subcategory_educational_values);
		subcategoriesMap.put("exhibition", R.array.event_tag_subcategory_exhibition_values);
		subcategoriesMap.put("natural", R.array.event_tag_subcategory_natural_values);
		subcategoriesMap.put("political", R.array.event_tag_subcategory_political_values);
		subcategoriesMap.put("social", R.array.event_tag_subcategory_social_values);
		subcategoriesMap.put("sport", R.array.event_tag_subcategory_sport_values);
		subcategoriesMap.put("traffic", R.array.event_tag_subcategory_traffic_values);
	}
			
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.event_edit);
		
		setTitle(getString(R.string.event_edit_title));
        
		loadEvent();
		
		ActionBar actionbar = getSupportActionBar();
		actionbar.setDisplayShowTitleEnabled(true);
	}

	
	@Override
	protected void onStart() {		
		super.onStart();
		
		// Add Listener
		Spinner categorySpinner = (Spinner) findViewById(R.id.event_edit_category_spinner);
		categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				// Make sure that the right subcategories are shown in the subcategorySpinner
				Spinner categorySpinner = (Spinner) parent;
				String category = categorySpinner.getSelectedItem().toString();
				EventEditor.this.updateSubcategorySpinner(category);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// Do nothing
			}
			
		});
		
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		// Remove Listener
		Spinner categorySpinner = (Spinner) findViewById(R.id.event_edit_category_spinner);
		categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				// Do nothing
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// Do nothing
			}
			
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_save_back, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			return true;
		case R.id.menu_save_back_save:
			EventVerification verificationResult = verifyEvent();
			switch (verificationResult) {
			case VERIFICATION_OK: break;
			case VERIFICATION_CHOOSE_NAME:
				Toast.makeText(this, "Please enter a name!", Toast.LENGTH_SHORT).show();
				return true;
			case VERIFICATION_CHOOSE_CATEGORY:
				Toast.makeText(this, "Please select a category!", Toast.LENGTH_SHORT).show();
				return true;
			case VERIFICATION_CHOOSE_SUBCATEGORY:
				Toast.makeText(this, "Please select a subcategory!", Toast.LENGTH_SHORT).show();
				return true;
			case VERIFICATION_CHOOSE_NUMPARTICIPANTS:
				Toast.makeText(this, "Please select the number of participants!", Toast.LENGTH_SHORT).show();
				return true;
			case VERIFICATION_CHOOSE_HOWOFTEN:
				Toast.makeText(this, "Please select how often the event takes place!", Toast.LENGTH_SHORT).show();
				return true;
			}
			
			saveEvent();

			resultIntent = new Intent();		
			resultIntent.putExtra(EventEditor.EVENT_EXTRA, new Event(event));
			resultIntent.putExtra(EventEditor.ACTION_EXTRA, EventEditor.ACTION_SAVE);
			setResult(RESULT_OK, resultIntent);
			
			finish();
			return true;
		case R.id.menu_save_back_back:
			
			resultIntent = new Intent();		
			resultIntent.putExtra(EventEditor.EVENT_EXTRA, new Event(event));
			resultIntent.putExtra(EventEditor.ACTION_EXTRA, EventEditor.ACTION_BACK);
			setResult(RESULT_OK, resultIntent);
			
			finish();
			return true;
		}
		
		return false;
	}

	private void loadEvent() {

		event = (Event) getIntent().getSerializableExtra(EventEditor.EVENT_EXTRA);
		
		EditText nameEdit = (EditText) findViewById(R.id.event_edit_name_value);
		
		Spinner categorySpinner = (Spinner) findViewById(R.id.event_edit_category_spinner);
		// Create an ArrayAdapter using the string array and a default spinner layout
		categoryAdapter = ArrayAdapter.createFromResource(this,
		        R.array.event_tag_category_values, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		categorySpinner.setAdapter(categoryAdapter);
		
		Spinner numParticipantsSpinner = (Spinner) findViewById(R.id.event_edit_num_participants_spinner);
		// Create an ArrayAdapter using the string array and a default spinner layout
		numParticipantsAdapter = ArrayAdapter.createFromResource(this,
		        R.array.event_tag_num_participants_values, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		numParticipantsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		numParticipantsSpinner.setAdapter(numParticipantsAdapter);	

		Spinner howoftenSpinner = (Spinner) findViewById(R.id.event_edit_howoften_spinner);
		// Create an ArrayAdapter using the string array and a default spinner layout
		howoftenAdapter = ArrayAdapter.createFromResource(this,
		        R.array.event_tag_howoften_values, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		howoftenAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		howoftenSpinner.setAdapter(howoftenAdapter);
		
		nameEdit.setText(event.getName());
		categorySpinner.setSelection(categoryAdapter.getPosition(event.getCategory()));
		Object selectedCategory = categorySpinner.getSelectedItem();
		if (selectedCategory != null) {
			String category = categorySpinner.getSelectedItem().toString();
			updateSubcategorySpinner(category);
		}
		
		startdate = event.getStartdate();
		updateStartdate();
		enddate = event.getEnddate();
		updateEnddate();
		EditText urlEdit = (EditText) findViewById(R.id.event_edit_url_value);
		String url = event.getUrl() == null ? "" : event.getUrl();
		urlEdit.setText(url);
		numParticipantsSpinner.setSelection(numParticipantsAdapter.getPosition(event.getNumParticipants()));
		howoftenSpinner.setSelection(howoftenAdapter.getPosition(event.getHowoften()));
		EditText commentEdit = (EditText) findViewById(R.id.event_edit_comment_value);
		commentEdit.setText(event.getComment() == null ? "" : event.getComment());
		EditText organizationEdit = (EditText) findViewById(R.id.event_edit_organization_value);
		organizationEdit.setText(event.getOrganization() == null ? "" : event.getOrganization());
		
		TextView relatedNodesView = (TextView) findViewById(R.id.event_edit_related_nodes_value);
		TextView relatedWaysView = (TextView) findViewById(R.id.event_edit_related_ways_value);
		TextView relatedRelationsView = (TextView) findViewById(R.id.event_edit_related_relations_value);
		relatedNodesView.setText(String.format(getString(R.string.nodes_d), event.numberOfRelatedNodes()));
		relatedWaysView.setText(String.format(getString(R.string.ways_d), event.numberOfRelatedWays()));
		relatedRelationsView.setText(String.format(getString(R.string.relations_d), event.numberOfRelatedRelations()));
	}
	
	protected void updateSubcategorySpinner(String category) {
		Spinner subcategorySpinner = (Spinner) findViewById(R.id.event_edit_subcategory_spinner);
		
		int subcategoriesArrayResId = subcategoriesMap.get(category);
		
		// Create an ArrayAdapter using the string array and a default spinner layout
		subcategoryAdapter = ArrayAdapter.createFromResource(this,
				subcategoriesArrayResId, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		subcategorySpinner.setAdapter(subcategoryAdapter);
		
		subcategorySpinner.setSelection(0);
		String subcategory = event.getSubcategory();
		if (subcategory != null) {
			for (int i = 0; i < subcategoryAdapter.getCount(); i++) {
				if (subcategory.equalsIgnoreCase(subcategoryAdapter.getItem(i).toString())) {
					subcategorySpinner.setSelection(i);
				}
			}
		}
	}

	private void saveEvent() {
		
		EditText nameEdit = (EditText) findViewById(R.id.event_edit_name_value);
		Spinner categorySpinner = (Spinner) findViewById(R.id.event_edit_category_spinner);
		Spinner subcategorySpinner = (Spinner) findViewById(R.id.event_edit_subcategory_spinner);
		EditText urlEdit = (EditText) findViewById(R.id.event_edit_url_value);
		Spinner numParticipantsSpinner = (Spinner) findViewById(R.id.event_edit_num_participants_spinner);
		Spinner howoftenSpinner = (Spinner) findViewById(R.id.event_edit_howoften_spinner);
		EditText commentEdit = (EditText) findViewById(R.id.event_edit_comment_value);
		EditText organizationEdit = (EditText) findViewById(R.id.event_edit_organization_value);
		
		event.setName(nameEdit.getText().toString());	
		event.setCategory(categorySpinner.getSelectedItem().toString());
		event.setSubcategory(subcategorySpinner.getSelectedItem().toString());
		event.setStartdate(startdate);
		event.setEnddate(enddate);
		event.setUrl(urlEdit.getText().toString());
		event.setNumParticipants(numParticipantsSpinner.getSelectedItem().toString());
		event.setHowoften(howoftenSpinner.getSelectedItem().toString());		
		event.setComment(commentEdit.getText().toString());
		event.setOrganization(organizationEdit.getText().toString());
		
		Log.d("MARK", "Event name set to \"" + event.getName() + "\"");
		Log.d("MARK", "Event category set to \"" + event.getCategory() + "\"");
		Log.d("MARK", "Event subcategory set to \"" + event.getSubcategory() + "\"");
		if (startdate == null) Log.d("MARK", "Event startdate not set!");		
		else Log.d("MARK", "Event startdate set to \"" + DateFormat.getDateInstance().format(event.getStartdate()) + "\"");
		if (enddate == null) Log.d("MARK", "Event enddate not set!");	
		else Log.d("MARK", "Event enddate set to \"" + DateFormat.getDateInstance().format(event.getEnddate()) + "\"");
		Log.d("MARK", "Event url set to \"" + event.getUrl() + "\"");
		Log.d("MARK", "Event num participants set to \"" + event.getNumParticipants() + "\"");
		Log.d("MARK", "Event how often set to \"" + event.getHowoften() + "\"");
		Log.d("MARK", "Event comment set to \"" + event.getComment() + "\"");
		Log.d("MARK", "Event organization set to \"" + event.getOrganization() + "\"");
	}
	
	public void startdateSetButtonClicked(View view) {
		DialogFragment newFragment = new DatePickerFragment();
		Bundle b = new Bundle();
		b.putString("variable", "startdate");
		if (startdate != null) {
			b.putLong("time", startdate.getTime());
		}
		newFragment.setArguments(b);
		newFragment.show(getSupportFragmentManager(), "DatePickerFragment");
	}

	public void startdateRemoveButtonClicked(View view) {
		startdate = null;
		updateStartdate();
	}
	
	private void updateStartdate() {
		TextView v = (TextView) findViewById(R.id.event_edit_startdate_text_value);
		if (startdate == null) {
			v.setText(getString(R.string.event_view_value_unspecified));
		} else {
			v.setText(DateFormat.getDateInstance().format(startdate));
		}
	}
	
	public void enddateSetButtonClicked(View view) {
		DialogFragment newFragment = new DatePickerFragment();
		Bundle b = new Bundle();
		b.putString("variable", "enddate");
		if (enddate != null) {
			b.putLong("time", enddate.getTime());
		}
		newFragment.setArguments(b);
		newFragment.show(getSupportFragmentManager(), "DatePickerFragment");
	}

	public void enddateRemoveButtonClicked(View view) {
		enddate = null;
		updateEnddate();
	}

	private void updateEnddate() {
		TextView v = (TextView) findViewById(R.id.event_edit_enddate_text_value);
		if (enddate == null) {
			v.setText(getString(R.string.event_view_value_unspecified));
		} else {
			v.setText(DateFormat.getDateInstance().format(enddate));
		}
	}

	@Override
	public void onDateSet(String variable, Date date) {
		if (variable.equals("startdate")) {
			startdate = date;
			updateStartdate();
		}
		if (variable.equals("enddate")) {
			enddate = date;
			updateEnddate();
		}
	}

	public void relatedItemsButtonClicked(View view) {
//		if (event != null && event.getTypeId() < 0) {
//			Toast.makeText(this, "New events need to be uploaded to the server, before related objects can be modified!", Toast.LENGTH_LONG).show();
//		} else {		
			saveEvent();
			
			resultIntent = new Intent();		
			resultIntent.putExtra(EventEditor.EVENT_EXTRA, event);
			resultIntent.putExtra(EventEditor.ACTION_EXTRA, EventEditor.ACTION_EDIT_RELATED_ITEMS);
			setResult(RESULT_OK, resultIntent);
			
			finish();
//		}
	}

	private EventVerification verifyEvent() {
		
		TextView nameView = (TextView) findViewById(R.id.event_edit_name_value);
		if (nameView.getText().toString().length() == 0) {
			return EventVerification.VERIFICATION_CHOOSE_NAME;
		}
		
		Spinner categorySpinner = (Spinner) findViewById(R.id.event_edit_category_spinner);
		if (categorySpinner.getItemAtPosition(0).toString().equals(categorySpinner.getSelectedItem().toString())) {
			return EventVerification.VERIFICATION_CHOOSE_CATEGORY;
		}
		
//		Spinner subcategorySpinner = (Spinner) findViewById(R.id.event_edit_subcategory_spinner);
//		if (subcategorySpinner.getItemAtPosition(0).toString().equals(subcategorySpinner.getSelectedItem().toString())) {
//			return EventVerification.VERIFICATION_CHOOSE_SUBCATEGORY;
//		}
//		
//		Spinner numparticipantsSpinner = (Spinner) findViewById(R.id.event_edit_num_participants_spinner);
//		if (numparticipantsSpinner.getItemAtPosition(0).toString().equals(numparticipantsSpinner.getSelectedItem().toString())) {
//			return EventVerification.VERIFICATION_CHOOSE_NUMPARTICIPANTS;
//		}
//		
//		Spinner howoftenSpinner = (Spinner) findViewById(R.id.event_edit_howoften_spinner);
//		if (howoftenSpinner.getItemAtPosition(0).toString().equals(howoftenSpinner.getSelectedItem().toString())) {
//			return EventVerification.VERIFICATION_CHOOSE_HOWOFTEN;
//		}
		
		return EventVerification.VERIFICATION_OK;
	}
}
