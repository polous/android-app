package de.tum.bgu.lfk.openeventmap;

public class EventIdentifier {

	public final String osmType;
	public final long osmId;
	public final long num;
	public final String name;
	
	public EventIdentifier(String type, long id, long num, String name) {
		this.osmType = type;
		this.osmId = id;
		this.num = num;
		this.name = name;
	}
}
