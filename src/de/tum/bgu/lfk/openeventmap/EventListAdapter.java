package de.tum.bgu.lfk.openeventmap;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import de.blau.android.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.content.Context;

public class EventListAdapter extends BaseAdapter {

	private Context mContext;
	private Collection<Event> mEvents;
	
	// Two types of list items are available.
	// One for categories and one for the events of the categories
	public static final int TYPE_EVENT = 0;
	public static final int TYPE_CATEGORY = 1;
	private static final int TYPE_COUNT = TYPE_CATEGORY + 1;
	
	private static final String CATEGORY_OTHER = "other";
	
	/** Contains all possible categories */
	private List<String> mCategories;
	/** Contains all categories of the events*/
	private List<String> mAvailableCategories = new LinkedList<String>();
	/** Saves whether the available categories are expanded or not */
	private List<Boolean> mExpandedCategories = new LinkedList<Boolean>();
	/** Saves the number of events of the available categories */
	private List<Integer> mSizeCategories = new LinkedList<Integer>();
	
	
	public EventListAdapter(Context context, final Collection<Event> events) {
		mContext = context;
		mEvents = events;
		
		// Insert all possible categories into mCategories
		String[] categories = mContext.getResources().getStringArray(R.array.event_tag_category_values);
		categories[0] = CATEGORY_OTHER;
		mCategories = new LinkedList<String>();
		for (int i = 0; i < categories.length; i++) {
			mCategories.add(categories[i]);
		}
		
		// Insert all available categories into mAvailableCategories
		for (Event e : events) {
			String c = getCategory(e, mCategories);
			if (!mAvailableCategories.contains(c)) {
				mAvailableCategories.add(c);
			}				
		}
		
		// Order available categories to be the same as in mCategories
		LinkedList<String> availableCategories = new LinkedList<String>();
		for (String s : mCategories) {
			if (mAvailableCategories.contains(s)) {
				availableCategories.add(s);
			}
		}
		mAvailableCategories = availableCategories;
		
		// init the helper lists
		for (int i = 0; i < mAvailableCategories.size(); i++) {
			mExpandedCategories.add(false);
			mSizeCategories.add(0);
		}
		
		// Count the elements in each available category
		for (Event e : mEvents) {
			String c = getCategory(e, mAvailableCategories);
			int catPosition = mAvailableCategories.indexOf(c);
			int catCount = mSizeCategories.get(catPosition) + 1;
			mSizeCategories.set(catPosition, catCount);
		}
	}

	private String getCategory(Event e, List<String> searchSpace) {
		String c;
		if (e.getCategory() == null || e.getCategory().length() == 0) {
			c = CATEGORY_OTHER;
		} else {
			c = e.getCategory();
			if (!searchSpace.contains(c)) {
				c = CATEGORY_OTHER;
			}
		}
		return c;
	}
	
	@Override
	public int getViewTypeCount() {
		return TYPE_COUNT;
	}
	
	@Override
	public int getItemViewType(int position) {
		int p = -1;
		for (int catPos = 0; catPos < mAvailableCategories.size(); catPos++) {
			p++;
			if (p >= position) {
				return TYPE_CATEGORY;
			}
			if (mExpandedCategories.get(catPos)) {
				p += mSizeCategories.get(catPos);
			}
			if (p >= position) {
				return TYPE_EVENT;
			}			
		}
		
		return TYPE_EVENT;
	}
	
	@Override
	public int getCount() { 
		int count = 0;
		count += mAvailableCategories.size();
		for (int catPos = 0; catPos < mAvailableCategories.size(); catPos++) {
			if (mExpandedCategories.get(catPos)) {
				count += mSizeCategories.get(catPos);
			}
		}
		
		return count;
	}

	@Override
	public Object getItem(int position) {
		int p = -1;
		for (int catPos = 0; catPos < mAvailableCategories.size(); catPos++) {
			p++;
			if (p >= position) {
				return mAvailableCategories.get(catPos); // return String if a category is selected
			}
			if (mExpandedCategories.get(catPos)) {
				p += mSizeCategories.get(catPos);
			}
			if (p >= position) {
				// find the event to be returned
				String category = mAvailableCategories.get(catPos);
				int i = p - position; // the i'th event of category is to be returned
				int ic = -1;
				for (Event e : mEvents) {
					String c = getCategory(e, mAvailableCategories);
					if (c.equals(category)) {
						ic++;
					}
					if (i == ic) {
						return e; // return event if an event is selected
					}
				}
			}			
		}
		
		return null; // This should never happen
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {  

		int type = getItemViewType(position);
		
		if (convertView == null) {
			LayoutInflater layoutInflator = LayoutInflater.from(mContext);
			switch (type) {
			case TYPE_CATEGORY:
				convertView = layoutInflator.inflate(R.layout.event_search_list_category_item, null);
				break;
			case TYPE_EVENT:
				convertView = layoutInflator.inflate(R.layout.event_search_list_item, null);
			}
		}

		switch (type) {
		case TYPE_CATEGORY:
			TextView categoryName = (TextView) convertView.findViewById(R.id.event_search_list_category_item_category);
			TextView eventNumber = (TextView) convertView.findViewById(R.id.event_search_list_category_item_number);
			
			String category = (String) getItem(position);
			categoryName.setText(Misc.capitalizeString(category));
			categoryName.setTextColor(Event.getColorForCategegory(category));
			int number = mSizeCategories.get(mAvailableCategories.indexOf(category));
			eventNumber.setText(mContext.getResources().getQuantityString(R.plurals.numberOfEventsFound, number, number ));
			break;
		case TYPE_EVENT:
			TextView eventName = (TextView) convertView.findViewById(R.id.event_search_list_item_name);
			TextView eventCategory = (TextView) convertView.findViewById(R.id.event_search_list_item_category);
			TextView eventDates = (TextView) convertView.findViewById(R.id.event_search_list_item_dates);
			
			Event event = (Event) getItem(position);
			eventName.setText(event.getName() != null ? event.getName() : "");
			eventCategory.setText(event.getCategory() != null ? Misc.capitalizeString(event.getCategory()) : "");
			eventCategory.setTextColor(Event.getColorForCategegory(event.getCategory() != null ? event.getCategory() : "other"));
			eventDates.setText(Misc.formatDate(mContext, event.getStartdate()) + " - " + Misc.formatDate(mContext, event.getEnddate()));
			break;
		}

		return convertView;
	}

	public void expandCategory(String category) {
		int catPos = mAvailableCategories.indexOf(category);
		boolean expanded = mExpandedCategories.get(catPos);
		mExpandedCategories.set(catPos, !expanded);
		notifyDataSetChanged();
	}
}