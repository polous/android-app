package de.tum.bgu.lfk.openeventmap;

import java.io.Serializable;
import java.util.Date;

import de.blau.android.osm.BoundingBox;

/**
 * Data structure passed from EventSearch-Activity to SearchableActivity
 * containing information about what to search for.
 * 
 * @author mark
 *
 */
public class EventQuery implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public String name = null;
	public String category = null;
	public Date startdate = null;
	public Date enddate = null;
	public BoundingBox searchArea = null;
	
}
