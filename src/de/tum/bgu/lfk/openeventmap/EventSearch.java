package de.tum.bgu.lfk.openeventmap;

import java.text.DateFormat;
import java.util.Date;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import de.blau.android.R;
import de.blau.android.osm.BoundingBox;

/**
 * This activity is used to gather information from the user about what events to search
 * for in the SQL database.
 * 
 * @author mark
 *
 */
public class EventSearch extends SherlockFragmentActivity implements DatePickerFragment.DateSetListener {

	public static final String EVENT_QUERY_EXTRA = "de.tum.bgu.lfk.openeventmap.EventSearch.extra_event_query";
	public static final String SEARCH_AREA_EXTRA = "de.tum.bgu.lfk.openeventmap.EventSearch.extra_search_area";
	
	public static final String ACTION_EXTRA = "de.tum.bgu.lfk.openeventmap.EventSearch.extra_action";
	public static final int ACTION_SEARCH = 0;
	public static final int ACTION_BACK = 1;
	
	public static final String LOG_TAG = "EventSearch";

	private BoundingBox searchArea = null;
	
	private ArrayAdapter<CharSequence> categoryAdapter;

	private Date startdate;
	private Date enddate;
		
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.event_search);
		
		setTitle(getString(R.string.event_search_title));
        
		Intent startedBy = getIntent();
		searchArea = (BoundingBox) startedBy.getSerializableExtra(SEARCH_AREA_EXTRA);
		
		initViews();
		
		ActionBar actionbar = getSupportActionBar();
		actionbar.setDisplayShowTitleEnabled(true);
	}
	
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_search_back, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			return true;
		case R.id.menu_search_back_search:
			
			searchButtonClicked(findViewById(R.id.event_search_menu_search));
			
			finish();
			return true;
		case R.id.menu_search_back_back:
			
			Intent resultIntent = new Intent();
			resultIntent.putExtra(EventSearch.ACTION_EXTRA, EventSearch.ACTION_BACK);
			setResult(RESULT_OK, resultIntent);
			
			finish();
			return true;
		}
		
		return false;
	}
	
	private void initViews() {
		
		Spinner categorySpinner = (Spinner) findViewById(R.id.event_search_category_spinner);
		// Create an ArrayAdapter using the string array and a default spinner layout
		categoryAdapter = ArrayAdapter.createFromResource(this,
		        R.array.event_tag_category_values_with_all, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		categorySpinner.setAdapter(categoryAdapter);
		categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				// Do nothing
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// Do nothing
			}
			
		});
		
		Intent intent = getIntent();
		EventQuery eventQuery = (EventQuery) intent.getSerializableExtra(EventSearch.EVENT_QUERY_EXTRA);
		if (eventQuery != null) {
			if (eventQuery.name != null) {
				TextView eventNameValue = (TextView) findViewById(R.id.event_search_name_value);
				eventNameValue.setText(eventQuery.name);
			}
			if (eventQuery.category != null) {
				categorySpinner.setSelection(categoryAdapter.getPosition(eventQuery.category));
			}
			startdate = eventQuery.startdate;
			updateStartdate();
			enddate = eventQuery.enddate;
			updateEnddate();
			searchArea = eventQuery.searchArea;
		}
	}
	
	public void startdateSetButtonClicked(View view) {
		DialogFragment newFragment = new DatePickerFragment();
		Bundle b = new Bundle();
		b.putString("variable", "startdate");
		if (startdate != null) {
			b.putLong("time", startdate.getTime());
		}
		newFragment.setArguments(b);
		newFragment.show(getSupportFragmentManager(), "DatePickerFragment");
	}

	public void startdateRemoveButtonClicked(View view) {
		startdate = null;
		updateStartdate();
	}
	
	private void updateStartdate() {
		TextView v = (TextView) findViewById(R.id.event_search_startdate_text_value);
		if (startdate == null) {
			v.setText(getString(R.string.event_view_value_unspecified));
		} else {
			v.setText(DateFormat.getDateInstance().format(startdate));
		}
	}
	
	public void enddateSetButtonClicked(View view) {
		DialogFragment newFragment = new DatePickerFragment();
		Bundle b = new Bundle();
		b.putString("variable", "enddate");
		if (enddate != null) {
			b.putLong("time", enddate.getTime());
		}
		newFragment.setArguments(b);
		newFragment.show(getSupportFragmentManager(), "DatePickerFragment");
	}

	public void enddateRemoveButtonClicked(View view) {
		enddate = null;
		updateEnddate();
	}

	private void updateEnddate() {
		TextView v = (TextView) findViewById(R.id.event_search_enddate_text_value);
		if (enddate == null) {
			v.setText(getString(R.string.event_view_value_unspecified));
		} else {
			v.setText(DateFormat.getDateInstance().format(enddate));
		}
	}

	@Override
	public void onDateSet(String variable, Date date) {
		if (variable.equals("startdate")) {
			startdate = date;
			updateStartdate();
		}
		if (variable.equals("enddate")) {
			enddate = date;
			updateEnddate();
		}
	}
	
	public void searchButtonClicked(View view) {
		// Use the entered values to find matching events
		EditText nameEdit = (EditText) findViewById(R.id.event_search_name_value);
		Spinner categorySpinner = (Spinner) findViewById(R.id.event_search_category_spinner);
		
		String name = nameEdit.getText().toString();
		String category = categorySpinner.getSelectedItem().toString();
		
		boolean validName = (name.length() > 0);
		boolean validCate = (category.length() > 0);
		
		if (!validName && !validCate) {
			Log.w(LOG_TAG, "Neither a search name nor category was specified!");
			return;
		}
		
		EventQuery query = new EventQuery();
		if (name != null && name.length() > 0) {
			query.name = name;
		}
		if (category != null && category.length() > 0) {
			query.category = category;
		}
		if (startdate != null) {
			query.startdate = startdate;
		}
		if (enddate != null) {
			query.enddate = enddate;
		}
		if (searchArea != null) {
			query.searchArea = searchArea;
		}
		
		Intent resultIntent = new Intent();
		resultIntent.putExtra(EventSearch.EVENT_QUERY_EXTRA, query);
		resultIntent.putExtra(EventSearch.ACTION_EXTRA, EventSearch.ACTION_SEARCH);
		setResult(RESULT_OK, resultIntent);
	}
	
}
