package de.tum.bgu.lfk.openeventmap;

import java.text.DateFormat;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import de.blau.android.R;

public class EventViewer extends SherlockActivity {

	public static final String EVENT_EXTRA = "de.tum.bgu.lfk.openeventmap.EventViewer.extra_event";
	
	public static final String ACTION_EXTRA = "de.tum.bgu.lfk.openeventmap.EventViewer.extra_action";
	public static final int ACTION_EDIT = 0;
	public static final int ACTION_BACK = 1;
	public static final int ACTION_ADD  = 2;
	public static final int ACTION_SHOWONMAP = 3;
	
	private Event event;

	private Intent resultIntent;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.event_view);
		
		setTitle(getString(R.string.event_view_title));
		
		loadEvent();
		
		ActionBar actionbar = getSupportActionBar();
		actionbar.setDisplayShowTitleEnabled(true);
	}
	
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_edit_back, menu);
		inflater.inflate(R.menu.menu_add, menu);
		inflater.inflate(R.menu.menu_showonmap, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			return true;
		case R.id.menu_edit_back_edit:
			resultIntent = new Intent();		
			resultIntent.putExtra(EVENT_EXTRA, event);
			resultIntent.putExtra(ACTION_EXTRA, ACTION_EDIT);
			setResult(RESULT_OK, resultIntent);
			finish();
			return true;
		case R.id.menu_edit_back_back:
			resultIntent = new Intent();
			resultIntent.putExtra(ACTION_EXTRA, ACTION_BACK);
			setResult(RESULT_OK, resultIntent);
			finish();
			return true;
		case R.id.menu_add_add:
			resultIntent = new Intent();		
			resultIntent.putExtra(EVENT_EXTRA, event);
			resultIntent.putExtra(ACTION_EXTRA, ACTION_ADD);
			setResult(RESULT_OK, resultIntent);
			finish();
			return true;
		case R.id.menu_showonmap_show:
			resultIntent = new Intent();
			resultIntent.putExtra(EVENT_EXTRA, event);
			resultIntent.putExtra(ACTION_EXTRA, ACTION_SHOWONMAP);
			setResult(RESULT_OK, resultIntent);
			finish();
		}
		
		return false;
	}
	
	private void loadEvent() {
		
		event = (Event) getIntent().getSerializableExtra(EventViewer.EVENT_EXTRA);
		
		TextView nameView = (TextView) findViewById(R.id.event_view_name_value);
		TextView categoryView = (TextView) findViewById(R.id.event_view_category_value);
		TextView subcategoryView = (TextView) findViewById(R.id.event_view_subcategory_value);
		TextView startdateView = (TextView) findViewById(R.id.event_view_startdate_value);
		TextView enddateView = (TextView) findViewById(R.id.event_view_enddate_value);
		TextView urlView = (TextView) findViewById(R.id.event_view_url_value);
		TextView numParticipantsView = (TextView) findViewById(R.id.event_view_num_participants_value);
		TextView howoftenView = (TextView) findViewById(R.id.event_view_howoften_value);
		TextView commentView = (TextView) findViewById(R.id.event_view_comment_value);
		TextView organizationView = (TextView) findViewById(R.id.event_view_organization_value);
		TextView relatedNodesView = (TextView) findViewById(R.id.event_view_related_nodes_value);
		TextView relatedWaysView = (TextView) findViewById(R.id.event_view_related_ways_value);
		TextView relatedRelationsView = (TextView) findViewById(R.id.event_view_related_relations_value);	
		
		nameView.setText(event.getName());
		categoryView.setText(event.getCategory());
		subcategoryView.setText(event.getSubcategory());
		if (event.getStartdate() != null) {
			startdateView.setText(DateFormat.getDateInstance().format(event.getStartdate()));
		} else {
			startdateView.setText(getString(R.string.event_view_value_unspecified));
		}
		if (event.getEnddate() != null) {
			enddateView.setText(DateFormat.getDateInstance().format(event.getEnddate()));
		} else {
			enddateView.setText(getString(R.string.event_view_value_unspecified));
		}
		urlView.setText(event.getUrl());
		urlView.setTextColor(Color.YELLOW);
		// Open browser when the user clicks on the events URL
		urlView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String uri = event.getUrl();
				if (uri == null) {
					Toast.makeText(EventViewer.this, "There is no URL specified for this event.", Toast.LENGTH_SHORT).show();
					return;
				}
				
				Uri u = Uri.parse(uri);
				if (u == null || u.getScheme() == null) {
					u = Uri.parse("http://" + uri);
					
				} else if (!(u.getScheme().equals("http") || u.getScheme().equals("https"))) {
					Toast.makeText(EventViewer.this, "This given URL is not suited to be displayed in browsers.", Toast.LENGTH_SHORT).show();
					return;
				}
				if (u != null) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, u);
					startActivity(browserIntent);
				} else {
					Toast.makeText(EventViewer.this, "This is not a valid browser URL.", Toast.LENGTH_SHORT).show();
					return;
				}
				
			}
		});
		numParticipantsView.setText(event.getNumParticipants());
		howoftenView.setText(event.getHowoften());
		commentView.setText(event.getComment());
		organizationView.setText(event.getOrganization());
		relatedNodesView.setText(String.format(getString(R.string.nodes_d), event.numberOfRelatedNodes()));
		relatedWaysView.setText(String.format(getString(R.string.ways_d), event.numberOfRelatedWays()));
		relatedRelationsView.setText(String.format(getString(R.string.relations_d), event.numberOfRelatedRelations()));
	}
}
