package de.tum.bgu.lfk.openeventmap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import de.blau.android.Application;
import de.blau.android.Main;
import de.blau.android.Map;
import de.blau.android.R;
import de.blau.android.osm.BoundingBox;
import de.blau.android.osm.Server;
import de.blau.android.util.GeoMath;
import de.blau.android.views.IMapView;
import de.blau.android.views.overlay.OpenStreetMapViewOverlay;

public class MapOverlay extends OpenStreetMapViewOverlay {
	
	
	private final Map map;
	private final Server server;
	
	private Rect prev;
	private Rect cur;
	private Rect viewPort;
	private float radius;
	private static final float ratio = 18.0f / 23.0f;
	
	private static Bitmap markerBitmap;
	
	private static final int DRAWING_MODE_CIRCLES = 0;
	private static final int DRAWING_MODE_MARKERS = 1;
	private static int drawingMode = DRAWING_MODE_MARKERS;
	
	private Collection<Event> events;
	
	private Handler handler;

	private static final String LOG_TAG = "MARK";
	
	// Following variables are needed to control what is beeing drawn on the map
	// and whether new events have to be downloaded for the current view box
	// Disable automated download for current map
	private boolean shouldGetEvents = false;
	
	private boolean isLegendVisible = false;
	
	/**
	 * Request to update the events for the current view.
	 * Ensure cur is set before invoking.
	 */
	private final Runnable getEvents = new Runnable() {
		public void run() {
			new AsyncTask<Void, Void, Collection<Event>>() {
				@Override
				protected Collection<Event> doInBackground(Void... params) {
					return server.getEventsForBox(cur);
				}

				@Override
				protected void onPostExecute(Collection<Event> result) {
					prev.set(cur);
					events.clear();
					events.addAll(result);
					if (!events.isEmpty()) {
						map.invalidate();
					}
				}

			}.execute();
		}
	};
	
	static {
		markerBitmap = BitmapFactory.decodeResource(Application.mainActivity.getResources(), R.drawable.markers_soft_half);
		Log.d(LOG_TAG, "bitmap width = " + String.valueOf(markerBitmap.getWidth()) + " ; bitmap height = " + String.valueOf(markerBitmap.getHeight()));
	}
	
	public MapOverlay(Map m, Server s) {
		this.map = m;
		this.server = s;
		this.prev = new Rect();
		this.cur = new Rect();
		this.viewPort = new Rect();
		this.radius = 3.0f;
		this.events = new ArrayList<Event>();
		this.handler = new Handler();
	}
	
	@Override
	protected void onDraw(Canvas c, IMapView osmv) {
		viewPort = c.getClipBounds();
		BoundingBox bb = osmv.getViewBox();

		cur.set(bb.getLeft(), bb.getTop(), bb.getRight(), bb.getBottom());
		if (!cur.equals(prev)) {
			// map has moved/zoomed - need to refresh the events on display
			// don't flood database with requests - wait for 2s
			if (shouldGetEvents) {
				handler.removeCallbacks(getEvents);
				handler.postDelayed(getEvents, 2000);
			}
		}
		// the idea is to have the circles a bit bigger when zoomed in, not so
		// big when zoomed out
		radius = 10.0f + (float) osmv.getZoomLevel(viewPort) / 2.0f;
		if (osmv.getZoomLevel(viewPort) >= 18) radius += 5.0f;

		if (drawingMode == DRAWING_MODE_CIRCLES) {
			// Following code draws the events as circles
			Paint paint1 = new Paint();
			paint1.setColor(Color.argb(255, 255, 255, 255));
			Paint paint2 = new Paint();
			paint2.setColor(Color.argb(255, 255, 0, 0));
			for (Event event : events) {
				if (bb.isIn(event.getLat(), event.getLon())) {
					float x = GeoMath.lonE7ToX(viewPort.width() , bb, event.getLon());
					float y = GeoMath.latE7ToY(viewPort.height(), bb, event.getLat());
					paint2.setColor(Event.getColorForCategegory(event.getCategory()));
					// draw all the events on the map as slightly transparent circles
					c.drawCircle(x, y, radius, paint1);
					c.drawCircle(x, y, radius-3f, paint2);
				}
			}
		} else if (drawingMode == DRAWING_MODE_MARKERS) {
			// Following code draws the events as markers
			if (markerBitmap == null) {
				Log.e(LOG_TAG, "MapOverlay.onDraw(): Marker bitmap could not be decoded correctly!");
				return;
			} else {
				for (Event event : events) {
					if (bb.isIn(event.getLat(), event.getLon())) {
						float x = GeoMath.lonE7ToX(viewPort.width() , bb, event.getLon());
						float y = GeoMath.latE7ToY(viewPort.height(), bb, event.getLat());
						Rect srcRect = Event.getRectForCategory(event.getCategory());
						RectF dstRect = new RectF();
						dstRect.set(x - radius, y - 2 * (radius / ratio), x + radius, y);
						c.drawBitmap(markerBitmap, srcRect, dstRect, null);
					}
				}
			}
		} else {
			Log.w(LOG_TAG, "MapOverlay: Drawing mode not set correctly!");
		}
		// Draw a legend
		if (isLegendVisible) {
			Main main = Application.mainActivity;
			// Find the position of the legend button on the map.
			int buttonLeft = main.getLegendButtonLeft();
			int buttonTop = main.getLegendButtonTop();			
			
			int marginLeft = 13;
			int markerWidth = 26;
			int markerHeight = markerWidth * 23 / 18; // keep original aspect ratio
			int textSize = 18;
			int textToMarker = 10;
			
			int legendRowHeight = 40;
			int legendHeight = 11 * legendRowHeight;
			int legendWidth = 182;
			int legendLeft = buttonLeft; // align legend with button horizontally
			int legendTop = buttonTop - legendHeight;
			
			// draw a half transparent rectangle as background for the legend
			Paint backgroundPaint = new Paint();
//			backgroundPaint.setColor(Color.argb(127, 127, 127, 127)); // half transparent gray background
			backgroundPaint.setColor(Color.argb(127, 255, 255, 255)); // half transparent white background
			c.drawRect(legendLeft, legendTop, legendLeft+legendWidth, legendTop+legendHeight, backgroundPaint);
			
			// draw markers and category names
			String[] categories = Application.mainActivity.getResources().getStringArray(R.array.event_tag_category_values);
			for (int category = 1; category < categories.length; category++) {
				Rect srcRect = Event.getRectForCategory(categories[category]);
				int verticalOffset = (category - 1) * legendRowHeight + 10;
				Rect dstRect = new Rect(legendLeft+marginLeft, legendTop+verticalOffset-5, legendLeft+marginLeft+markerWidth, legendTop+verticalOffset-5+markerHeight);
				c.drawBitmap(markerBitmap, srcRect, dstRect, null);
				Paint textPaint = new Paint();
//				textPaint.setColor(Event.getColorForCategegory(categories[category])); // text color based on category
				textPaint.setColor(Color.argb(255, 0, 0, 0)); // opaque black text
				textPaint.setTextSize(textSize);
				c.drawText(categories[category], legendLeft+marginLeft+markerWidth + textToMarker, legendTop+verticalOffset+textSize, textPaint);
			}
		}
	}

	@Override
	protected void onDrawFinished(Canvas c, IMapView osmv) {
	}

	public List<Event> getClickedEvents(float x, float y, BoundingBox viewBox) {
		List<Event> result = new ArrayList<Event>();
		final float tolerance = radius;
		for (Event event : events) {
			int lat = event.getLat();
			int lon = event.getLon();
			float eventX = GeoMath.lonE7ToX(map.getWidth(), viewBox, lon);
			float eventY = GeoMath.latE7ToY(map.getHeight(), viewBox, lat);
			if (drawingMode == DRAWING_MODE_CIRCLES) {
				float differenceX = Math.abs(eventX - x);
				float differenceY = Math.abs(eventY - y);
				if ((differenceX <= tolerance) && (differenceY <= tolerance)) {
					if (Math.hypot(differenceX, differenceY) <= tolerance) {
						result.add(event);
					}
				}
			} else if (drawingMode == DRAWING_MODE_MARKERS) {
				if (x > eventX - radius &&
						x < eventX + radius &&
						y > eventY - 2 * (radius / ratio) &&
						y < eventY) {
					result.add(event);
				}
			} else {
				Log.w(LOG_TAG, "MapOverlay: Drawing mode not set correctly!");					
			}
		}
		return result;
	}

	public void showEventList(Collection<Event> eventList) {
		// Stop downloading events for the current view box
		shouldGetEvents = false;
		handler.removeCallbacks(getEvents);
		events = eventList;
	}

	public void showEventsInViewBox() {
		// Resume downloading events for the current view box
		// shouldGetEvents = true;
	}
	
	public void getEvents() {
		handler.post(getEvents);
	}

	public void toggleLegendVisibility() {
		isLegendVisible = !isLegendVisible;
	}
	
}
