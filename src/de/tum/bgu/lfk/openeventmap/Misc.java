package de.tum.bgu.lfk.openeventmap;

import java.text.DateFormat;
import java.util.Date;

import android.content.Context;
import de.blau.android.R;

/**
 * This class offers functions that are performed in many different activities and
 * classes of the application.
 * 
 * @author mark
 *
 */
public class Misc {

	public static String formatDate(Context context, Date date) {
		if (date == null) {
			return context.getString(R.string.event_view_value_unspecified);
		} else {
			return DateFormat.getDateInstance().format(date);
		}
	}
	
	public static String capitalizeString(String s) {
		if (s == null) {
			return null;
		}
		if (s.length() == 0)
		{
			return "";
		}
		String result = s.substring(0, 1).toUpperCase();
		result += s.substring(1);
		
		return result;
	}
}
