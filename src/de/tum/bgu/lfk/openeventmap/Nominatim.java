package de.tum.bgu.lfk.openeventmap;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

public class Nominatim {

	private static final String nominatimSearchURL = "http://nominatim.openstreetmap.org/search/";
	private static final int TIMEOUT = 3 * 1000;
		
	private static final String LOG_TAG = "MARK";
	
	public static List<SearchResult> getSearchResults(final String query) throws IOException {
		List<SearchResult> result = null;
		
		InputStream in = null;
		
		try {
			SearchParser searchParser = new SearchParser();
			in = getStreamForNominatimSearch(query);

			searchParser.start(in);
			result = searchParser.getPlaces();
		} catch (Exception e) {
			Log.e(LOG_TAG, "Nominatim: Problem parsing", e);
		} finally {
			if (in != null)	in.close();
		}
		return result;
	}
	
	private static InputStream getStreamForNominatimSearch(final String query) throws IOException {
		
		Log.d(LOG_TAG, "getStreamForNominatimSearch: Getting stream for query: " + query);
		
		String q = URLEncoder.encode(query, "UTF-8");
		String nominatimParameters = "format=xml&countrycodes=de&limit=10&bounded=1&viewbox=11.305,48.274,12.482,47.675";
		URL url = new URL(nominatimSearchURL + q + "?" + nominatimParameters);
		
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		boolean isServerGzipEnabled = false;

		Log.d(LOG_TAG, "getStreamForNominatimSearch: url = " + url.toString());
		
		//--Start: header not yet send
		con.setReadTimeout(TIMEOUT);
		con.setConnectTimeout(TIMEOUT);
		con.setRequestProperty("Accept-Encoding", "gzip");

		//--Start: got response header
		isServerGzipEnabled = "gzip".equals(con.getHeaderField("Content-encoding"));

		if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
			Log.e(LOG_TAG, "Nominatim: Couldn't connect to server!");
			return null;
		}
		
		if (isServerGzipEnabled) {
			return new GZIPInputStream(con.getInputStream());
		} else {
			return con.getInputStream();
		}
	}
	
	public static class SearchResult implements Serializable {

		private static final long serialVersionUID = 1L;
		
		public final String display_name;
		public final Integer lat;
		public final Integer lon;
		
		public SearchResult(String dn, Integer lat, Integer lon) {
			this.display_name = dn;
			this.lat = lat;
			this.lon = lon;
		}
		
		@Override
		public String toString() {
			return display_name;
		}
	}
	
	private static class SearchParser extends DefaultHandler {

		private List<SearchResult> places;

		public List<SearchResult> getPlaces() {
			return places;
		}

		public SearchParser() {
			super();
			places = new ArrayList<SearchResult>();
		}
		
		public void start(final InputStream in) throws SAXException, IOException, ParserConfigurationException {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(in, this);
		}
		
		@Override
		public void startElement(final String uri, final String name, final String qName, final Attributes atts) {
			if (name.equals("place")) {
				String dn = atts.getValue("display_name");
				Integer lat = (int) (Double.valueOf(atts.getValue("lat")) * 1E7);
				Integer lon = (int) (Double.valueOf(atts.getValue("lon")) * 1E7);
				places.add(new SearchResult(dn, lat, lon));
			}
		}
	}
	
}
