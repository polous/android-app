package de.tum.bgu.lfk.openeventmap;

public class OsmElementIdentifier {

	public final String osmType;
	public final long osmId;
	public final String name;
	
	public OsmElementIdentifier(String type, long id, String name) {
		this.osmType = type;
		this.osmId = id;
		this.name = name;
	}
	
}
