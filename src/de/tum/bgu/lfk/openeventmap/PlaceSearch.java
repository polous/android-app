package de.tum.bgu.lfk.openeventmap;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import de.blau.android.R;

/**
 * This activity is used to gather information from the user about what events to search
 * for in the SQL database.
 * 
 * @author mark
 *
 */
public class PlaceSearch extends SherlockFragmentActivity {

	// The following extras serve as input to this activity
	public static final String PLACE_QUERY_EXTRA = "de.tum.bgu.lfk.openeventmap.PlaceSearch.extra_place_query";
	private String query;
	
	// The following extras serve as output of this activity
	public static final String ACTION_EXTRA = "de.tum.bgu.lfk.openeventmap.PlaceSearch.extra_action";
	public static final int ACTION_SEARCH = 0;
	public static final int ACTION_BACK = 1;
	
	public static final String LOG_TAG = "PlaceSearch";
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.place_search);
		
		setTitle(getString(R.string.place_search_title));
        
		Intent startedBy = getIntent();
		query = startedBy.getStringExtra(PLACE_QUERY_EXTRA);
		
		initViews();
		
		ActionBar actionbar = getSupportActionBar();
		actionbar.setDisplayShowTitleEnabled(true);
	}
	
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_search_back, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			return true;
		case R.id.menu_search_back_search:
			
			searchButtonClicked(null);
			
			finish();
			return true;
		case R.id.menu_search_back_back:
			
			Intent resultIntent = new Intent();
			resultIntent.putExtra(PlaceSearch.ACTION_EXTRA, PlaceSearch.ACTION_BACK);
			setResult(RESULT_OK, resultIntent);
			
			finish();
			return true;
		}
		
		return false;
	}
	
	private void initViews() {		
		if (query != null) {
			EditText queryEdit = (EditText) findViewById(R.id.place_search_query_value);
			queryEdit.setText(query);
		}
	}
	
	public void searchButtonClicked(View view) {
		EditText queryEdit = (EditText) findViewById(R.id.place_search_query_value);
		
		query = queryEdit.getText().toString();
		
		if (query.length() == 0) {
			Toast.makeText(this, "Please enter a place to search for.", Toast.LENGTH_SHORT).show();
			return;
		} 
		
		Intent resultIntent = new Intent();
		resultIntent.putExtra(PlaceSearch.PLACE_QUERY_EXTRA, query);
		resultIntent.putExtra(PlaceSearch.ACTION_EXTRA, PlaceSearch.ACTION_SEARCH);
		setResult(RESULT_OK, resultIntent);
	}
	
}
