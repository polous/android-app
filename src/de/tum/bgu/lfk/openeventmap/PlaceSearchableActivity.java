package de.tum.bgu.lfk.openeventmap;

import java.io.IOException;
import java.util.List;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import de.blau.android.R;

/**
 * This activity is used to perform a place search and display the results. Its
 * design is inspired but not strictly adheres to the official android design
 * described at http://developer.android.com/guide/topics/search/search-dialog.html
 * 
 * @author mark
 *
 */
public class PlaceSearchableActivity extends SherlockListActivity {

	// The following extras serve as input to this activity
	public static final String PLACE_QUERY_EXTRA = "de.tum.bgu.lfk.openeventmap.PlaceSearchableActivity.extra_place_query";

	// The following extras serve as output of this activity
	public static final String ACTION_EXTRA = "de.tum.bgu.lfk.openeventmap.PlaceSearchableActivity.extra_action";
	public static final int ACTION_VIEW = 0;
	public static final int ACTION_BACK = 1;
	public static final String SEARCH_RESULT_EXTRA = "de.tum.bgu.lfk.openeventmap.PlaceSearchableActivity.extra_search_result";
	
	public static final String LOG_TAG = "MARK";
	
	private List<Nominatim.SearchResult> searchResults;

	private static final int DIALOG_ID_PROGRESS = 0;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setTitle(getString(R.string.place_searchable_activity_title));
				
		ActionBar actionbar = getSupportActionBar();
		actionbar.setDisplayShowTitleEnabled(true);
		
	    // Get the intent, verify the action and get the query
	    Intent intent = getIntent();
	    if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
	      String query = intent.getStringExtra(PlaceSearchableActivity.PLACE_QUERY_EXTRA);
	      searchPlace(query);
	    }

	}
	
	private void searchPlace(final String query) {
		Log.d(LOG_TAG, "searchPlace() called!");
		
		new AsyncTask<Void, Void, Void>() {
			
			@SuppressWarnings("deprecation")
			@Override
			protected void onPreExecute() {
				Log.d(LOG_TAG, "searchPlace() onPreExecute() called!");
				showDialog(DIALOG_ID_PROGRESS);
			}
			
			@Override
			protected Void doInBackground(Void... arg) {
				Log.d(LOG_TAG, "searchPlace() doInBackground() called!");
				try {
					searchResults = Nominatim.getSearchResults(query);
				} catch (IOException e) {
					Log.e(LOG_TAG, "searchPlace() with exception: ", e);
				}
				return null;
			}
			
			@SuppressWarnings("deprecation")
			@Override
			protected void onPostExecute(Void result) {
				Log.d(LOG_TAG, "searchPlace() onPostExecute() called!");
				try {
					dismissDialog(DIALOG_ID_PROGRESS);
				} catch (IllegalArgumentException e) {
					 // Avoid crash if dialog is already dismissed
					Log.d(LOG_TAG, "Loading dialog was alreday dismissed!", e);
				}
				
				// Display found places in the ListView
				PlaceSearchableActivity.this.setListAdapter(
						new ArrayAdapter<Nominatim.SearchResult>(PlaceSearchableActivity.this,
								android.R.layout.simple_list_item_1, searchResults));
			}
			
		}.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			return true;
		case R.id.menu_back_back:

			Intent resultIntent = new Intent();
			resultIntent.putExtra(PlaceSearchableActivity.ACTION_EXTRA, PlaceSearchableActivity.ACTION_BACK);
			setResult(RESULT_OK, resultIntent);
			
			finish();
			return true;
		}
		
		return false;
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Nominatim.SearchResult searchResult = searchResults.get(position);

		Intent resultIntent = new Intent();
		resultIntent.putExtra(PlaceSearchableActivity.SEARCH_RESULT_EXTRA, searchResult);
		resultIntent.putExtra(PlaceSearchableActivity.ACTION_EXTRA, PlaceSearchableActivity.ACTION_VIEW);
		setResult(RESULT_OK, resultIntent);
		
		finish();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		
		case DIALOG_ID_PROGRESS :
			ProgressDialog progress = new ProgressDialog(PlaceSearchableActivity.this);
			progress.setTitle("Searching Places...");
			progress.setIndeterminate(true);
			progress.setCancelable(true);
			progress.setMessage("Receiving Search Results.");
			return progress;		
		}
		
		return super.onCreateDialog(id);
	}
}
