package de.tum.bgu.lfk.openeventmap;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import de.blau.android.Application;
import de.blau.android.DialogFactory;
import de.blau.android.R;

/**
 * This activity is used to perform an event search and display the results. Its
 * design is inspired but not strictly adheres to the official android design
 * described at http://developer.android.com/guide/topics/search/search-dialog.html
 * 
 * @author mark
 *
 */
public class SearchableActivity extends SherlockListActivity {
	
	public static final String EVENT_EXTRA = "de.tum.bgu.lfk.openeventmap.SearchableActivity.extra_event";
	
	public static final String EVENT_QUERY_EXTRA = "de.tum.bgu.lfk.openeventmap.SearchableActivity.extra_event_query";

	private EventQuery query;
	
	public static final String ACTION_EXTRA = "de.tum.bgu.lfk.openeventmap.SearchableActivity.extra_action";
	public static final int ACTION_VIEW = 0;
	public static final int ACTION_BACK = 1;
	public static final int ACTION_CREATE_EVENT = 2;
	
	public static final String LOG_TAG = "MARK";
	
	private static final String sqlURL = Application.mainActivity.getString(R.string.sql_database_url);
	private static final String sqlUser = Application.mainActivity.getString(R.string.sql_database_user);
	private static final String sqlPassword = Application.mainActivity.getString(R.string.sql_database_password);
	private static final String sqlTable = Application.mainActivity.getString(R.string.sql_database_table);
	
	private List<Event> searchResults;

	private static final int DIALOG_ID_INSERT_EVENT = 0;
	
	public static final int SQL_LIMIT = 100;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setTitle(getString(R.string.event_searchable_activity_title));
				
		ActionBar actionbar = getSupportActionBar();
		actionbar.setDisplayShowTitleEnabled(true);
		
	    // Get the intent, verify the action and get the query
	    Intent intent = getIntent();
	    if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
	      query = (EventQuery) intent.getSerializableExtra(SearchableActivity.EVENT_QUERY_EXTRA);
	      searchEvent(query);
	    }

	}
	
	private void searchEvent(final EventQuery query) {
		Log.d(LOG_TAG, "searchEvent() called!");
		
		new AsyncTask<Void, Void, Integer>() {
			
			@SuppressWarnings("deprecation")
			@Override
			protected void onPreExecute() {
				Log.d(LOG_TAG, "searchEvent() onPreExecute() called!");
				showDialog(DialogFactory.PROGRESS_LOADING);
			}
			
			@Override
			protected Integer doInBackground(Void... arg) {

				Log.d(LOG_TAG, "searchEvent() doInBackground() called!");
				List<Event> result = new ArrayList<Event>();
				
				try {
				Class.forName("org.postgresql.Driver");
				Connection conn = DriverManager.getConnection(sqlURL, sqlUser, sqlPassword);
				
				// Create the SQL statement to be executed:
				boolean validName = query.name != null && query.name.length() > 0;
				boolean validCategory = query.category != null && query.category.length() > 0;
				boolean allCategories = query.category.equals(getResources().getStringArray(R.array.event_tag_category_values_with_all)[0]);
				boolean validStartdate = query.startdate != null;
				boolean validEnddate = query.enddate != null;
				boolean validSearchArea = query.searchArea != null;
				
				boolean needWhere = validName || (validCategory && !allCategories) || validStartdate || validEnddate || validSearchArea;
				boolean needAnd = false;
				
				String sql ;
				sql = "SELECT " + Event.sqlColumnsString + " FROM " + sqlTable;
				sql += (needWhere ? " WHERE " : "");
				sql += (validName ? "UPPER(name) LIKE UPPER('%" + query.name + "%')" : "");
				needAnd |= validName;
				sql += (needAnd && (validCategory && !allCategories) ? " AND " : "");
				sql += (validCategory && !allCategories ? "category = '" + query.category + "'" : "");
				needAnd |= (validCategory && !allCategories);
				sql += (needAnd && validStartdate ? " AND " : "");
				sql += (validStartdate ? "startdate = '" + Event.dateToSQLString(query.startdate) + "'" : "");
				needAnd |= validStartdate;
				sql += (needAnd && validEnddate ? " AND " : "");
				sql += (validEnddate ? "enddate = '" + Event.dateToSQLString(query.enddate) + "'" : "");
				needAnd |= validEnddate;
				sql += (needAnd && validSearchArea ? " AND " : "");
				sql += (validSearchArea ?
						" longitude<" + query.searchArea.getRight() +
						" AND longitude>" + query.searchArea.getLeft() +
						" AND latitude<" + query.searchArea.getTop() +
						" AND latitude>" + query.searchArea.getBottom() : "");
				sql	+= " ORDER BY category ASC, startdate ASC LIMIT " + SQL_LIMIT + ";";
				Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery(sql);
				while (rs.next()) {
					// Create events
					Event event = new Event();
					event.setId(rs.getInt("id"));
					event.setEventType(rs.getString("event_type"));
					event.setTypeId(rs.getLong("type_id"));
					event.setNumber(rs.getLong("number"));			
					event.setName(rs.getString("name"));
					event.setCategory(rs.getString("category"));
					event.setSubcategory(rs.getString("subcategory"));
					event.setStartdate(rs.getDate("startdate"));
					event.setEnddate(rs.getDate("enddate"));
					event.setRelatedItems(rs.getString("related_items"));
					event.setUrl(rs.getString("url"));
					event.setNumParticipants(rs.getString("num_participants"));
					event.setHowoften(rs.getString("howoften"));
					event.setLat(rs.getInt("latitude"));
					event.setLon(rs.getInt("longitude"));
					
					result.add(event);
				}
				rs.close();
				st.close();
				conn.close();
				} catch(ClassNotFoundException e) {
					Log.e(LOG_TAG, "ClassNotFoundException: " + e);
				} catch(SQLException e) {
					Log.e(LOG_TAG, "SQLException: " + e);			
				}		
				
				Log.d(LOG_TAG, "Found " + result.size() + " events.");
				searchResults = result;
				return searchResults.size();
			}
			
			@SuppressWarnings("deprecation")
			@Override
			protected void onPostExecute(Integer result) {
				Log.d(LOG_TAG, "searchEvent() onPostExecute(" + String.valueOf(result) +") called!");
				try {
					dismissDialog(DialogFactory.PROGRESS_LOADING);
				} catch (IllegalArgumentException e) {
					 // Avoid crash if dialog is already dismissed
					Log.d("Logic", "Loading dialog was alreday dismissed!", e);
				}
				
				// Order the events by start date
				// Events are now ordered in the SQL statement, first sorted by category and then sorted by startdate
				// Collections.sort(searchResults, new Event.ComparatorStartdate());
				
				// Display found events in the ListView
				ListAdapter listAdapter = new EventListAdapter(
						SearchableActivity.this, // Context.
		                searchResults); // Data.
				SearchableActivity.this.setListAdapter(listAdapter);
				
				// Warn if too many events where found
				if (result.equals(SQL_LIMIT)) {
					Toast.makeText(SearchableActivity.this, "Not all results are displayed! Please be more specific in your search request.", Toast.LENGTH_LONG).show();
				}		
				
				// Prompt to create new event or to go back to the event search
				if (result.equals(0)) {
					showDialog(DIALOG_ID_INSERT_EVENT);
				}
				
			}
			
		}.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			return true;
		case R.id.menu_back_back:

			Intent resultIntent = new Intent();
			resultIntent.putExtra(SearchableActivity.ACTION_EXTRA, SearchableActivity.ACTION_BACK);
			setResult(RESULT_OK, resultIntent);
			
			finish();
			return true;
		}
		
		return false;
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		ListView listView = getListView();
		EventListAdapter adapter = (EventListAdapter) getListAdapter();
		int type = adapter.getItemViewType(position);
		switch (type) {
		case EventListAdapter.TYPE_CATEGORY:
			String category = (String) listView.getItemAtPosition(position);
			adapter.expandCategory(category);
			break;
		case EventListAdapter.TYPE_EVENT:
			Event event = (Event) getListView().getItemAtPosition(position);
			
			Intent resultIntent = new Intent();
			resultIntent.putExtra(SearchableActivity.EVENT_EXTRA, event);
			resultIntent.putExtra(SearchableActivity.ACTION_EXTRA, SearchableActivity.ACTION_VIEW);
			setResult(RESULT_OK, resultIntent);
			
			finish();
			break;
		}		
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_ID_INSERT_EVENT :
			AlertDialog.Builder db = new AlertDialog.Builder(this);
			db.setTitle(R.string.no_result);
			db.setPositiveButton(R.string.create_event, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent resultIntent = new Intent();
					resultIntent.putExtra(SearchableActivity.ACTION_EXTRA, SearchableActivity.ACTION_CREATE_EVENT);
					setResult(RESULT_OK, resultIntent);
					
					finish();
				}
			});
			db.setNegativeButton(R.string.back, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {

					Intent resultIntent = new Intent();
					resultIntent.putExtra(SearchableActivity.ACTION_EXTRA, SearchableActivity.ACTION_BACK);
					setResult(RESULT_OK, resultIntent);
					
					finish();
				}
			});
			db.setMessage(R.string.ask_create_event);
			return db.create();
		}
		
		return null;
	}
}
